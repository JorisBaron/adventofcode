<?php

if($argc < 3){
	throw new Exception("Too few arguments");
}

$year = "Year".$argv[1];
$day = "Day".str_pad($argv[2], 2, "0", STR_PAD_LEFT);

mkdir('src/'.$year.DIRECTORY_SEPARATOR.$day.DIRECTORY_SEPARATOR."input", recursive: true);

touch('src/'.$year.DIRECTORY_SEPARATOR.$day.DIRECTORY_SEPARATOR."input/input.txt");
touch('src/'.$year.DIRECTORY_SEPARATOR.$day.DIRECTORY_SEPARATOR."input/test.txt");
copy(".template/_template.php", 'src/'.$year.DIRECTORY_SEPARATOR.$day.DIRECTORY_SEPARATOR."part1.php");
copy(".template/_template.php", 'src/'.$year.DIRECTORY_SEPARATOR.$day.DIRECTORY_SEPARATOR."part2.php");
