<?php

namespace AoC\Year2024\Day06;

use AoC\Common\Point;

readonly class MotionState
{
	public function __construct(
		public Point $position,
		public Point $direction,
	) {	}
}