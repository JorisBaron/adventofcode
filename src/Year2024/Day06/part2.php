<?php

use AoC\Common\Point;
use AoC\Common\Grid;
use AoC\Year2024\Day06\MotionState;

ini_set('memory_limit', '-1');

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);


$grid = Grid::createFromInput(iterator_to_array($file));
$start = null;

for($rowI = 0; $rowI < $grid->getHeight(); $rowI++) {
	for($colI = 0; $colI < $grid->getWidth(); $colI++) {
		$p = new Point($colI, $rowI);
		if($grid->get($p) === '^') {
			$start = $p;
			break 2;
		}
	}
}

/** @var MotionState[] $path */
$path = [];
$nextDirection = [
	(string)new Point( 0,-1) => new Point( 1, 0),
	(string)new Point( 1, 0) => new Point( 0, 1),
	(string)new Point( 0, 1) => new Point(-1, 0),
	(string)new Point(-1, 0) => new Point( 0,-1),
];

$position = $start;
$direction = new Point(0, -1);
$initialGrid = clone $grid;
while(true) {
	$nextPosition = $position->add($direction);
	if(!$initialGrid->isInBound($nextPosition)){
		break;
	}

	if($initialGrid->get($nextPosition) === '#') {
		$direction = $nextDirection[(string)$direction];
		continue;
	}

	$path[] = new MotionState($position, $direction);
	$position = $position->add($direction);
}

$cycleCount = 0;
$testedNewObstacle = [];
foreach($path as $i=> $motionState) {
	$newObstaclePoint = $motionState->position->add($motionState->direction);

	// Skip if obstacle already tested (intersection points)
	if(isset($testedNewObstacle[(string)$newObstaclePoint])) {
		continue;
	}

	$grid->set($newObstaclePoint, '#');

	if(walkUntilEnd($grid, $motionState, $nextDirection)) {
		$cycleCount++;
	}

	$grid->set($newObstaclePoint, '.');
	$testedNewObstacle[(string)$newObstaclePoint] = true;

	if($i % 100 === 0) {
		echo $i . " " .memory_get_peak_usage().PHP_EOL;
	}
}


echo memory_get_peak_usage().PHP_EOL;
echo $cycleCount.PHP_EOL;


/**
 * @param Grid                $grid
 * @param MotionState         $start
 * @param array<string,Point> $nextDirection
 * @return bool
 */
function walkUntilEnd(Grid $grid, MotionState $start, array $nextDirection) {
	$position = clone $start->position;
	$direction = clone $start->direction;

	$knownDirections = [];

	while(true) {

		if(!isset($knownDirections[$position->toString()])) {
			$knownDirections[$position->toString()] = [];
		}

		if(in_array($direction->toString(), $knownDirections[$position->toString()])) {
			return true;
		}

		$knownDirections[$position->toString()][] = $direction->toString();

		$nextPosition = $position->add($direction);
		if(!$grid->isInBound($nextPosition)){
			return false;
		}

		if($grid->get($nextPosition) === '#') {
			$direction = $nextDirection[(string)$direction];
			continue;
		}

		$position = $position->add($direction);
	}
}