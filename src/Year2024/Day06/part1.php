<?php

use AoC\Common\Point;
use AoC\Common\Grid;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$grid = Grid::createFromInput(iterator_to_array($file));

$position = null;
for($rowI = 0; $rowI < $grid->getHeight(); $rowI++) {
	for($colI = 0; $colI < $grid->getWidth(); $colI++) {
		$p = new Point($colI, $rowI);
		if($grid->get($p) === '^') {
			$position = $p;
			break 2;
		}
	}
}


$distinctPositionsCount = 0;
$nextDirection = [
	(string)new Point( 0,-1) => new Point( 1, 0),
	(string)new Point( 1, 0) => new Point( 0, 1),
	(string)new Point( 0, 1) => new Point(-1, 0),
	(string)new Point(-1, 0) => new Point( 0,-1),
];

$direction = new Point(0, -1);
while(true) {
	if($grid->get($position) !== 'X') {
		$grid->set($position, 'X');
		$distinctPositionsCount++;
	}

	$nextPosition = $position->add($direction);
	if(!$grid->isInBound($nextPosition)){
		break;
	}

	if($grid->get($nextPosition) === '#') {
		$direction = $nextDirection[(string)$direction];
		continue;
	}

	$position = $position->add($direction);
}

echo $distinctPositionsCount.PHP_EOL;