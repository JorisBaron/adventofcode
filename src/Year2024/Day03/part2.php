<?php

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = file_get_contents(__DIR__."/input/test.txt");
$file = file_get_contents(__DIR__."/input/input.txt");

preg_match_all("/mul\((\d{1,3}),(\d{1,3})\)|do(?:n't)?\(\)/", $file, $matches, PREG_SET_ORDER);

$enabled = true;
$sum = 0;
foreach($matches as $match) {
	if($match[0] === "do()") {
		$enabled = true;
	} elseif($match[0] === "don't()") {
		$enabled = false;
	} elseif($enabled) {
		$sum += $match[1] * $match[2];
	}
}
echo $sum;