<?php

use AoC\Common\Point;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

/** @var array<string,Point[]> $antennas */
$antennas = [];
$grid = \AoC\Common\Grid::createFromInput(iterator_to_array($file));

for($i = 0; $i < $grid->getHeight(); $i++) {
	for($j = 0; $j < $grid->getWidth(); $j++) {
		$p = new Point($j, $i);
		$cell = $grid->get($p);

		if($cell === '.') {
			continue;
		}

		if(!isset($antennas[$cell])) {
			$antennas[$cell] = [$p];
		} else {
			$antennas[$cell][] = $p;
		}
	}
}

$antinodes = [];
foreach($antennas as $frequency => $frequencyAntennas) {

	for($i = 0; $i < count($frequencyAntennas); $i++){
		for($j = 0; $j < count($frequencyAntennas); $j++){
			if($i === $j) {
				continue;
			}

			$a = $frequencyAntennas[$i];
			$b = $frequencyAntennas[$j];

			$aToB = $b->add($a->multiply(-1));

			$antinode = $b->add($aToB);
			if($grid->isInBound($antinode)) {
				$antinodes[(string)$antinode] = true;
			}
		}
	}
}


echo count($antinodes);