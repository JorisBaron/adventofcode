<?php

use AoC\Common\StringIntMap;

require_once __DIR__.'/../../../vendor/autoload.php';

ini_set('memory_limit', '2G');

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

// ((x << 6)  ^ x) & 0b11111111_11111111_11111111 -> x
// ((x >> 5)  ^ x) & 0b11111111_11111111_11111111 -> x
// ((x << 11) ^ x) & 0b11111111_11111111_11111111 -> x

$t = microtime(true);

/** @var array<string,int>[] $sequenceToPriceMaps */
$sequenceToPriceMaps = [];
while(!$file->eof()) {
	$secret = (int)$file->fgets();
	$sequenceToPriceMaps[] = buildSequenceToPriceMap($secret);
}

echo microtime(true)-$t, PHP_EOL;

echo count($sequenceToPriceMaps), PHP_EOL;

$t = microtime(true);
$bananaCount = dfs($sequenceToPriceMaps);
echo microtime(true)-$t, PHP_EOL;

echo $bananaCount, PHP_EOL;



/**
 * @param int $secret
 * @return array<string, int>
 */
function buildSequenceToPriceMap(int $secret) : array {
	$sequenceToPriceMap = [];

	$previous = $secret%10;
	$changeHistory = [];
	for($i = 0; $i < 3; $i++) {
		$secret = (($secret << 6) ^ $secret) & 0b11111111_11111111_11111111;
		$secret = (($secret >> 5) ^ $secret) & 0b11111111_11111111_11111111;
		$secret = (($secret << 11) ^ $secret) & 0b11111111_11111111_11111111;
		$price = $secret%10;
		$changeHistory[] = $price-$previous;
		$previous = $price;
	}
	for($i = 3; $i < 2000; $i++) {
		$secret = (($secret << 6) ^ $secret) & 0b11111111_11111111_11111111;
		$secret = (($secret >> 5) ^ $secret) & 0b11111111_11111111_11111111;
		$secret = (($secret << 11) ^ $secret) & 0b11111111_11111111_11111111;
		$price = $secret%10;
		$changeHistory[] = $price-$previous;
		$sequence = $changeHistory[0].','.$changeHistory[1].','.$changeHistory[2].','.$changeHistory[3];
		if(!isset($sequenceToPriceMap[$sequence])) {
			$sequenceToPriceMap[$sequence] = $price;
		}
		array_shift($changeHistory);
		$previous = $price;
	}
	return $sequenceToPriceMap;
}


/**
 * @param array<string,int>[] $sequenceToPriceMaps
 * @param int[]               $sequenceArray
 * @return int
 */
function dfs(array $sequenceToPriceMaps, array $sequenceArray = []): int {
	if(count($sequenceArray) === 4) {
		return getBananaCount(
			$sequenceToPriceMaps,
			$sequenceArray[0].','.$sequenceArray[1].','.$sequenceArray[2].','.$sequenceArray[3]
		);
	}

	$max = 0;
	for($i = -9; $i <= 9; $i++) {
		$sequenceArray[] = $i;
		$max = max($max, dfs($sequenceToPriceMaps, $sequenceArray));
		array_pop($sequenceArray);
	}
	return $max;
}

function getBananaCount(array $sequenceToPriceMaps, string $sequence): int {
	$count = 0;
	foreach($sequenceToPriceMaps as $sequenceToPriceMap) {
		if(isset($sequenceToPriceMap[$sequence])) {
			$count += $sequenceToPriceMap[$sequence];
		}
	}
	return $count;
}