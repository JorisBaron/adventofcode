Today was quite nice to do!

---

Part 1 was straightforward: loop the computation 2000 times for each line, sum everything, and that all.

---

For part 2, I started by doing a function to generate a map of "sequence to price" for each input line.
I tried to optimize the function a bit, got a <5s for the whole input, can't except much more from PHP I guess.

Then, I just implemented a DFS to find the best sequence (maximize the price). A bit slow (about 30s),
but, yeah, this is fine, this will have to do.