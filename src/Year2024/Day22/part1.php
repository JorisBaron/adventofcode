<?php

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$pruneValue = 0b11111111_11111111_11111111;

// ((x << 6)  ^ x) & $pruneValue -> x
// ((x >> 5)  ^ x) & $pruneValue -> x
// ((x << 11) ^ x) & $pruneValue -> x

$sum = 0;
while(!$file->eof()) {
	$secret = (int)$file->fgets();
	for($i = 0; $i < 2000; $i++) {
		$secret = (($secret << 6) ^ $secret) & $pruneValue;
		$secret = (($secret >> 5) ^ $secret) & $pruneValue;
		$secret = (($secret << 11) ^ $secret) & $pruneValue;
	}
	$sum += $secret;
}

echo $sum, PHP_EOL;
