<?php

use AoC\Common\Graph\Graph;
use AoC\Common\Graph\Node;
use AoC\Common\Graph\Algorithms\Components\Components;
use AoC\Common\StringIntSet;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);


$graph = new Graph();

while (!$file->eof()) {
	$line = $file->fgets();
	$line = trim($line);
	if ($line === "") {
		continue;
	}

	[$computer1, $computer2] = explode("-", $line);
	$graph->createNode($computer1);
	$graph->createNode($computer2);
	$graph->createEdge($computer1, $computer2, 1, false);
}

/** @var StringIntSet<string>[] $cliques */
$cliques = [];
$nodes = $graph->getNodes();
while(count($nodes) > 0) {
	$node = array_shift($nodes);
	$clique = findClique($graph, $node->identifier);
	$cliques[] = $clique;
	$nodes = array_filter($nodes, fn(Node $node) => !$clique->has($node->identifier));
}

usort($cliques, fn(StringIntSet $a, StringIntSet $b) => $b->count() <=> $a->count());
$largestClique = $cliques[0];
$largestCliqueArray = $largestClique->toArray();
usort($largestCliqueArray, fn(string $a, string $b) => $a <=> $b);
echo implode(",", $largestCliqueArray);



/**
 * @param Graph     $graph
 * @param string    $nodeIdentifier
 * @return StringIntSet<string>
 */
function findClique(Graph $graph, string $nodeIdentifier): StringIntSet {
	$clique = new StringIntSet();
	$clique->add($nodeIdentifier);

	$remainingNodeIdentifiers = $graph->getNextNodes($nodeIdentifier);
	foreach ($remainingNodeIdentifiers as $nextNodeIdentifier) {
		foreach ($clique as $cliqueNodeIdentifier) {
			if (!$graph->hasNextNode($cliqueNodeIdentifier, $nextNodeIdentifier)) {
				continue 2;
			}
		}
		$clique->add($nextNodeIdentifier);
	}
	return $clique;
}