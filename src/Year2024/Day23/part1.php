<?php

use AoC\Common\Graph\Graph;
use AoC\Common\Graph\Node;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);


$graph = new Graph();

while (!$file->eof()) {
	$line = $file->fgets();
	$line = trim($line);
	if ($line === "") {
		continue;
	}

	[$computer1, $computer2] = explode("-", $line);
	$graph->createNode($computer1);
	$graph->createNode($computer2);
	$graph->createEdge($computer1, $computer2, 1, false);
}


$nodes = $graph->getNodes();
$countNodes = count($nodes);

usort($nodes, function (Node $a, Node $b) {
	if(str_starts_with($a->identifier, "t")) {
		return -1;
	} elseif(str_starts_with($b->identifier, "t")) {
		return 1;
	} else {
		return $a->identifier <=> $b->identifier;
	}
});

$trio = [];
$tNodes = array_filter($nodes, fn(Node $node) => str_starts_with($node->identifier, "t"));

foreach($tNodes as $i => $tNode) {
	for($j = $i + 1; $j < $countNodes; $j++) {
		if(!$graph->hasNextNode($tNode->identifier, $nodes[$j]->identifier)) {
			continue;
		}

		for($k = $j + 1; $k < $countNodes; $k++) {
			if($graph->hasNextNode($tNode->identifier, $nodes[$k]->identifier) &&
				$graph->hasNextNode($nodes[$j]->identifier, $nodes[$k]->identifier)) {
				$trio[] = [$tNode, $nodes[$j], $nodes[$k]];
			}
		}
	}
}

echo count($trio);
