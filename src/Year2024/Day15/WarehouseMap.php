<?php

namespace AoC\Year2024\Day15;

use AoC\Common\Grid;
use AoC\Year2024\Day15\MapObjects\MapObject;
use AoC\Common\Point;

/**
 * @template-implements Grid<MapObject>
 */
class WarehouseMap extends Grid
{
	public function display() : void {
		foreach($this->grid as $y=>$row) {
			foreach($row as $x=>$cell) {
				echo $cell?->symbolOfPosition(new Point($x,$y)) ?? '.';
			}
			echo PHP_EOL;
		}
	}
}