<?php

namespace AoC\Year2024\Day15\MapObjects;

use AoC\Common\Point;

class Wall extends MapObject
{
	public function symbol() : string {
		return '#';
	}

	public function symbolOfPosition(Point $position) : string {
		return $this->symbol();
	}

	public function isMovable(Point $direction) : bool {
		return false;
	}

	public function move(Point $direction) : void {
		throw new \RuntimeException('Cannot move a wall');
	}
}