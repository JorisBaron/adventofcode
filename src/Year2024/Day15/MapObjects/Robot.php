<?php

namespace AoC\Year2024\Day15\MapObjects;

use AoC\Year2024\Day15\MapObjects\MapObject;
use AoC\Common\Point;
use OutOfBoundsException;

class Robot extends MapObject
{

	public function symbol() : string {
		return '@';
	}

	public function symbolOfPosition(Point $position) : string {
		return $this->symbol();
	}

	public function isMovable(Point $direction) : bool {
		try {
			$nextObject = $this->grid->get($this->position->add($direction));
			return $nextObject === null || $nextObject->isMovable($direction);
		} catch(OutOfBoundsException) {
			return false;
		}
	}

	public function move(Point $direction) : void {
		$nextPosition = $this->position->add($direction);
		$nextObject = $this->grid->get($nextPosition);
		$nextObject?->move($direction);

		$this->grid->set($nextPosition, $this);
		$this->grid->set($this->position, null);
		$this->position = $nextPosition;
	}
}