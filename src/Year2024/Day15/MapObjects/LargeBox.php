<?php

namespace AoC\Year2024\Day15\MapObjects;

use AoC\Year2024\Day15\MapObjects\MapObject;
use AoC\Common\Point;
use AoC\Common\Grid;
use OutOfBoundsException;

class LargeBox extends MapObject
{
	/** @var Point[] */
	protected array $positions;

	public function __construct(
		Grid $grid, Point $position
	) {
		parent::__construct($grid, $position);
		$this->positions = [
			$position,
			$position->add(new Point(1, 0)),
		];
		$this->grid->set($this->positions[1], $this);
	}


	public function symbol() : string {
		return '[]';
	}

	public function symbolOfPosition(Point $position) : string {
		if($position->equals($this->positions[0])) {
			return '[';
		} elseif($position->equals($this->positions[1])) {
			return ']';
		} else {
			throw new \RuntimeException('Invalid position');
		}
	}

	public function isMovable(Point $direction) : bool {
		try {
			return array_all($this->getNextObjects($direction), fn($nextObject) => $nextObject === null || $nextObject->isMovable($direction));
		} catch(OutOfBoundsException) {
			return false;
		}
	}

	public function move(Point $direction) : void {
		foreach($this->getNextObjects($direction) as $nextObject) {
			$nextObject?->move($direction);
		}

		foreach($this->positions as $position) {
			$this->grid->set($position, null);
		}
		$this->positions = array_map(fn(Point $position) => $position->add($direction), $this->positions);
		$this->position = $this->positions[0];
		foreach($this->positions as $position) {
			$this->grid->set($position, $this);
		}
	}

	/**
	 * @param Point $direction
	 * @return array<?MapObject>
	 */
	protected function getNextObjects(Point $direction) : array {
		return array_unique(array_filter(
			array_map(fn(Point $position) => $this->grid->get($position->add($direction)), $this->positions),
			fn(?MapObject $object) => $object !== $this
		), SORT_REGULAR);
	}
}