<?php

namespace AoC\Year2024\Day15\MapObjects;

use AoC\Common\Point;
use AoC\Common\Grid;

abstract class MapObject
{
	/**
	 * @param Grid<?MapObject> $grid
	 * @param Point            $position
	 */
	public function __construct(
		public protected(set) Grid $grid,
		public protected(set) Point $position
	) {
		$this->grid->set($position, $this);
	}

	public abstract function symbol(): string;
	public abstract function symbolOfPosition(Point $position): string;
	public abstract function isMovable(Point $direction): bool;
	public abstract function move(Point $direction): void;
}