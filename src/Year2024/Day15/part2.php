<?php

use AoC\Common\Point;
use AoC\Year2024\Day15\MapObjects\Box;
use AoC\Year2024\Day15\MapObjects\Robot;
use AoC\Year2024\Day15\MapObjects\Wall;
use AoC\Year2024\Day15\MapObjects\MapObject;
use AoC\Year2024\Day15\WarehouseMap;
use AoC\Year2024\Day15\MapObjects\LargeBox;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$gridLineArray = [];
while(($line=$file->fgets()) !== "") {
	$gridLineArray[] = str_split(widenInputLine($line));
}

$robot = null;
$grid = WarehouseMap::fromDimensions(count($gridLineArray[0]), count($gridLineArray), null);
foreach($gridLineArray as $y => $line) {
	foreach($line as $x => $cell) {
		$object = mapObjectFactory($cell, $grid, new Point($x, $y));
		if($cell === '@') {
			$robot = $object;
		}
	}
}

$inputs = '';
while(!$file->eof()) {
	$inputs .= $file->fgets();
}

foreach(str_split($inputs) as $i=> $input) {
	$direction = moveMapper($input);
	if($robot->isMovable($direction)) {
		$robot->move($direction);
	}

}

$grid->display();

echo calculateMapGpsCoordinates($grid);


function widenInputLine(string $inputLine) : string {
	$widenInputLine = '';
	foreach(str_split($inputLine) as $objectSymbol) {
		$widenInputLine .= match ($objectSymbol) {
			'O'     => '[]',
			'@'     => '@.',
			'#'     => '##',
			default => '..',
		};
	}
	return $widenInputLine;
}

function mapObjectFactory(string $symbol, WarehouseMap $grid, Point $position) : ?MapObject {
	return match ($symbol) {
		'['     => new LargeBox($grid, $position),
		']'     => $grid->get($position),
		'@'     => new Robot($grid, $position),
		'#'     => new Wall($grid, $position),
		default => null,
	};
}

function moveMapper(string $input) : Point {
	return match ($input) {
		'^' => new Point(0, -1),
		'v' => new Point(0, 1),
		'<' => new Point(-1, 0),
		'>' => new Point(1, 0)
	};
}

function calculateMapGpsCoordinates(WarehouseMap $map) : int {
	$sum = 0;
	for($y = 0; $y < $map->getHeight(); $y++) {
		for($x = 0; $x < $map->getWidth(); $x++) {
			$object = $map->get(new Point($x, $y));
			if($object instanceof LargeBox && $object->position->equals(new Point($x, $y))) {
				$sum += $x + $y*100;
			}
		}
	}
	return $sum;
}