<?php

use AoC\Common\Grid;
use AoC\Common\Point;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$start = null;
$end = null;

/** @var Grid<string> $grid */
$grid = Grid::createFromInput(iterator_to_array($file));
for($y = 0; $y < $grid->getHeight(); $y++) {
	for($x = 0; $x < $grid->getWidth(); $x++) {
		$point = new Point($x, $y);
		$cell = $grid->get($point);
		switch($cell) {
			case 'S':
				$start = $point;
				$grid->set($point, '.');
				break;
			case 'E':
				$end = $point;
				$grid->set($point, '.');
				break;
		}
		if($start && $end) {
			break 2; // early break
		}
	}
}


$current = $start;
$previous = new Point(-1, -1);
$neighbourDirections = [
	new Point(0, -1),
	new Point(1, 0),
	new Point(0, 1),
	new Point(-1, 0),
];

$pathStack = new SplStack();
$pathStack->push($current);
while(!$current->equals($end)) {
	foreach($neighbourDirections as $direction) {
		$neighbour = $current->add($direction);
		if(!$neighbour->equals($previous) && $grid->isInBound($neighbour) && $grid->get($neighbour) !== '#') {
			$previous = $current;
			$current = $neighbour;
			$pathStack->push($current);
			break;
		}
	}
}

$pathLengthMap = [];
$distance = 0;
while(!$pathStack->isEmpty()) {
	$point = $pathStack->pop();
	$pathLengthMap[$point->toString()] = $distance;
	$distance++;
}

$count = 0;
foreach($pathLengthMap as $point => $distance) {
	$point = new Point(...explode(',',$point));
	$count += countUnitsSaved($grid, $point, 100, $pathLengthMap);
}
echo $count.PHP_EOL;


/**
 * @param Grid<string>      $grid
 * @param Point             $position
 * @param int               $minToSave
 * @param array<string,int> $pathLengthMap
 * @return int
 */
function countUnitsSaved(Grid $grid, Point $position, int $minToSave, array $pathLengthMap) : int {
	$count = 0;
	foreach([
		new Point( 2, 0),
		new Point(-2, 0),
		new Point( 0, 2),
		new Point( 0,-2)
	] as $direction){
		$aboveWallPosition = $position->add($direction);
		if($grid->isInBound($aboveWallPosition) && $grid->get($aboveWallPosition) === '.') {
			if($pathLengthMap[$aboveWallPosition->toString()] - 2 - $minToSave >= $pathLengthMap[$position->toString()]) {
				$count++;
			}
		}
	}
	return $count;
}