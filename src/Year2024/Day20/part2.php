<?php

use AoC\Common\Grid;
use AoC\Common\Point;
use AoC\Common\StringIntSet;
use AoC\Common\StringIntMap;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$start = null;
$end = null;

/** @var Grid<string> $grid */
$grid = Grid::createFromInput(iterator_to_array($file));
for($y = 0; $y < $grid->getHeight(); $y++) {
	for($x = 0; $x < $grid->getWidth(); $x++) {
		$point = new Point($x, $y);
		$cell = $grid->get($point);
		switch($cell) {
			case 'S':
				$start = $point;
				$grid->set($point, '.');
				break;
			case 'E':
				$end = $point;
				$grid->set($point, '.');
				break;
		}
		if($start && $end) {
			break 2; // early break
		}
	}
}


$current = $start;
$previous = new Point(-1, -1);
$neighbourDirections = [
	new Point(0, -1),
	new Point(1, 0),
	new Point(0, 1),
	new Point(-1, 0),
];

$pathStack = new SplStack();
$pathStack->push($current);
while(!$current->equals($end)) {
	foreach($neighbourDirections as $direction) {
		$neighbour = $current->add($direction);
		if(!$neighbour->equals($previous) && $grid->isInBound($neighbour) && $grid->get($neighbour) !== '#') {
			$previous = $current;
			$current = $neighbour;
			$pathStack->push($current);
			break;
		}
	}
}

$pathLengthMap = [];
$distance = 0;
while(!$pathStack->isEmpty()) {
	$point = $pathStack->pop();
	$pathLengthMap[$point->toString()] = $distance;
	$distance++;
}

$points20Distance = generatePointAtDistance(new Point(0, 0), 0, 20);

$count = 0;
foreach($pathLengthMap as $point => $distance) {
	$point = new Point(...explode(',', $point));
	$count += countUnitsSaved($grid, $point, $points20Distance, 100, $pathLengthMap);
}
echo $count.PHP_EOL;


/**
 * @param Grid<string>               $grid
 * @param Point                      $position
 * @param StringIntMap<string,Point> $landingPoints
 * @param int                        $minToSave
 * @param array<string,int>          $pathLengthMap
 * @return int
 */
function countUnitsSaved(
	Grid $grid,
	Point $position,
	StringIntMap $landingPoints,
	int $minToSave,
	array $pathLengthMap
) : int {
	$count = 0;
	foreach($landingPoints as $direction) {
		$aboveWallPosition = $position->add($direction);
		$distance = $position->manhattanDistance($aboveWallPosition);
		if($grid->isInBound($aboveWallPosition) && $grid->get($aboveWallPosition) === '.') {
			if($pathLengthMap[$aboveWallPosition->toString()] - $distance - $minToSave >= $pathLengthMap[$position->toString()]) {
				$count++;
			}
		}
	}
	return $count;
}


function generatePointAtDistance(Point $point, int $minDist, int $maxDistance) : StringIntMap {
	$points = new StringIntMap();
	for($y = -$maxDistance; $y <= $maxDistance; $y++) {
		for($x = -$maxDistance + abs($y); $x <= $maxDistance - abs($y); $x++) {
			$p = $point->add(new Point($x, $y));
			if($point->manhattanDistance($p) >= $minDist) {
				$points[$p->toString()] = $p;
			}
		}
	}
	return $points;
}