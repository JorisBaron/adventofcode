<?php

use AoC\Common\Point;
use AoC\Common\Graph\Graph;
use AoC\Common\Graph\Node;
use AoC\Year2024\Day16\ReindeerMazeGrid;
use AoC\Common\Graph\Algorithms\DijkstraAllPath;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

/** @var ReindeerMazeGrid<string> $grid */
$grid = ReindeerMazeGrid::createFromInput(iterator_to_array($file));
$startNodeIdent = null;
$endPosition = null;

/** @var array<array{Point,Point}> $pathToProcess */
$pathToProcess = [];
$graph = new Graph();

for($y = 0; $y < $grid->getHeight(); $y++) {
	for($x = 0; $x < $grid->getWidth(); $x++) {
		$p = new Point($x, $y);
		$cell = $grid->get($p);
		if($cell === 'S') {
			$startNodeIdent = getNodeIdentifier($p, new Point(1,0));
		} elseif($cell === 'E') {
			$endPosition = $p;
		}

		if($cell === '.' || $cell === 'S') {
			$nonWallDirections = $grid->getNonWallDirection($p);

			if(count($nonWallDirections) === 1 && $cell !== 'S') {
				$graph->addNode(new Node(getNodeIdentifier($p, $nonWallDirections[0]->multiply(-1))));
				continue;
			}

			if(count($nonWallDirections) === 2 && $cell !== 'S') {
				continue;
			}

			/** @var array<array{Point,Node}> $allDirections */
			$allDirections = array_map(
				fn(Point $direction) => [$direction, new Node(getNodeIdentifier($p, $direction))],
				[   // order is important, this is a clockwise rotation
				    new Point( 1, 0),
				    new Point( 0, 1),
				    new Point(-1, 0),
				    new Point( 0,-1)
				]
			);
			foreach($allDirections as [$direction, $node]) {
				$graph->addNode($node);
			}

			for($i = 0; $i < 4; $i++) {
				[$direction, $node] = $allDirections[$i];
				$nextNode = $allDirections[($i + 1) % 4][1];
				$graph->createEdge($node->identifier, $nextNode->identifier, 1000, false);

				if(array_any($nonWallDirections, fn($pointNonWallDirection) => $pointNonWallDirection->equals($direction))) {
					$pathToProcess[] = [$p,$direction];
				}
			}
		}
	}
}


$endNode = new Node("E");
$graph->addNode($endNode);

foreach($pathToProcess as [$position, $direction]) {
	createEdge($position, $direction, $grid, $graph);
}

$dijkstra = new DijkstraAllPath($graph);
$result = $dijkstra->execute($graph->getNode($startNodeIdent), $graph->getNode("E"));
echo $result[0] . PHP_EOL;

$spotCount = 0;
foreach($result[1]->getEdges() as $edge) {
	$spotCount += $edge->weight%1000;
}
foreach($result[1]->getNodes() as $node) {
	$prevCount = count($result[1]->getPreviousNodes($node->identifier));
	if($prevCount > 1) {
		$spotCount -= $prevCount-1;
	}
}
echo ($spotCount+1) . PHP_EOL;





function getNodeIdentifier(Point $position, Point $direction): string {
	return $position->toString().','.$direction->toString();
}

function createEdge(Point $position, Point $direction, ReindeerMazeGrid $grid, Graph $graph): void {
	$length = 0;
	$startNode = $graph->getNode(getNodeIdentifier($position, $direction));

	do {
		$position = $position->add($direction);
		$length++;

		if($grid->get($position) !== ".") { // break if start or end
			break;
		}

		$previousPointDirection = $direction->multiply(-1);
		$nextDirections = array_values(array_filter(
			$grid->getNonWallDirection($position),
			fn($pointNonWallDirection) => !$pointNonWallDirection->equals($previousPointDirection)
		));

		if(count($nextDirections) !== 1) { // break if junction or dead end
			break;
		}

		if(!$nextDirections[0]->equals($direction)) {
			$length += 1000;
			$direction = $nextDirections[0];
		}
	} while(true);

	$lastCell = $grid->get($position);
	$endNode = $lastCell === "E" ? $graph->getNode("E") : $graph->getNode(getNodeIdentifier($position, $direction));

	$existingEdge = $graph->getEdge($startNode->identifier, $endNode->identifier);
	if($existingEdge === null || $length < $existingEdge->weight){
		$graph->createEdge($startNode->identifier, $endNode->identifier, $length);
	}
}