<?php

namespace AoC\Year2024\Day16;

use AoC\Common\Grid;
use AoC\Common\Point;

class ReindeerMazeGrid extends Grid
{
	public const string WALL  = '#';
	public const string EMPTY = '.';
	public const string START = 'S';
	public const string END   = 'E';

	/**
	 * @param Point $point
	 * @return Point[]
	 */
	public function getNonWallDirection(Point $point) : array {
		return array_values(array_filter([
			new Point( 1, 0),
			new Point(-1, 0),
			new Point( 0, 1),
			new Point( 0,-1)
		], function(Point $direction) use ($point) {
			$next = $point->add($direction);
			return $this->isInBound($next) && $this->get($next) !== self::WALL;
		}));
	}
}