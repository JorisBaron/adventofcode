<?php

$file = new SplFileObject("input/input.txt");

$list1 = new SplMinHeap();
$list2 = new SplMinHeap();

foreach ($file as $line) {
	$line = trim($line);
	if(empty($line)) {
		continue;
	}

	[$n1, $n2] = explode('   ', $line);
	$list1->insert($n1);
	$list2->insert($n2);
}

$distanceSum = 0;
while(!$list1->isEmpty()) {
	$n1 = $list1->extract();
	$n2 = $list2->extract();

	$distanceSum += abs($n1 - $n2);
}

echo $distanceSum;