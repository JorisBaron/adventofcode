<?php


$file = new SplFileObject("input/input.txt");

$listLeft = [];
$listRight = [];

foreach($file as $line) {
	$line = trim($line);
	if(empty($line)) {
		continue;
	}

	[$n1, $n2] = explode('   ', $line);

	if(isset($listLeft[$n1])) {
		$listLeft[$n1]++;
	} else {
		$listLeft[$n1] = 1;
	}

	if(isset($listRight[$n2])) {
		$listRight[$n2]++;
	} else {
		$listRight[$n2] = 1;
	}
}

$sum = 0;
foreach($listLeft as $n1 => $count) {
	if(isset($listRight[$n1])) {
		$sum += abs($count * $n1 * $listRight[$n1]);
	}
}

echo $sum;