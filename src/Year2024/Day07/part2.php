<?php

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);



function recursiveCalc(array $numbers, int $target, int $accumulator = 0): int {
	if(count($numbers) === 0) {
		return $accumulator === $target;
	}

	if($accumulator > $target) {
		return false;
	}

	$number = array_shift($numbers);



	return recursiveCalc($numbers, $target, $accumulator + $number) ||
	       recursiveCalc($numbers, $target, $accumulator * $number) ||
	       recursiveCalc($numbers, $target, intval($accumulator . $number));
}

$sum = 0;
while(!$file->eof()) {
	$line = $file->fgets();
	$splitLine = explode(": ", $line);
	$target = (int)$splitLine[0];
	$numbers = array_map(intval(...), explode(" ", $splitLine[1]));

	if(recursiveCalc($numbers, $target)) {
		$sum += $target;
	}
}

echo $sum;