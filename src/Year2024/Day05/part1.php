<?php

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$afterSet = [];

while(!empty($line = $file->fgets())) {
	[$first, $next] = explode('|', $line);

	if(!isset($afterSet[$first])) {
		$afterSet[$first] = [];
	}

	$afterSet[$first][$next] = true;
}

$middlePageSum = 0;
while(!$file->eof()){
	$line = $file->fgets();
	$pages = explode(',', $line);
	$middle = $pages[(int)((count($pages)-1)/2)];


	$toPrint = $pages;

	while(!empty($toPrint)) {
		$page = array_shift($toPrint);

		foreach($toPrint as $toPrintPage) {
			if(!isset($afterSet[$page][$toPrintPage]) || isset($afterSet[$toPrintPage][$page])) {
				continue 3;
			}
		}
	}

	$middlePageSum += (int)$middle;
}

echo $middlePageSum;