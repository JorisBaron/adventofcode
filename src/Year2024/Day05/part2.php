<?php

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$afterSet = [];

while(!empty($line = $file->fgets())) {
	[$first, $next] = explode('|', $line);

	if(!isset($afterSet[$first])) {
		$afterSet[$first] = [];
	}

	$afterSet[$first][$next] = true;
}

$pageOrdering = function(string $page1, string $page2) use ($afterSet): int {
	return isset($afterSet[$page1][$page2]) ? -1 : (isset($afterSet[$page2][$page1]) ? 1 : 0);
};

$middlePageSum = 0;
while(!$file->eof()){
	$line = $file->fgets();
	$pages = explode(',', $line);


	$toPrint = $pages;

	$sorted = true;
	while(!empty($toPrint)) {
		$page = array_shift($toPrint);

		foreach($toPrint as $toPrintPage) {
			if(!isset($afterSet[$page][$toPrintPage]) || isset($afterSet[$toPrintPage][$page])) {
				$sorted = false;
				break 2;
			}
		}
	}

	if($sorted) {
		continue;
	}


	// usort($pages, $pageOrdering);
	$pages = customSort($pages, $pageOrdering);
	$middle = $pages[(int)((count($pages)-1)/2)];
	$middlePageSum += (int)$middle;

}
echo $middlePageSum;


function customSort(array $pages, callable $ordering) : array {
	if(count($pages) <= 1) {
		return $pages;
	}

	$pivot = array_shift($pages);
	$before = [];
	$after = [];

	foreach($pages as $page) {
		if($ordering($page, $pivot) <= 0) {
			$before[] = $page;
		} else {
			$after[] = $page;
		}
	}

	return array_merge(
		customSort($before, $ordering),
		[$pivot],
		customSort($after, $ordering)
	);
}