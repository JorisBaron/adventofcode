<?php

use AoC\Common\StringIntSet;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

/** @var StringIntSet<string> $towels */
$towels = StringIntSet::fromIterable(explode(", ", $file->fgets()));
$longestTowel = array_reduce($towels->toArray(), fn($carry, $item) => max($carry, strlen($item)), 0);

$file->fgets(); // skip empty line

$designs = [];
while(!$file->eof()) {
	$designs[] = trim($file->fgets());
}

$validDesigns = array_filter($designs, fn($design) => checkDesignValidity($design, $towels, $longestTowel));
echo count($validDesigns);


/**
 * @param string               $design
 * @param StringIntSet<string> $towels
 * @param int                  $longestTowel
 * @return bool
 */
function checkDesignValidity(string $design, StringIntSet $towels, int $longestTowel): bool {
	$designLength = strlen($design);
	if($designLength === 0) {
		return true;
	}

	for ($len = min($longestTowel, $designLength); $len > 0; $len--) {
		$designStart = substr($design, 0, $len);
		if(!$towels->has($designStart)) {
			continue;
		}
		if(checkDesignValidity(substr($design, $len), $towels, $longestTowel)) {
			return true;
		}
	}
	return false;
}