<?php

use AoC\Common\StringIntSet;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

/** @var StringIntSet<string> $towels */
$towels = StringIntSet::fromIterable(explode(", ", $file->fgets()));
$longestTowel = array_reduce($towels->toArray(), fn($carry, $item) => max($carry, strlen($item)), 0);

$file->fgets(); // skip empty line

$designs = [];
while(!$file->eof()) {
	$designs[] = trim($file->fgets());
}

$count = 0;
foreach($designs as $design) {
	echo "Checking design: $design";
	$cache = ["" => 1];
	$c = checkDesignValidity($design, $towels, $longestTowel, $cache);
	echo " - $c\n";
	$count += $c;
}
echo $count;


/**
 * @param string               $design
 * @param StringIntSet<string> $towels
 * @param int                  $longestTowel
 * @param array                $cache
 * @return int
 */
function checkDesignValidity(string $design, StringIntSet $towels, int $longestTowel, array &$cache): int {
	if(isset($cache[$design])) {
		return $cache[$design];
	}

	$designLength = strlen($design);
	if($designLength === 0) {
		return 1;
	}

	$count = 0;
	for ($len = min($longestTowel, $designLength); $len > 0; $len--) {
		$designStart = substr($design, 0, $len);
		if(!$towels->has($designStart)) {
			continue;
		}
		$count += checkDesignValidity(substr($design, $len), $towels, $longestTowel, $cache);
	}

	$cache[$design] = $count;
	return $count;
}