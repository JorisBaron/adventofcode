<?php

use AoC\Year2024\Day02\Common;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$count = 0;
foreach($file as $line) {
	$line = trim($line);
	if(empty($line)) {
		continue;
	}

	$report = explode(' ', $line);
	if(Common::getReportUnsafePairFirstIndex($report) === null) {
		$count++;
	}
}

echo $count;