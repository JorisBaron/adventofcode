<?php

use AoC\Year2024\Day02\Common;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$count = 0;
foreach($file as $line) {
	$line = trim($line);
	if(empty($line)) {
		continue;
	}

	$report = explode(' ', $line);

	if(($unsafePairFirstIndex = Common::getReportUnsafePairFirstIndex($report)) === null) {
		$count++;
		continue;
	}

	// try to remove faulty-1, faulty or faulty+1
	//  faulty or faulty+1 are obvious choices to remove: one of the two of the faulty pair
	//  it can also be faulty-1, for exemple if faulty-1==0 and this list : 2 1 2 3 4 5
	//      -> removing the first "2" make the report valid
	// less optimal solution would be to try to remove all value one by one and check each altered report
	for($i = max(0, $unsafePairFirstIndex-1); $i <= $unsafePairFirstIndex+1; $i++) {
		$alt = $report;
		array_splice($alt, $i, 1);
		if(Common::getReportUnsafePairFirstIndex($alt) === null) {
			$count++;
			continue 2;
		}
	}
}

echo $count;