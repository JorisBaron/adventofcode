<?php

namespace AoC\Year2024\Day02;

class Common
{
	public static function getReportUnsafePairFirstIndex(array $report): ?int {
		if(count($report) < 2) {
			return null;
		}

		$increasing = $report[1] - $report[0] > 0;

		for($i = 0; $i < count($report)-1; $i++) {
			if(!self::isValid($report[$i], $report[$i+1], $increasing)) {
				return $i;
			}
		}

		return null;
	}

	private static function isValid(int $current, int $next, bool $increasing) : bool {
		$diff = $next - $current;
		return abs($diff) >= 1 && abs($diff) <= 3 && ($increasing ? $diff > 0 : $diff < 0);
	}
}