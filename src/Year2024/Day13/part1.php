<?php

use AoC\Year2024\Day13\Machine;
use AoC\Common\Point;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

/** @var Machine[] $machines */
$machines = [];
while(!$file->eof()) {
	$buttonALine = $file->fgets();
	$buttonBLine = $file->fgets();
	$prizeLine = $file->fgets();
	$file->fgets(); // Skip empty line

	$machines[] = Machine::fromInput($buttonALine, $buttonBLine, $prizeLine);
}

$sum = 0;
foreach($machines as $machine) {
	$currentLocation = new Point(0, 0);
	$aCount = 0;
	$cheapest = null;
	do {
		$remaining = $machine->prizeLocation->subtract($currentLocation);
		if($remaining->x % $machine->buttonBIncrement->x === 0 && $remaining->y % $machine->buttonBIncrement->y === 0
			&& intdiv($remaining->x, $machine->buttonBIncrement->x) === intdiv($remaining->y, $machine->buttonBIncrement->y)) {
			$cost = $aCount*3 + (int)($remaining->x / $machine->buttonBIncrement->x);
			if($cheapest===null || $cost < $cheapest) {
				$cheapest = $cost;
			}
		}

		$currentLocation = $currentLocation->add($machine->buttonAIncrement);
		$aCount++;
	} while($currentLocation->x < $machine->prizeLocation->x && $currentLocation->y < $machine->prizeLocation->y);

	if($cheapest !== null) {
		$sum += $cheapest;
	}
}

echo $sum.PHP_EOL;