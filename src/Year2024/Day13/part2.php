<?php

use AoC\Year2024\Day13\Machine;
use AoC\Common\Point;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

/** @var Machine[] $machines */
$machines = [];
while(!$file->eof()) {
	$buttonALine = $file->fgets();
	$buttonBLine = $file->fgets();
	$prizeLine = $file->fgets();
	$file->fgets(); // Skip empty line

	$machine = Machine::fromInput($buttonALine, $buttonBLine, $prizeLine);
	$machine->prizeLocation = $machine->prizeLocation->add(new Point(10000000000000, 10000000000000));
	$machines[] = $machine;
}

// xa - ya and xb - yb have opposite signs
//   aka if xa > ya then xb < yb, and vice versa
// so there is only one solution per machine
// we just need it to be an integer number of tokens


// X = a*xa + b*xb
// Y = a*ya + b*yb

// X = a*xa + b*xb
// Y = a*ya + b*yb

// X = a*xa + b*xb
// Y - X*ya/xa = a*ya - a*xa*ya/xa + b*yb - b*xb*ya/xa <- subtract (X = a*xa + b*xb)*ya/xa
//             = b*yb - b*xb*ya/xa
//             = b*(yb - xb*ya/xa)

// b = (Y - X*ya/xa) / (yb - xb*ya/xa)
//   = (Y*xa/xa - X*ya/xa) / (yb*xa/xa - xb*ya/xa)
//   = ((Y*xa - X*ya)/xa) / ((yb*xa - xb*ya)/xa)
//   = (Y*xa - X*ya) / (yb*xa - xb*ya)
//
// and we need b to be an integer
// so (Y*xa - X*ya) % (yb*xa - xb*ya) === 0

// X = a*xa + b*xb
// a = (X - b*xb) / xa
//   = (X - ((Y*xa - X*ya) / (yb*xa - xb*ya))*xb) / xa
//   = X/xa - ((Y*xa - X*ya) / (yb*xa - xb*ya))*xb/xa
//   = X/xa - ((Y*xa - X*ya)*xa) / ((yb*xa - xb*ya)*xb)
//   = X/xa - (Y*xa*xa - X*ya*xa) / (yb*xa*xb - xb*ya*xb)



$sum = 0;
foreach($machines as $machine) {
	$xa = $machine->buttonAIncrement->x;
	$ya = $machine->buttonAIncrement->y;
	$xb = $machine->buttonBIncrement->x;
	$yb = $machine->buttonBIncrement->y;
	$X = $machine->prizeLocation->x;
	$Y = $machine->prizeLocation->y;
	if(($Y*$xa - $X*$ya) % ($yb*$xa - $xb*$ya) === 0) {
		$b = intdiv($Y*$xa - $X*$ya, $yb*$xa - $xb*$ya);
		$sum += $b + 3*intdiv($X - $b*$xb, $xa);
	}
}

echo $sum.PHP_EOL;