<?php

namespace AoC\Year2024\Day13;

use AoC\Common\Point;

class Machine
{
	const BUTTON_LINE_REGEX = '/^Button [AB]: X\+(\d+), Y\+(\d+)$/';
	const PRIZE_LINE_REGEX = '/^Prize: X=(\d+), Y=(\d+)$/';

	public function __construct(
		public Point $prizeLocation,
		public Point $buttonAIncrement,
		public Point $buttonBIncrement,
	)
	{ }

	public static function fromInput(string $buttonALine, string $buttonBLine, string $prizeLine): static
	{
		preg_match(self::BUTTON_LINE_REGEX, $buttonALine, $matches);
		$buttonAIncrement = new Point((int)$matches[1], (int)$matches[2]);

		preg_match(self::BUTTON_LINE_REGEX, $buttonBLine, $matches);
		$buttonBIncrement = new Point((int)$matches[1], (int)$matches[2]);

		preg_match(self::PRIZE_LINE_REGEX, $prizeLine, $matches);
		$prizeLocation = new Point((int)$matches[1], (int)$matches[2]);

		return new static($prizeLocation, $buttonAIncrement, $buttonBIncrement);
	}
}