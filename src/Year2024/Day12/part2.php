<?php

use AoC\Common\Grid;
use AoC\Common\Point;
use AoC\Year2024\Day12\Region;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

/** @var Grid<string> $grid */
$grid = Grid::createFromInput(iterator_to_array($file, false));

/** @var Region[] $regions */
$regions = [];
/** @var Grid<bool> $markedGrid */
$markedGrid = Grid::fromDimensions($grid->getWidth(), $grid->getHeight(), false);

$neighbourDirections = [
	new Point( 1,  0),
	new Point(-1,  0),
	new Point( 0,  1),
	new Point( 0, -1),
];

for ($y = 0; $y < $grid->getHeight(); $y++) {
	for ($x = 0; $x < $grid->getWidth(); $x++) {
		$p = new Point($x, $y);
		if($markedGrid->get($p)) {
			continue;
		}
		$cell = $grid->get($p);

		$region = new Region($cell);
		$regions[] = $region;

		$pointsToExplore = new SplQueue();
		$pointsToExplore->enqueue($p);

		while(!$pointsToExplore->isEmpty()) {
			$point = $pointsToExplore->dequeue();
			$markedGrid->set($point, true);
			$region->addPoint($point);

			foreach($neighbourDirections as $neighbourDirection) {
				$neighbourPoint = $point->add($neighbourDirection);

				if(!$grid->isInBound($neighbourPoint)) {
					$region->incrementPerimeter();
					continue;
				}

				if($grid->get($neighbourPoint) === $region->regionLetter) {
					if($markedGrid->get($neighbourPoint)) {
						continue;
					}
					$markedGrid->set($neighbourPoint, true);
					$pointsToExplore->enqueue($neighbourPoint);
				} else {
					$region->incrementPerimeter();
				}
			}
		}
	}
}

echo array_reduce(
	     $regions,
	     fn(int $fenceSum, Region $region) => $fenceSum + calculateNumberOfSide($region)*$region->size,
	     0
     ).PHP_EOL;


function calculateNumberOfSide(Region $region) : int {
	$leftRotationMap = [
		new Point( 1,  0)->toString() => new Point( 0, -1),
		new Point( 0, -1)->toString() => new Point(-1,  0),
		new Point(-1,  0)->toString() => new Point( 0,  1),
		new Point( 0,  1)->toString() => new Point( 1,  0),
	];

	$rightRotationMap = [
		new Point( 1,  0)->toString() => new Point( 0,  1),
		new Point( 0,  1)->toString() => new Point(-1,  0),
		new Point(-1,  0)->toString() => new Point( 0, -1),
		new Point( 0, -1)->toString() => new Point( 1,  0),
	];

	$neighbourDirections = [
		new Point( 1,  0),
		new Point(-1,  0),
		new Point( 0,  1),
		new Point( 0, -1),
	];
	$north = new Point(0, -1);
	$east = new Point(1, 0);

	/** @var array<string,array<string,array{Point,Point}>> $borders */
	$borders = [];
	foreach($region->points as $point) {
		foreach($neighbourDirections as $neighbourDirection) {
			$neighbourPoint = $point->add($neighbourDirection);
			if(!$region->hasPoint($neighbourPoint)) {
				$borders[(string)$point][(string)$neighbourDirection] = [$point, $neighbourDirection];
			}
		}
	}

	$sideCount = 0;
	while(!empty($borders)) {
		//select a border
		foreach($borders as $borderInsidePoint) {
			if(isset($borderInsidePoint[(string)$north])) {
				$start = $borderInsidePoint[(string)$north][0];
				$startDirection = $east;
				break;
			}
		}

		$currentPoint = clone $start;
		$direction = clone $startDirection;

		do {
			$borderSide = $leftRotationMap[$direction->toString()];
			unset($borders[$currentPoint->toString()][$borderSide->toString()]);
			if(empty($borders[$currentPoint->toString()])) {
				unset($borders[$currentPoint->toString()]);
			}

			$frontPoint = $currentPoint->add($direction);
			if(!$region->hasPoint($frontPoint)) {
				$direction = $rightRotationMap[$direction->toString()];
				$sideCount++;
				continue;
			}
			$frontLeftPoint = $frontPoint->add($leftRotationMap[$direction->toString()]);
			if($region->hasPoint($frontLeftPoint)) {
				$direction = $leftRotationMap[$direction->toString()];
				$currentPoint = $frontLeftPoint;
				$sideCount++;
				continue;
			}

			$currentPoint = $frontPoint;
		} while(!$currentPoint->equals($start) || !$direction->equals($startDirection));
	}

	return $sideCount;
}




