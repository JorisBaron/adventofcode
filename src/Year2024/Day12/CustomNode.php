<?php

namespace AoC\Year2024\Day12;

use AoC\Common\Graph\Node;

class CustomNode extends Node
{
	public protected(set) string $regionLetter;
	public int $size;

	public function __construct(string $identifier, string $regionLetter, int $size)
	{
		parent::__construct($identifier);
		$this->regionLetter = $regionLetter;
		$this->size = $size;
	}
}