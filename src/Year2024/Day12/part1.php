<?php

use AoC\Common\Grid;
use AoC\Common\Point;
use AoC\Year2024\Day12\Region;

require_once __DIR__.'/../../../vendor/autoload.php';

$file = new SplFileObject(__DIR__."/input/test.txt");
// $file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

/** @var Grid<string> $grid */
$grid = Grid::createFromInput(iterator_to_array($file, false));

$regions = [];
$markedGrid = Grid::fromDimensions($grid->getWidth(), $grid->getHeight(), false);

$neighbourDirections = [
	new Point( 1,  0),
	new Point(-1,  0),
	new Point( 0,  1),
	new Point( 0, -1),
];

for ($y = 0; $y < $grid->getHeight(); $y++) {
	for ($x = 0; $x < $grid->getWidth(); $x++) {
		$p = new Point($x, $y);
		if($markedGrid->get($p)) {
			continue;
		}
		$cell = $grid->get($p);

		$region = new Region($cell);
		$regions[] = $region;

		$pointsToExplore = new SplQueue();
		$pointsToExplore->enqueue($p);

		while(!$pointsToExplore->isEmpty()) {
			$point = $pointsToExplore->dequeue();
			$markedGrid->set($point, true);
			$region->addPoint($point);

			foreach($neighbourDirections as $neighbourDirection) {
				$neighbourPoint = $point->add($neighbourDirection);

				if(!$grid->isInBound($neighbourPoint)) {
					$region->incrementPerimeter();
					continue;
				}

				if($grid->get($neighbourPoint) === $region->regionLetter) {
					if($markedGrid->get($neighbourPoint)) {
						continue;
					}
					$markedGrid->set($neighbourPoint, true);
					$pointsToExplore->enqueue($neighbourPoint);
				} else {
					$region->incrementPerimeter();
				}
			}
		}
	}
}

echo array_reduce(
	$regions,
	fn(int $fenceSum, Region $region) => $fenceSum + $region->perimeter*$region->size,
	0
     ).PHP_EOL;



