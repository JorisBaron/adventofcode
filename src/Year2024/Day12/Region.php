<?php

namespace AoC\Year2024\Day12;

use AoC\Common\Point;

class Region
{
	public protected(set) string $regionLetter;
	/** @var array<string,Point> */
	public protected(set) array $points = [];
	public protected(set) int $perimeter = 0;

	public int $size {
		get => count($this->points);
	}

	public function __construct(string $regionLetter) {
		$this->regionLetter = $regionLetter;
		$this->points = [];
		$this->perimeter = 0;
	}

	public function addPoint(Point $point) : void {
		$this->points[(string)$point] = $point;
	}

	public function hasPoint(Point $point) : bool {
		return isset($this->points[(string)$point]);
	}

	public function incrementPerimeter(int $perimeter = 1) : void {
		$this->perimeter += $perimeter;
	}
}