<?php

use AoC\Common\Graph\Graph;
use AoC\Common\Graph\Node;
use AoC\Common\Point;
use AoC\Common\Graph\Algorithms\Components\Components;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);


$h = 70;
$w = 70;
$bytesToRead = 1024;

$startNodeIdentifier = nodeIdentifier(0, 0);
$endNodeIdentifier = nodeIdentifier($w, $h);

$graph = new Graph();
for($x = 0; $x <= $w; $x++) {
	for($y = 0; $y <= $h; $y++) {
		$p = new Point($x, $y);
		$node = new Node($p->toString());
		$graph->addNode($node);

		foreach([
			$p->add(new Point(-1, 0)),
			$p->add(new Point(0, -1)),
		] as $neighborPoint) {
			if($neighborPoint->x >= 0 && $neighborPoint->y >= 0) {
				$graph->createEdge($node->identifier, $neighborPoint->toString(), 1, false);
			}
		}
	}
}

$componentsAlgorithm = new Components();
/** @var SplStack<Point> $removedNodesPoint */
$removedNodesPoint = new SplStack();

while(!$file->eof()) {
	[$x, $y] = explode(',', $file->fgets());
	$graph->removeNode(nodeIdentifier($x, $y));
	$removedNodesPoint->push(new Point($x, $y));
}


while(!$removedNodesPoint->isEmpty()) {
	$point = $removedNodesPoint->pop();

	$graph->createNode($point->toString());
	foreach([
		$point->add(new Point(-1, 0)),
		$point->add(new Point(0, -1)),
		$point->add(new Point(1, 0)),
		$point->add(new Point(0, 1)),
	] as $neighborPoint) {
		if($graph->hasNode($neighborPoint->toString())) {
			$graph->createEdge($point->toString(), $neighborPoint->toString(), 1, false);
		}
	}


	$component = $componentsAlgorithm->getComponent($graph, $startNodeIdentifier);
	if($component->has($endNodeIdentifier)) {
		echo $point->toString();
		break;
	}
}






function nodeIdentifier(int $x, int $y) : string {
	return $x.','.$y;
}