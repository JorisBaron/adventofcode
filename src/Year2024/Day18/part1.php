<?php

use AoC\Common\Graph\Graph;
use AoC\Common\Graph\Node;
use AoC\Common\Point;
use AoC\Common\Graph\Algorithms\Dijkstra;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);


$h = 70;
$w = 70;
$bytesToRead = 1024;

$graph = new Graph();
for($x = 0; $x <= $w; $x++) {
	for($y = 0; $y <= $h; $y++) {
		$p = new Point($x, $y);
		$node = new Node($p->toString());
		$graph->addNode($node);

		foreach([
			$p->add(new Point(-1, 0)),
			$p->add(new Point(0, -1)),
		] as $previousNeighbor) {
			if($previousNeighbor->x >= 0 && $previousNeighbor->y >= 0) {
				$graph->createEdge($node->identifier, $previousNeighbor->toString(), 1, false);
			}
		}
	}
}

for($i = 0; $i < $bytesToRead; $i++) {
	[$x, $y] = explode(',', $file->fgets());
	$graph->removeNode(nodeIdentifier($x, $y));
}

$pathAlgorithm = new Dijkstra($graph);
$source = $graph->getNode(nodeIdentifier(0, 0));
$destination = $graph->getNode(nodeIdentifier($w, $h));
$path = $pathAlgorithm->execute($source, $destination);


var_dump($path);



function nodeIdentifier(int $x, int $y) : string {
	return $x.','.$y;
}