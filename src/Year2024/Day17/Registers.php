<?php

namespace AoC\Year2024\Day17;

enum Registers: int
{
	case A = 0;
	case B = 1;
	case C = 2;
	case INSTRUCTION_POINTER = 3;
	case OUTPUT = 4;
}
