<?php

namespace AoC\Year2024\Day17;

use AoC\Year2024\Day17\Instructions\UnaryInstruction;

class Program
{
	private array $instructions = [];
	private array $registers;

	public function __construct() {
		$this->registers = [
			Registers::A->value => 0,
			Registers::B->value => 0,
			Registers::C->value => 0,
			Registers::INSTRUCTION_POINTER->value => 0,
			Registers::OUTPUT->value => [],
		];
	}

	public function registerInstruction(int $opcode, UnaryInstruction $instruction) : void {
		$this->instructions[$opcode] = $instruction;
	}

	public function setRegisterValue(Registers $register, mixed $value) : void {
		$this->registers[$register->value] = $value;
	}

	public function getRegisterValue(Registers $register) : mixed {
		return $this->registers[$register->value];
	}


	public function run(array $program) : array {
		while($this->registers[Registers::INSTRUCTION_POINTER->value] < count($program)) {
			$instructionPointer = $this->registers[Registers::INSTRUCTION_POINTER->value];
			$instruction = $this->instructions[$program[$instructionPointer]];
			$operand = $program[$instructionPointer+1];
			$this->registers = $instruction->execute($this->registers, $operand);
		}
		return $this->registers[Registers::OUTPUT->value];
	}

	public function resetRegisters() : void {
		$this->registers = [
			Registers::A->value => 0,
			Registers::B->value => 0,
			Registers::C->value => 0,
			Registers::INSTRUCTION_POINTER->value => 0,
			Registers::OUTPUT->value => [],
		];
	}
}