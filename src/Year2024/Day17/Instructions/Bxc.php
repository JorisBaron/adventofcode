<?php

namespace AoC\Year2024\Day17\Instructions;

use AoC\Year2024\Day17\Instructions\UnaryInstruction;
use AoC\Year2024\Day17\Registers;

class Bxc implements UnaryInstruction
{

	public function execute(array $registers, int $operand) : array {
		$registers[Registers::B->value] = $registers[Registers::B->value] ^ $registers[Registers::C->value];
		$registers[Registers::INSTRUCTION_POINTER->value] += 2;
		return $registers;
	}
}