<?php

namespace AoC\Year2024\Day17\Instructions;

use AoC\Year2024\Day17\Instructions\UnaryInstruction;
use AoC\Year2024\Day17\Operands\Operand;
use AoC\Year2024\Day17\Operands\ComboOperand;
use AoC\Year2024\Day17\Registers;

class Out implements UnaryInstruction
{
	private Operand $operandType;
	public function __construct() {
		$this->operandType = new ComboOperand();
	}

	public function execute(array $registers, int $operand) : array {
		$registers[Registers::OUTPUT->value][] = $this->operandType->getValue($registers, $operand) & 0b111;
		$registers[Registers::INSTRUCTION_POINTER->value] += 2;
		return $registers;
	}
}