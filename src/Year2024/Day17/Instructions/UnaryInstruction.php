<?php

namespace AoC\Year2024\Day17\Instructions;

interface UnaryInstruction
{
	public function execute(array $registers, int $operand): array;
}