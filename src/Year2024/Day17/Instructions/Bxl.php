<?php

namespace AoC\Year2024\Day17\Instructions;

use AoC\Year2024\Day17\Instructions\UnaryInstruction;
use AoC\Year2024\Day17\Operands\LiteralOperand;
use AoC\Year2024\Day17\Operands\Operand;
use AoC\Year2024\Day17\Registers;

class Bxl implements UnaryInstruction
{
	private Operand $operandType;
	public function __construct() {
		$this->operandType = new LiteralOperand();
	}

	public function execute(array $registers, int $operand) : array {
		$registers[Registers::B->value] = $registers[Registers::B->value] ^ $this->operandType->getValue($registers, $operand);
		$registers[Registers::INSTRUCTION_POINTER->value] += 2;
		return $registers;
	}
}