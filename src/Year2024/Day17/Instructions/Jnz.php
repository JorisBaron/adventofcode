<?php

namespace AoC\Year2024\Day17\Instructions;

use AoC\Year2024\Day17\Instructions\UnaryInstruction;
use AoC\Year2024\Day17\Operands\Operand;
use AoC\Year2024\Day17\Operands\LiteralOperand;
use AoC\Year2024\Day17\Registers;

class Jnz implements UnaryInstruction
{
	private Operand $operandType;
	public function __construct() {
		$this->operandType = new LiteralOperand();
	}

	public function execute(array $registers, int $operand) : array {
		if($registers[Registers::A->value] === 0) {
			$registers[Registers::INSTRUCTION_POINTER->value] += 2;
		} else {
			$registers[Registers::INSTRUCTION_POINTER->value] = $this->operandType->getValue($registers, $operand);
		}
		return $registers;
	}
}