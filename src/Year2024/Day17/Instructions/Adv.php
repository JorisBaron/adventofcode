<?php

namespace AoC\Year2024\Day17\Instructions;

use AoC\Year2024\Day17\Instructions\UnaryInstruction;
use AoC\Year2024\Day17\Operands\ComboOperand;
use Exception;
use AoC\Year2024\Day17\Registers;
use AoC\Year2024\Day17\Operands\Operand;

class Adv implements UnaryInstruction
{
	private Operand $operandType;
	public function __construct() {
		$this->operandType = new ComboOperand();
	}

	/**
	 * @throws Exception
	 */
	public function execute(array $registers, int $operand) : array {
		$registers[Registers::A->value] = (int)($registers[Registers::A->value] / (2 ** $this->operandType->getValue($registers, $operand)));
		$registers[Registers::INSTRUCTION_POINTER->value] += 2;
		return $registers;
	}
}