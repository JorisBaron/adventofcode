<?php

namespace AoC\Year2024\Day17\Operands;

use AoC\Year2024\Day17\Operands\Operand;
use Exception;

class ComboOperand implements Operand
{

	/**
	 * @param array $registers
	 * @param int   $operand
	 * @return int
	 * @throws Exception
	 */
	public function getValue(array $registers, int $operand) : int {
		if($operand <= 3) {
			return $operand;
		}
		if($operand < 7) {
			return $registers[$operand-4];
		}
		throw new Exception("Invalid operand");
	}
}