<?php

namespace AoC\Year2024\Day17\Operands;

interface Operand
{
	public function getValue(array $registers, int $operand): int;
}