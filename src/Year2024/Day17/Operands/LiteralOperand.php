<?php

namespace AoC\Year2024\Day17\Operands;

use AoC\Year2024\Day17\Operands\Operand;

class LiteralOperand implements Operand
{
	public function getValue(array $registers, int $operand) : int {
		return $operand;
	}
}