<?php

use AoC\Year2024\Day17\Instructions\{Adv, Bxl, Bst, Jnz, Bxc, Out, Bdv, Cdv};
use AoC\Year2024\Day17\Registers;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = file_get_contents(__DIR__."/input/test.txt");
$file = file_get_contents(__DIR__."/input/input.txt");

preg_match('/^Register A: (\d+)\s+Register B: (\d+)\s+Register C: (\d+)\s+Program: (.+)$/', $file, $matches);
[$_, $registerA, $registerB, $registerC, $input] = $matches;

$program = new \AoC\Year2024\Day17\Program();
$program->registerInstruction(0, new Adv());
$program->registerInstruction(1, new Bxl());
$program->registerInstruction(2, new Bst());
$program->registerInstruction(3, new Jnz());
$program->registerInstruction(4, new Bxc());
$program->registerInstruction(5, new Out());
$program->registerInstruction(6, new Bdv());
$program->registerInstruction(7, new Cdv());

$program->setRegisterValue(Registers::A, $registerA);
$program->setRegisterValue(Registers::B, $registerB);
$program->setRegisterValue(Registers::C, $registerC);

$output = $program->run(array_map(intval(...), explode(',', $input)));

echo implode(',', $output);