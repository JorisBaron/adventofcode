<?php

use AoC\Year2024\Day17\Instructions\{Adv, Bxl, Bst, Jnz, Bxc, Out, Bdv, Cdv};
use AoC\Year2024\Day17\Registers;
use AoC\Year2024\Day17\Program;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = file_get_contents(__DIR__."/input/test.txt");
$file = file_get_contents(__DIR__."/input/input.txt");

preg_match('/^Register A: (\d+)\s+Register B: (\d+)\s+Register C: (\d+)\s+Program: (.+)$/', $file, $matches);
[$_, $registerA, $registerB, $registerC, $input] = $matches;

$program = new Program();
$program->registerInstruction(0, new Adv());
$program->registerInstruction(1, new Bxl());
$program->registerInstruction(2, new Bst());
$program->registerInstruction(3, new Jnz());
$program->registerInstruction(4, new Bxc());
$program->registerInstruction(5, new Out());
$program->registerInstruction(6, new Bdv());
$program->registerInstruction(7, new Cdv());


/**
 * @param Program $program
 * @param int     $stateBits
 * @param array   $inputArray
 * @param int     $i
 * @return ?array<bool>
 * @throws Exception
 */
function computeSolution(Program $program, int $stateBits, array $inputArray, int $i) : ?int {
	if($i < 0) {
		return $stateBits>>3;
	}

	$inputSlice = array_slice($inputArray, $i);
	$desiredOutput = implode(',', $inputSlice);

	for($a = 0; $a < 8; $a++) {
		$localBits = $stateBits + $a;

		$program->resetRegisters();
		$program->setRegisterValue(Registers::A, $localBits);

		$outputArray = $program->run($inputArray);
		$output = implode(',', $outputArray);
		if($output === $desiredOutput) {
			// we only keep the first working solution since we are processing the bits in ascending order (0->7)
			$solution = computeSolution($program, $localBits<<3, $inputArray, $i - 1);
			if($solution !== null) {
				return $solution;
			}
		}
	}
	return null;
}

$inputArray = array_map(intval(...), explode(',', $input));
$solution = computeSolution($program, 0, $inputArray, count($inputArray) - 1);
echo $solution.PHP_EOL;

$program->resetRegisters();
$program->setRegisterValue(Registers::A, $solution);
$output = $program->run($inputArray);
echo implode(',', $output).PHP_EOL;
