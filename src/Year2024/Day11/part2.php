<?php

use AoC\Common\StringIntSet;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);


$stones = array_map(intval(...), explode(" ", $file->fgets()));
$stones = array_combine(
	$stones,
	array_fill(0, count($stones), 1)
);


echo blinkAndCount($stones, 75);


function blinkAndCount(array $stones, int $remainingBlinks) : int {
	if($remainingBlinks === 0) {
		return array_sum($stones);
	}

	/** @var array<string,int> $stoneMap */
	$stoneMap = [];

	foreach($stones as $stone => $count) {
		if($stone === 0) {
			$stoneMap = addOrIncrement($stoneMap, 1, $count);
		} elseif(strlen((string)$stone) % 2 === 0) {
			$middle = strlen((string)$stone) / 2;
			// (string)(int) remove leading zeros
			$stoneMap = addOrIncrement($stoneMap, (int)substr((string)$stone, 0, $middle), $count);
			$stoneMap = addOrIncrement($stoneMap, (int)substr((string)$stone, $middle), $count);
		} else {
			$stoneMap = addOrIncrement($stoneMap, $stone * 2024, $count);
		}
	}

	return blinkAndCount($stoneMap, $remainingBlinks - 1);
}


function addOrIncrement(array $map, string $key, int $value) : array {
	if(isset($map[$key])) {
		$map[$key] += $value;
	} else {
		$map[$key] = $value;
	}
	return $map;
}
