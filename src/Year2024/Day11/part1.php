<?php

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);


$stones = explode(" ", $file->fgets());

$stoneCount = 0;
foreach($stones as $stone) {
	$stoneCount += blinkAndCount($stone, 25);
}

echo $stoneCount;


function blinkAndCount(string $stone, int $remainingBlinks) : int {
	if ($remainingBlinks === 0) {
		return 1;
	}

	if($stone === '0') {
		return blinkAndCount('1', $remainingBlinks - 1);
	}

	if(strlen($stone)%2 === 0) {
		$middle = strlen($stone)/2;
		// (string)(int) remove leading zeros
		return blinkAndCount((string)(int)substr($stone, 0, $middle), $remainingBlinks - 1) +
		       blinkAndCount((string)(int)substr($stone, $middle), $remainingBlinks - 1);
	}

	return blinkAndCount((string)((int)$stone*2024), $remainingBlinks - 1);
}