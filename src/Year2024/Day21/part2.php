<?php

use AoC\Common\StringIntMap;
use AoC\Common\Point;
use AoC\Year2024\Day21\F;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = file_get_contents(__DIR__."/input/test.txt");
$file = file_get_contents(__DIR__."/input/input.txt");

$numericKeypadLayout = new StringIntMap();
$numericKeypadLayout->addAll([
	'7' => new Point(0,0), '8' => new Point(1,0), '9' => new Point(2,0),
	'4' => new Point(0,1), '5' => new Point(1,1), '6' => new Point(2,1),
	'1' => new Point(0,2), '2' => new Point(1,2), '3' => new Point(2,2),
	/*                  */ '0' => new Point(1,3), 'A' => new Point(2,3),
]);
$numericKeypadMapping = F::computeKeypadMoveMapping($numericKeypadLayout, F::checkNumericKeypadMoveValidity(...));

$directionalKeypadLayout = new StringIntMap();
$directionalKeypadLayout->addAll([
	/*                  */ '^' => new Point(1,0), 'A' => new Point(2,0),
	'<' => new Point(0,1), 'v' => new Point(1,1), '>' => new Point(2,1),
]);
$directionalKeypadMapping = F::computeKeypadMoveMapping($directionalKeypadLayout, F::checkDirectionalKeypadMoveValidity(...));


$codes = explode("\n", str_replace(["\r\n","\r"], "\n", $file));

$cache = new StringIntMap();
$complexitySum = 0;
foreach($codes as $code) {
	$initialCode = $code;
	$codeAfterNumericKeypad = F::calculateShortestButtonSequence($numericKeypadMapping, 'A', $code);
	$complexity = F::calculateShortestButtonSequenceWithCache($directionalKeypadMapping, 'A', $codeAfterNumericKeypad, 25, $cache);

	$complexitySum += (int)(substr($initialCode, 0, -1)) * $complexity;
}

echo $complexitySum.PHP_EOL;