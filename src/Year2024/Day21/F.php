<?php

namespace AoC\Year2024\Day21;

use AoC\Common\StringIntMap;
use AoC\Common\Point;

class F
{
	/**
	 * @param StringIntMap<string,Point> $keypadLayout
	 * @return StringIntMap<string,StringIntMap<string,string>>
	 */
	public static function computeKeypadMoveMapping(
		StringIntMap $keypadLayout,
		callable $moveValidityCheckFunction
	) : StringIntMap {
		$numericKeypadMapping = new StringIntMap();
		foreach ($keypadLayout as $key => $position) {
			$numericKeypadMapping[$key] = new StringIntMap();
			foreach ($keypadLayout as $otherKey => $otherPosition) {
				$deltaX = $otherPosition->x - $position->x;
				$deltaY = $otherPosition->y - $position->y;
				$verticalSequence = str_repeat($deltaY>0 ? 'v' : '^', abs($deltaY));
				$horizontalSequence = str_repeat($deltaX>0 ? '>' : '<', abs($deltaX));


				if($deltaX === 0 && $deltaY === 0) {
					$numericKeypadMapping[$key][$otherKey] = 'A';
					continue;
				}

				if($deltaX === 0 || $deltaY === 0) {
					$numericKeypadMapping[$key][$otherKey] = $verticalSequence.$horizontalSequence.'A';
					continue;
				}

				$validSequences = [];
				foreach (array_unique([
					$verticalSequence.$horizontalSequence.'A',
					$horizontalSequence.$verticalSequence.'A',
				]) as $sequence) {
					if($moveValidityCheckFunction($position, $sequence)) {
						$validSequences[] = $sequence;
					}
				}
				if(count($validSequences) > 1) {
					usort($validSequences, self::compareSequencePriority(...));
				}
				$numericKeypadMapping[$key][$otherKey] = $validSequences[0];

			}
		}
		return $numericKeypadMapping;
	}


	private static function compareSequencePriority(string $sequence1, string $sequence2): int {
		return -(static::sequencePriority($sequence1) <=> static::sequencePriority($sequence2));
	}

	private static function sequencePriority(string $sequence): int {
		return match (substr($sequence,0,1)) {
			'<' => 4,
			'v' => 3,
			'^' => 2,
			'>' => 1,
			default => 0,
		};
	}

	/**
	 * @param StringIntMap<string,Point> $numericKeypadLayout
	 * @return StringIntMap<string,StringIntMap<string,string[]>>
	 */
	public static function computeKeypadMovesMapping(
		StringIntMap $numericKeypadLayout,
		callable $moveValidityCheckFunction
	) : StringIntMap {
		$numericKeypadMapping = new StringIntMap();
		foreach ($numericKeypadLayout as $key => $position) {
			$numericKeypadMapping[$key] = new StringIntMap();
			foreach ($numericKeypadLayout as $otherKey => $otherPosition) {
				$deltaX = $otherPosition->x - $position->x;
				$deltaY = $otherPosition->y - $position->y;
				$verticalSequence = str_repeat($deltaY>0 ? 'v' : '^', abs($deltaY));
				$horizontalSequence = str_repeat($deltaX>0 ? '>' : '<', abs($deltaX));


				if($deltaX === 0 && $deltaY === 0) {
					$numericKeypadMapping[$key][$otherKey] = ['A'];
					continue;
				}

				if($deltaX === 0 || $deltaY === 0) {
					$numericKeypadMapping[$key][$otherKey] = [$verticalSequence.$horizontalSequence.'A'];
					continue;
				}

				$validSequences = [];
				foreach (array_unique([
					$verticalSequence.$horizontalSequence.'A',
					$horizontalSequence.$verticalSequence.'A',
				]) as $sequence) {
					if($moveValidityCheckFunction($position, $sequence)) {
						$validSequences[] = $sequence;
					}
				}
				$numericKeypadMapping[$key][$otherKey] = $validSequences;
			}
		}
		return $numericKeypadMapping;
	}

	public static function checkNumericKeypadMoveValidity(Point $origin, string $sequence): bool {
		if($origin->y === 3) { // on the '.0A' line
			return !str_starts_with($sequence, str_repeat('<', $origin->x));
		} elseif($origin->x === 0) { // on the '741.' column
			return !str_starts_with($sequence, str_repeat('v', 3-$origin->y));
		}
		return true;
	}

	public static function checkDirectionalKeypadMoveValidity(Point $origin, string $sequence): bool {
		if($origin->y === 0) { // on the '.^A' line
			return !str_starts_with($sequence, str_repeat('<', $origin->x));
		} elseif($origin->x === 0) { // on the '.<' column
			return !str_starts_with($sequence, str_repeat('^', $origin->y));
		}
		return true;
	}


	public static function calculateShortestButtonSequence(StringIntMap $keypadMapping, string $startKey, string $keys) : string {
		$code = '';
		$currentKey = $startKey;
		foreach (str_split($keys) as $key) {
			$code .= $keypadMapping[$currentKey][$key];
			$currentKey = $key;
		}
		return $code;
	}

	/**
	 * @param StringIntMap<string,StringIntMap<string,string>> $keypadMapping
	 * @param string                                           $startKey
	 * @param string                                           $keys
	 * @param int                                              $depth
	 * @param StringIntMap<string,int>                         $cache
	 * @return int
	 */
	public static function calculateShortestButtonSequenceWithCache(
		StringIntMap $keypadMapping, string $startKey, string $keys, int $depth, StringIntMap &$cache
	) : int {
		if($cache->has($keys.','.$depth)) {
			return $cache[$keys.','.$depth];
		}

		if($depth === 0) {
			$cache[$keys.','.$depth] = strlen($keys);
			return strlen($keys);
		}

		$complexity = 0;
		$currentKey = $startKey;
		foreach (str_split($keys) as $key) {
			$code = $keypadMapping[$currentKey][$key];
			$complexity += self::calculateShortestButtonSequenceWithCache($keypadMapping, 'A', $code, $depth-1, $cache);
			$currentKey = $key;
		}

		$cache[$keys.','.$depth] = $complexity;
		return $complexity;
	}


	/**
	 * For debugging purposes
	 * @param StringIntMap<string,Point> $keypadLayout
	 * @param string       $startKey
	 * @param string       $keys
	 * @return string
	 */
	public static function playButtonSequence(StringIntMap $keypadLayout, string $startKey, string $keys) : string {
		$reverseKeypadLayout = new StringIntMap();
		foreach ($keypadLayout as $key => $position) {
			$reverseKeypadLayout->add($position->toString(), $key);
		}

		$currentPosition = $keypadLayout[$startKey];
		$output = '';
		foreach (str_split($keys) as $key) {
			if($key == 'A') {
				$output .= $reverseKeypadLayout[$currentPosition->toString()];
			} else {
				$currentPosition = $currentPosition->add(match ($key) {
					'<' => new Point(-1, 0),
					'>' => new Point(1, 0),
					'^' => new Point(0, -1),
					'v' => new Point(0, 1),
				});
			}
		}
		return $output;
	}
}