<?php

use AoC\Common\StringIntMap;
use AoC\Common\Point;
use AoC\Year2024\Day21\F;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = file_get_contents(__DIR__."/input/test.txt");
$file = file_get_contents(__DIR__."/input/input.txt");

$numericKeypadLayout = new StringIntMap();
$numericKeypadLayout->addAll([
    '7' => new Point(0,0), '8' => new Point(1,0), '9' => new Point(2,0),
    '4' => new Point(0,1), '5' => new Point(1,1), '6' => new Point(2,1),
    '1' => new Point(0,2), '2' => new Point(1,2), '3' => new Point(2,2),
	/*                  */ '0' => new Point(1,3), 'A' => new Point(2,3),
]);
$numericKeypadMapping = F::computeKeypadMoveMapping($numericKeypadLayout, F::checkNumericKeypadMoveValidity(...));

$directionalKeypadLayout = new StringIntMap();
$directionalKeypadLayout->addAll([
	/*                  */ '^' => new Point(1,0), 'A' => new Point(2,0),
	'<' => new Point(0,1), 'v' => new Point(1,1), '>' => new Point(2,1),
]);
$directionalKeypadMapping = F::computeKeypadMoveMapping($directionalKeypadLayout, F::checkDirectionalKeypadMoveValidity(...));

$keypadMappingOrder = [
	$numericKeypadMapping,
	$directionalKeypadMapping,
	$directionalKeypadMapping,
];

$codes = explode("\n", str_replace(["\r\n","\r"], "\n", $file));

$complexitySum = 0;
foreach($codes as $code) {
	$initialCode = $code;
	foreach($keypadMappingOrder as $keypadMapping) {
		$code = F::calculateShortestButtonSequence($keypadMapping, 'A', $code);
	}

	$complexitySum += (int)(substr($initialCode, 0, -1)) * strlen($code);
}

echo $complexitySum.PHP_EOL;

//        (A)                     1            7          9                 A
//        (A)         <<      ^   A       ^^   A     >>   A        vvv      A
//        (A)  v <<   AA >  ^ A > A   <   AA > A  v  AA ^ A  v <   AAA >  ^ A
//3(me) : (A)v<A<AA>>^AAvA<^A>AvA^Av<<A>>^AAvA^Av<A>^AA<A>Av<A<A>>^AAAvA<^A>A


//
//3(aoc): <v<A>>^A<vA<A>>^AAvAA<^A>A<v<A>>^AAvA^A<vA>^AA<A>A<v<A>A>^AAAvA<^A>A


//<v<A>>^AA<vA<A>>^AAvAA<^A>A<vA>^A<A>A<vA>^A<A>A<v<A>A>^AAvA<^A>A

//                   3                          7          9                 A
//               ^   A       ^^        <<       A     >>   A        vvv      A
//           <   A > A   <   AA  v <   AA >>  ^ A  v  AA ^ A  v <   AAA >  ^ A
//5(me) : v<<A>>^AvA^Av<<A>>^AAv<A<A>>^AAvAA^<A>Av<A>^AA<A>Av<A<A>>^AAAvA^<A>A
//        v<<A>>^AvA^Av<<A>>^AAv<A<A>>^AAvAA^<A>Av<A>^AA<A>Av<A<A>>^AAAvA^<A>A


//                   3                      7          9                 A
//               ^   A         <<      ^^   A     >>   A        vvv	     A
//           <   A > A  v <<   AA >  ^ AA > A  v  AA ^ A   < v  AAA >  ^ A
//5(aoc): <v<A>>^AvA^A<vA<AA>>^AAvA<^A>AAvA^A<vA>^AA<A>A<v<A>A>^AAAvA<^A>A