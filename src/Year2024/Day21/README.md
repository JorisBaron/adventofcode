Holy moly, this was hard AF!

---

For the 1st part, I first tried to compute a basic mapping of the keypads 
(like going to `8` to `A` is `>vvv`), but that didn't work on example inputs.
Turned out that, for example, `<<^^` and `^^<<` might give different path lengths.
So I tried to do some DFS, tried both "edge" paths if there was
(since robots cannot go in the "empty" spot, it can reduce path count to 1 instead of 2).
That works for part 1, but doing it for part 2 is far too _complex_, both in terms of
time and space (16G was not event close to being enough memory).

---

First, I figured that it seems prioritizing `<` key for directional keypad was the _key_.
So I computed paths with the following priority: `<`, `v`, `^`, `>`,
which is from farthest to closest to `A`. With that, no more DFS (I left it in the code, though).

I tried part 2 with that, but this still took too much space (concatenating string to 25 depth was too much...). 
I have been too many days in this, so I went to search for some inspiration on the Reddit. The idea seems to be to cache 
the key sequence lengths. A sequence length depends on the sum of the lengths of the underlying sequences.
Also, a sequence can be divided in parts, each part beginning on the `A` key and ending on the `A` key.
So we can cache those lengths, then compute the length of the higher order sequence by summing the lengths of its parts,
caching the result, etc.

---

This was a really challenging problem, but, phew, thanks I'm done with it!