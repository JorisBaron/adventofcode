<?php

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$keys = [];
$locks = [];

while(!$file->eof()){
	$firstRow = $file->fgets();
	$isKey = $firstRow === "#####";

	$schematicsGrid = [];
	while(($line = $file->fgets()) !== ""){
		$schematicsGrid[] = str_split(trim($line));
	}
	array_pop($schematicsGrid); // remove last "delimiter" line

	if(!$isKey) {
		$schematicsGrid = array_reverse($schematicsGrid);
	}

	$heightMap = [0,0,0,0,0];
	foreach($schematicsGrid as $row){
		foreach($row as $colIndex => $value){
			if($value === "#"){
				$heightMap[$colIndex]++;
			}
		}
	}

	if($isKey){
		$keys[] = $heightMap;
	} else {
		$locks[] = $heightMap;
	}
}

$fitCount = 0;
foreach($keys as $key){
	foreach($locks as $lock){
		if(keyFitsLock($key, $lock, 5)){
			$fitCount++;
		}
	}
}

echo $fitCount;

/**
 * @param int[] $key
 * @param int[] $lock
 * @param int   $maxHeight
 * @return bool
 */
function keyFitsLock(array $key, array $lock, int $maxHeight): bool {
	return array_all(
		$key,
		fn(int $keyValue, int $index) => $keyValue + $lock[$index] <= $maxHeight
	);
}