<?php

namespace AoC\Year2024\Day09\Part2;

class EmptySlot
{
	public function __construct(
		public private(set) int $size,
		public private(set) int $position
	) {}

	public int $endPosition {
		get => $this->position + $this->size - 1;
	}

	public function shrink(int $size) : void {
		$this->size -= $size;
		$this->position += $size;
	}

	public function grow(int $size) : void {
		$this->size += $size;
	}
}