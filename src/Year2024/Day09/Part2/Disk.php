<?php

namespace AoC\Year2024\Day09\Part2;

use AoC\Common\DoublyLinkedList\DoublyLinkedList;
use AoC\Common\DoublyLinkedList\Node;

class Disk
{
	/** @var File[] */
	private array $files = [];
	/** @var DoublyLinkedList<EmptySlot> */
	private DoublyLinkedList $emptySlotsList;

	private int $size = 0;

	public function __construct(array $diskmapNumbers)
	{
		$this->emptySlotsList = new DoublyLinkedList();

		$isFile = true;
		$iFile = 0;
		$this->size = 0;
		foreach($diskmapNumbers as $diskmapNumber) {
			if($isFile) {
				$this->files[] = new File($iFile, $diskmapNumber, $this->size);
				$iFile++;
			} else {
				$emptySlot = new EmptySlot($diskmapNumber, $this->size);

				$this->emptySlotsList->append($emptySlot);
			}
			$isFile = !$isFile;
			$this->size += $diskmapNumber;
		}
	}



	public function getStringRepresentation(): string {
		$arr = array_fill(0, $this->size, '.');
		for($i = 0; $i < count($this->files); $i++) {
			$file = $this->files[$i];
			for($j = $file->position; $j < $file->position + $file->size; $j++) {
				$arr[$j] = $file->id;
			}
		}
		return implode("|", $arr);
	}

	/**
	 * @return File[]
	 */
	public function getFiles() : array {
		return $this->files;
	}

	/**
	 * @return DoublyLinkedList<EmptySlot>
	 */
	public function getEmptySlotsList() : DoublyLinkedList {
		return $this->emptySlotsList;
	}


	/**
	 * @param File            $file
	 * @param Node<EmptySlot> $emptySlotNode
	 * @throws \Exception
	 */
	public function moveFile(File $file, Node $emptySlotNode) : void {
		$emptySlot = $emptySlotNode->value;
		if($emptySlot->size < $file->size) {
			throw new \Exception("Empty slot is too small");
		}

		$offset = $emptySlot->position;

		if($emptySlot->size === $file->size) {
			$this->emptySlotsList->remove($emptySlotNode);
		} else {
			$emptySlotNode->value->shrink($file->size);
		}

		$file->moveTo($offset);


		// we don't care about the space left empty after the file is moved
		// it cannot be filled by another file since we are moving the files in reverse order (aka right to left)
		// and a file cannot go further right than its current position
	}


	public function getChecksum() : int {
		$checksum = 0;
		foreach($this->files as $file) {
			$checksum += $file->getChecksum();
		}
		return $checksum;
	}
}