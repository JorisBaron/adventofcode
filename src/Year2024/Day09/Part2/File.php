<?php

namespace AoC\Year2024\Day09\Part2;

class File
{
	public function __construct(
		public readonly int $id,
		public readonly int $size,
		public private(set) int $position
	) {}

	public int $endPosition {
		get => $this->position + $this->size - 1;
	}

	public function moveTo(int $offset) : void {
		$this->position = $offset;
	}

	public function getChecksum() : int {
		// checksum = id*position + id*(position+1) + id*(position+2) + ... + id*(position+size-1)
		//          = id*(position + (position+1) + (position+2) + ... + (position+size-1))
		//          = id * (size*position + (1 + 2 + ... + size-1))
		//          = id * (size*position + size*(size-1)/2)
		//            -> keep that since size*(size-1)/2) is an int
		//		    = id * size * (position + (size-1)/2)
		return $this->id * ($this->size*$this->position + ($this->size*($this->size-1)/2));
	}
}