<?php

use AoC\Year2024\Day09\DiskArray;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$line = $file->fgets();
$diskmapNumbers = array_map(intval(...), str_split($line));


$disk = new DiskArray($diskmapNumbers);

$disk->startGotoNextEmptySpot();
$disk->endGotoNextDataSpot();
while(!$disk->isCompressed()) {
	$disk->swap();
	$disk->startGotoNextEmptySpot();
	$disk->endGotoNextDataSpot();
}


echo $disk->getChecksum().PHP_EOL;