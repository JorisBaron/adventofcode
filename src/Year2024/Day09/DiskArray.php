<?php

namespace AoC\Year2024\Day09;

class DiskArray
{
	protected const EMPTY_VALUE = '.';

	private array $mappedDisk;
	private int $iStart;
	private int $iEnd;

	public function __construct(array $diskmapNumbers)
	{
		$isFile = true;
		$iFile = 0;
		$this->mappedDisk = [];
		foreach($diskmapNumbers as $diskmapNumber) {
			if($isFile) {
				$this->mappedDisk = array_merge($this->mappedDisk, array_fill(0, $diskmapNumber, $iFile));
				$iFile++;
			} else {
				$this->mappedDisk = array_merge($this->mappedDisk, array_fill(0, $diskmapNumber, self::EMPTY_VALUE));
			}
			$isFile = !$isFile;
		}

		$this->iStart = 0;
		$this->iEnd = count($this->mappedDisk) - 1;
	}

	public function isCompressed(): bool {
		return $this->iStart >= $this->iEnd;
	}

	public function startGotoNextEmptySpot() : void {
		while($this->mappedDisk[$this->iStart] !== self::EMPTY_VALUE) {
			$this->iStart++;
		}
	}

	public function endGotoNextDataSpot() : void {
		while($this->mappedDisk[$this->iEnd] === self::EMPTY_VALUE) {
			$this->iEnd--;
		}
	}

	public function swap() : void {
		$temp = $this->mappedDisk[$this->iStart];
		$this->mappedDisk[$this->iStart] = $this->mappedDisk[$this->iEnd];
		$this->mappedDisk[$this->iEnd] = $temp;
	}

	public function getChecksum() : int {
		$checksum = 0;
		for($i = 0; $i < count($this->mappedDisk); $i++) {
			$checksum += $this->mappedDisk[$i] === self::EMPTY_VALUE ? 0 : $i*$this->mappedDisk[$i];
		}
		return $checksum;
	}

	public function getMappedDisk() : array {
		return $this->mappedDisk;
	}
}