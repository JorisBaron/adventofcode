<?php

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$line = $file->fgets();
$diskmapNumbers = array_map(intval(...), str_split($line));

$disk = new \AoC\Year2024\Day09\Part2\Disk($diskmapNumbers);

foreach(array_reverse($disk->getFiles()) as $file) {
	$emptySlotsList = $disk->getEmptySlotsList();
	$emptySlot = $emptySlotsList->head;
	while($emptySlot !== null) {
		if($emptySlot->value->position > $file->position) {
			break;
		}

		if($emptySlot->value->size >= $file->size) {
			$disk->moveFile($file, $emptySlot);
			break;
		}
		$emptySlot = $emptySlot->next;
	}
}

//echo $disk->getStringRepresentation().PHP_EOL;
echo $disk->getChecksum().PHP_EOL;