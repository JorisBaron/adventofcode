<?php

namespace AoC\Year2024\Day14;

use AoC\Common\Point;

class Robot
{
	public function __construct(
		public Point $position,
		public Point $velocity
	) {}
}