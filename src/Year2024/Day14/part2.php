<?php

use AoC\Year2024\Day14\Robot;
use AoC\Common\Point;
use AoC\Common\Grid;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$height = 103;
$width = 101;
$rounds = 10000;

/** @var Robot[] $robots */
$robots = [];

const ROBOT_REGEX = '/^p=(-?\d+),(-?\d+) v=(-?\d+),(-?\d+)$/';
while (!$file->eof()) {
	$line = $file->fgets();
	preg_match(ROBOT_REGEX, $line, $matches);
	$robots[] = new Robot(
		new Point((int)$matches[1], (int)$matches[2]),
		new Point((int)$matches[3], (int)$matches[4])
	);
}


for($i = 0; $i < $rounds; $i++) {
	processRound($width, $height, $robots, $i);
}


/**
 * @param Robot[] $robots
 */
function processRound(int $width, int $height, array $robots, int $i) : bool {
	$positions = [];

	foreach($robots as $robot) {
		$finalPosition = $robot->position->add($robot->velocity->multiply($i));
		// modulo to bring back to the grid, and then add the height/width to make sure it's positive
		$finalPosition = new Point(
			(($finalPosition->x % $width) +$width ) % $width,
			(($finalPosition->y % $height)+$height) % $height
		);

		if (!isset($positions[$finalPosition->toString()])) {
			$positions[$finalPosition->toString()] = [$finalPosition, 0];
		} else {
			$positions[$finalPosition->toString()][1]++;
		}
	}

	$grid = Grid::fromDimensions($width, $height,".");
	foreach($positions as $data) {
		[$point,] = $data;
		$grid->set($point, '#');
	}

	ob_start();
	$grid->display();
	$gridString = ob_get_contents();
	ob_end_clean();
	if (str_contains($gridString, "#########") !== false) {
		$grid->display();
		echo "Check round $i\n";
		return true;
	}

	return false;
}
