<?php

use AoC\Year2024\Day14\Robot;
use AoC\Common\Point;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$height = 103;
$width = 101;
$rounds = 100;

/** @var Robot[] $robots */
$robots = [];

const ROBOT_REGEX = '/^p=(-?\d+),(-?\d+) v=(-?\d+),(-?\d+)$/';
while (!$file->eof()) {
	$line = $file->fgets();
	preg_match(ROBOT_REGEX, $line, $matches);
	$robots[] = new Robot(
		new Point((int)$matches[1], (int)$matches[2]),
		new Point((int)$matches[3], (int)$matches[4])
	);
}


$quadrantCount = [
	'NW' => 0,
	'NE' => 0,
	'SW' => 0,
	'SE' => 0
];

foreach($robots as $robot) {
	$finalPosition = $robot->position->add($robot->velocity->multiply($rounds));
	// modulo to bring back to the grid, and then add the height/width to make sure it's positive
	$finalPosition = new Point(
		(($finalPosition->x % $width) +$width ) % $width,
		(($finalPosition->y % $height)+$height) % $height
	);

	$quadrant = getQuadrant($finalPosition->x, $finalPosition->y, $width, $height);
	if ($quadrant !== null) {
		$quadrantCount[$quadrant]++;
	}
}


echo array_product($quadrantCount) . PHP_EOL;


function getQuadrant(int $x, int $y, int $width, int $height): ?string {
	$quadrantVerticalSeparation = intdiv($width-1, 2);
	$quadrantHorizontalSeparation = intdiv($height-1, 2);

	if ($x === $quadrantVerticalSeparation || $y === $quadrantHorizontalSeparation) {
		return null;
	}

	return ($y < $quadrantHorizontalSeparation ? 'N' : 'S') .
	       ($x < $quadrantVerticalSeparation ? 'W' : 'E');
}
