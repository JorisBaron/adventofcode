<?php

namespace AoC\Year2024\Day24\Part2\Swaps;

use AoC\Common\StringIntMap;
use AoC\Year2024\Day24\Circuit\Wire;
use AoC\Year2024\Day24\Circuit\Gate;

interface Swap
{
	/**
	 * @param StringIntMap<string,Wire>                      $wires
	 * @param array<string,array<string,array<string,Gate>>> $wireToGates
	 * @param int                                            $bit
	 * @return bool
	 */
	public function check(StringIntMap $wires, array $wireToGates, int $bit): bool;

	/**
	 * @param StringIntMap<string,Wire>                      $wires
	 * @param array<string,array<string,array<string,Gate>>> $wireToGates
	 * @param int                                            $bit
	 * @return array{Wire,Wire}
	 */
	public function wiresToSwap(StringIntMap $wires, array $wireToGates, int $bit): array;
}