<?php

namespace AoC\Year2024\Day24\Part2\Swaps;

use AoC\Common\StringIntMap;

class XXorYWithXAndY implements Swap
{
	public function check(StringIntMap $wires, array $wireToGates, int $bit) : bool {
		$xWireName = sprintf('x%02d', $bit);
		$yWireName = sprintf('y%02d', $bit);
		$xor = $wireToGates[$xWireName][$yWireName]['XOR'];
		return count($xor->outputWire->getOutputGates()) === 1;
	}

	public function wiresToSwap(StringIntMap $wires, array $wireToGates, int $bit) : array {
		$xWireName = sprintf('x%02d', $bit);
		$yWireName = sprintf('y%02d', $bit);
		return [
			$wireToGates[$xWireName][$yWireName]['XOR']->outputWire,
			$wireToGates[$xWireName][$yWireName]['AND']->outputWire,
		];
	}
}