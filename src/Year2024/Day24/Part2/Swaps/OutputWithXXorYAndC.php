<?php

namespace AoC\Year2024\Day24\Part2\Swaps;

use AoC\Year2024\Day24\Part2\Swaps\Swap;
use AoC\Common\StringIntMap;
use AoC\Year2024\Day24\Circuit\Gate;

class OutputWithXXorYAndC implements Swap
{
	/**
	 * @inheritDoc
	 */
	public function check(StringIntMap $wires, array $wireToGates, int $bit) : bool {
		/** @var Gate $outputGate */
		$outputGate = $wires[sprintf('z%02d', $bit)]->inputGate;
		// Output gate is an AND gate and its inputs are not x and y
		return $outputGate->getType() === "AND" && !preg_match('/^[xy]/', $outputGate->inputWire1->name);
	}

	/**
	 * @inheritDoc
	 */
	public function wiresToSwap(StringIntMap $wires, array $wireToGates, int $bit) : array {
		/** @var Gate $secondXor */
		$secondXorGate = array_values(array_filter(
			$wireToGates[sprintf('x%02d', $bit)][sprintf('y%02d', $bit)]['XOR']->outputWire->getOutputGates(),
			fn(Gate $gate) => $gate->getType() === "XOR"
		))[0];
		return [
			$wires[sprintf('z%02d', $bit)],
			$secondXorGate->outputWire,
		];
	}
}