<?php

namespace AoC\Year2024\Day24\Part2\Swaps;

use AoC\Year2024\Day24\Circuit\Wire;
use AoC\Year2024\Day24\Circuit\Gate;
use AoC\Common\StringIntMap;

class OutputWithCarry implements Swap
{

	/**
	 * @inheritDoc
	 */
	public function check(StringIntMap $wires, array $wireToGates, int $bit) : bool {
		return $wires[sprintf('z%02d', $bit)]->inputGate->getType() === "OR";
	}

	/**
	 * @inheritDoc
	 */
	public function wiresToSwap(StringIntMap $wires, array $wireToGates, int $bit) : array {
		/** @var Gate $secondXor */
		$secondXorGate = array_values(array_filter(
			$wireToGates[sprintf('x%02d', $bit)][sprintf('y%02d', $bit)]['XOR']->outputWire->getOutputGates(),
			fn(Gate $gate) => $gate->getType() === "XOR"
		))[0];
		return [
			$wires[sprintf('z%02d', $bit)],
			$secondXorGate->outputWire,
		];
	}
}