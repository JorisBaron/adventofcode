<?php

namespace AoC\Year2024\Day24\Circuit;

use AoC\Common\StringIntMap;

class Simulation
{
	/**
	 * @param StringIntMap<string,Wire> $wires
	 */
	public function __construct(
		private(set) StringIntMap $wires,
	) {}


	/**
	 * @param StringIntMap<string,Wire> $inputs
	 */
	public function simulate(StringIntMap $inputs) : void {
		foreach($inputs as $name => $value) {
			$this->wires[$name]->setValue($value);
		}
	}

	public function reset() : void {
		foreach(array_filter($this->wires->toArray(), fn(Wire $wire) => preg_match('/^[xy]/', $wire->name)) as $inputWire) {
			$inputWire->setValue(null);
		}
	}

	/**
	 * @return StringIntMap<string,Wire>
	 */
	public function getOutputWires() : StringIntMap {
		return StringIntMap::fromArray(array_filter(
			$this->wires->toArray(),
			fn(Wire $wire) => str_starts_with($wire->name, 'z')
		));
	}
}