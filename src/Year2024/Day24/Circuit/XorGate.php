<?php

namespace AoC\Year2024\Day24\Circuit;

class XorGate extends Gate
{
	protected function calculateValue() : void {
		$this->value = ($this->inputWire1->value xor $this->inputWire2->value);
	}

	public function getType() : string {
		return 'XOR';
	}
}