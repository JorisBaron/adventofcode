<?php

namespace AoC\Year2024\Day24\Circuit;

use SplObserver;
use SplSubject;

class Wire
{
	protected(set) ?bool $value = null;
	public ?Gate $inputGate = null;
	/** @var Gate[] */
	protected array $outputGates = [];

	public function __construct(
		public readonly string $name,
	) {}

	public function setValue(?bool $value) : void {
		$this->value = $value;
		$this->notify();
	}

	/**
	 * @param Gate $inputGate
	 * @return void
	 */
	public function update(Gate $inputGate) : void {
		$this->value = $inputGate->value;
		$this->notify();
	}

	/**
	 * @param Gate $outputGate
	 * @return void
	 */
	public function attach(Gate $outputGate) : void {
		$this->outputGates[$outputGate->name] = $outputGate;
	}

	/**
	 * @param Gate $outputGate
	 * @return void
	 */
	public function detach(Gate $outputGate) : void {
		unset($this->outputGates[$outputGate->name]);
	}

	/**
	 * @return void
	 */
	public function notify() : void {
		foreach ($this->outputGates as $gate) {
			$gate->update();
		}
	}

	public function getOutputGates() : array {
		return $this->outputGates;
	}
}