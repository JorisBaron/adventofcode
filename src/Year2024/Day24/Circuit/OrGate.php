<?php

namespace AoC\Year2024\Day24\Circuit;

use AoC\Year2024\Day24\Circuit\Gate;

class OrGate extends Gate
{
	protected function calculateValue() : void {
		$this->value = $this->inputWire1->value || $this->inputWire2->value;
	}

	public function getType() : string {
		return 'OR';
	}
}