<?php

namespace AoC\Year2024\Day24\Circuit;

abstract class Gate
{
	public ?bool $value = null;

	public function __construct(
		public readonly string $name,
		public readonly Wire $inputWire1,
		public readonly Wire $inputWire2,
		public Wire $outputWire,
	) {}

	public function update() : void {
		if($this->inputWire1->value === null || $this->inputWire2->value === null) {
			$this->value = null;
		} else {
			$this->calculateValue();
		}
		$this->notify();
	}


	public function notify() : void {
		$this->outputWire->update($this);
	}


	protected abstract function calculateValue() : void;
	public abstract function getType() : string;
}