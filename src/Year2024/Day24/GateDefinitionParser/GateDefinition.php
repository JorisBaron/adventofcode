<?php

namespace AoC\Year2024\Day24\GateDefinitionParser;

readonly class GateDefinition
{
	public function __construct(
		public string $gateType,
		public string $input1,
		public string $input2,
		public string $output,
	) { }
}