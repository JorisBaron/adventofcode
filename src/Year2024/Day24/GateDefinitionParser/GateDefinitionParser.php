<?php

namespace AoC\Year2024\Day24\GateDefinitionParser;

class GateDefinitionParser
{
	protected const string GATE_DEFINITION_REGEX = '/^(\w+)\s*(\w+)\s*(\w+)\s*->\s*(\w+)$/';

	public function parse(string $definition) : GateDefinition {
		if (!preg_match(self::GATE_DEFINITION_REGEX, $definition, $matches)) {
			throw new \InvalidArgumentException("Invalid gate definition: $definition");
		}

		return new GateDefinition($matches[2], $matches[1], $matches[3], $matches[4]);
	}
}