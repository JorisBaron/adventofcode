Oh wow...

---

Part 1 was pretty easy and fun actually. It was similar to a puzzle from last year.
Coding the circuitry with the gates, wires and all was fun.

---

Part 2........ Yeah.

I understood quickly that the whole circuit was a binary adder, composed of "full adder" modules,
except for the first bit (which is a half adder). But I struggled on "how to detect swaps to do".
I first tried to be as generic as possible about which wires could be swapped 
and tried to find a way to detect which was the other wire to swap when
we have the first faulty one. However, it got telly complicated really quickly.

I scrolled a bit on the subreddit and that made me realize that, in fact, to avoid loops,
we have a limited amount of possible swaps.

I took the 
[full adder schematic](https://media.geeksforgeeks.org/wp-content/uploads/digital_Logic2.png) 
(from [here](https://www.geeksforgeeks.org/carry-look-ahead-adder/)), and tried to swap every output wire with each other.
That resulted in 3 possible swaps:

* `x AND y` output with `x XOR y` output
* `z` with the outgoing `carry`
* `z` with `x AND y`
* `z` with `(x XOR y) AND c` output (where `c` is the in-carry)

I also assumed that swaps could only occur inside an adder module.
I cannot demonstrate it, but I'm pretty sure that otherwise it create loops.

Finally, I implemented those 4 swaps and their respective detection function.
For each adder, we just check for every swap, and if one is needed, we do it and reset the simulation.
We and up with the swaps array which we can sort and output!

---

What a journey!
