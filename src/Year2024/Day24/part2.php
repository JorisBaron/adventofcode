<?php

use AoC\Common\StringIntMap;
use AoC\Year2024\Day24\Circuit\Gate;
use AoC\Year2024\Day24\Circuit\Wire;
use AoC\Year2024\Day24\Factory\GateFactory;
use AoC\Year2024\Day24\Circuit\AndGate;
use AoC\Year2024\Day24\Circuit\OrGate;
use AoC\Year2024\Day24\Circuit\XorGate;
use AoC\Year2024\Day24\GateDefinitionParser\GateDefinitionParser;
use AoC\Year2024\Day24\Circuit\Simulation;
use AoC\Year2024\Day24\Part2\Swaps\{Swap, XXorYWithXAndY, OutputWithCarry, OutputWithXAndY, OutputWithXXorYAndC};

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

/** @var StringIntMap<string,Wire> $wires */
$wires = new StringIntMap();
/** @var StringIntMap<string,Gate> $gates */
$gates = new StringIntMap();
/** @var StringIntMap<string,bool> $inputs */
$inputs = new StringIntMap();


$gateFactory = new GateFactory();
$gateFactory->registerGateType('AND', AndGate::class);
$gateFactory->registerGateType('OR', OrGate::class);
$gateFactory->registerGateType('XOR', XorGate::class);

$gateDefinitionParser = new GateDefinitionParser();

/** @var Swap[] $swapOptions */
$swapOptions = [
	new XXorYWithXAndY(),
	new OutputWithCarry(),
	new OutputWithXAndY(),
	new OutputWithXXorYAndC()
];




while(($line = $file->fgets()) !== "") {
	[$name, $value] = explode(": ", $line);
	$inputs->add($name, $value==='1');
}


while(!$file->eof()) {
	$line = $file->fgets();
	$gateDefinition = $gateDefinitionParser->parse($line);
	if(!$wires->has($gateDefinition->input1)) {
		$wires->add($gateDefinition->input1, new Wire($gateDefinition->input1));
	}
	if(!$wires->has($gateDefinition->input2)) {
		$wires->add($gateDefinition->input2, new Wire($gateDefinition->input2));
	}
	if(!$wires->has($gateDefinition->output)) {
		$wires->add($gateDefinition->output, new Wire($gateDefinition->output));
	}

	$gate = $gateFactory->createGate(
		$gateDefinition->gateType,
		$line,
		$wires[$gateDefinition->input1],
		$wires[$gateDefinition->input2],
		$wires[$gateDefinition->output]
	);
	$gates->add($gate->name, $gate);
}

$wireToGates = generateWireToGateMapping($gates->toArray());

$simulation = new Simulation($wires);
$simulation->simulate($inputs);



$swappedWireNames = [];

$i=1;
$carryValue = $inputs['x00'] && $inputs['y00'];

while($wires->has($xWire = sprintf('x%02d', $i))) {
	$yWire = sprintf('y%02d', $i);

	$swapped = false;
	foreach($swapOptions as $swapOption) {
		if($swapOption->check($wires, $wireToGates, $i)) {
			$swapped = true;
			$simulation->reset();
			[$wire1, $wire2] = $swapOption->wiresToSwap($wires, $wireToGates, $i);
			swapWiresInputs($wire1, $wire2);
			$swappedWireNames[] = $wire1->name;
			$swappedWireNames[] = $wire2->name;
			break;
		}
	}

	if(!$swapped) {
		$i++;
		$carryValue = ($inputs[$xWire] && $inputs[$yWire]) || ($carryValue && ($inputs[$xWire] xor $inputs[$yWire]));
		continue;
	}

	//reset Loop
	$wireToGates = generateWireToGateMapping($gates->toArray());

	$simulation->simulate($inputs);

	$carryValue = $inputs['x00'] && $inputs['y00'];

	$i=1;
}

sort($swappedWireNames);
echo implode(',', $swappedWireNames).PHP_EOL;


/**
 * @param Gate[] $gates
 * @return array<string,array<string,array<string,Gate>>>
 */
function generateWireToGateMapping(array $gates) : array {
	$wireToGates = [];
	foreach($gates as $gate) {
		$wireToGates[$gate->inputWire1->name][$gate->inputWire2->name][$gate->getType()] = $gate;
		$wireToGates[$gate->inputWire2->name][$gate->inputWire1->name][$gate->getType()] = $gate;
	}
	return $wireToGates;
}


function swapWiresInputs(Wire $wire1, Wire $wire2) : void {
	$wire1Input = $wire1->inputGate;
	$wire2Input = $wire2->inputGate;

	$wire1->inputGate = $wire2Input;
	$wire2->inputGate = $wire1Input;

	$wire1Input->outputWire = $wire2;
	$wire2Input->outputWire = $wire1;
}