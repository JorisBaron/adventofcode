<?php

use AoC\Common\StringIntSet;
use AoC\Common\StringIntMap;
use AoC\Year2024\Day24\Circuit\Gate;
use AoC\Year2024\Day24\Circuit\Wire;
use AoC\Year2024\Day24\Factory\GateFactory;
use AoC\Year2024\Day24\Circuit\AndGate;
use AoC\Year2024\Day24\Circuit\OrGate;
use AoC\Year2024\Day24\Circuit\XorGate;
use AoC\Year2024\Day24\GateDefinitionParser\GateDefinitionParser;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

/** @var StringIntMap<string,Wire> $wires */
$wires = new StringIntMap();
/** @var StringIntMap<string,bool> $inputs */
$inputs = new StringIntMap();
/** @var StringIntMap<string,Gate> $gates */
$gates = new StringIntMap();


$gateFactory = new GateFactory();
$gateFactory->registerGateType('AND', AndGate::class);
$gateFactory->registerGateType('OR', OrGate::class);
$gateFactory->registerGateType('XOR', XorGate::class);

$gateDefinitionParser = new GateDefinitionParser();


while(($line = $file->fgets()) !== "") {
	[$name, $value] = explode(": ", $line);
	$inputs->add($name, $value==='1');
}

while(!$file->eof()) {
	$line = $file->fgets();
	$gateDefinition = $gateDefinitionParser->parse($line);
	if(!$wires->has($gateDefinition->input1)) {
		$wires->add($gateDefinition->input1, new Wire($gateDefinition->input1));
	}
	if(!$wires->has($gateDefinition->input2)) {
		$wires->add($gateDefinition->input2, new Wire($gateDefinition->input2));
	}
	if(!$wires->has($gateDefinition->output)) {
		$wires->add($gateDefinition->output, new Wire($gateDefinition->output));
	}

	$wire1 = $wires[$gateDefinition->input1];
	$wire2 = $wires[$gateDefinition->input2];

	$gate = $gateFactory->createGate(
		$gateDefinition->gateType,
		$line,
		$wire1,
		$wire2,
		$wires[$gateDefinition->output]
	);
	$gates->add($gate->name, $gate);
}

foreach($inputs as $name => $value) {
	$wires[$name]->setValue($value);
}


$outputWires = array_filter($wires->toArray(), fn(Wire $wire) => str_starts_with($wire->name, 'z'));
usort($outputWires, fn(Wire $a, Wire $b) => $b->name <=> $a->name);
$result = 0;
foreach($outputWires as $wire) {
	$result <<= 1;
	$result += $wire->value ? 1 : 0;
}

echo $result.PHP_EOL;