<?php

namespace AoC\Year2024\Day24\Factory;

use AoC\Year2024\Day24\Circuit\Gate;
use AoC\Year2024\Day24\Circuit\Wire;


class GateFactory
{
	/**
	 * @var array<string, class-string<Gate>>
	 */
	protected array $registeredGateTypes = [];

	public function registerGateType(string $type, string $class) : void {
		$this->registeredGateTypes[$type] = $class;
	}

	public function createGate(string $type, string $name, Wire $input1, Wire $input2, Wire $output) : Gate {
		$class = $this->registeredGateTypes[$type];
		$gate = new $class($name, $input1, $input2, $output);
		$input1->attach($gate);
		$input2->attach($gate);
		$output->inputGate = $gate;
		return $gate;
	}
}