<?php

use AoC\Common\Grid;
use AoC\Common\Point;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$input = [];
foreach ($file as $line) {
	$input[] = $line;
}

/** @var Grid<string> $grid */
$grid = Grid::createFromInput(iterator_to_array($file));


$oppositeCorner = [
	'M' => 'S',
	'S' => 'M'
];

$count = 0;
for($rowI = 1; $rowI < $grid->getHeight()-1; $rowI++) {
	for($colI = 1; $colI < $grid->getWidth()-1; $colI++) {
		$cellPoint = new Point($colI, $rowI);
		if($grid->get($cellPoint) !== 'A') {
			continue;
		}

		$nw = $grid->get($cellPoint->add(new Point(-1, -1)));
		$ne = $grid->get($cellPoint->add(new Point( 1, -1)));
		$sw = $grid->get($cellPoint->add(new Point(-1,  1)));
		$se = $grid->get($cellPoint->add(new Point( 1,  1)));
		if(isset($oppositeCorner[$nw]) && $oppositeCorner[$nw] === $se &&
		   isset($oppositeCorner[$ne]) && $oppositeCorner[$ne] === $sw
		) {
			$count++;
		}
	}
}

echo $count;