<?php

use AoC\Common\Grid;
use AoC\Common\Point;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$input = [];
foreach ($file as $line) {
	$input[] = $line;
}

/** @var Grid<string> $grid */
$grid = Grid::createFromInput(iterator_to_array($file));


$directions = [
	new Point( 1, -1),
	new Point( 0, -1),
	new Point(-1, -1),
	new Point( 1,  1),
	new Point( 0,  1),
	new Point(-1,  1),
	new Point( 1,  0),
	new Point(-1,  0),
];

$count = 0;
for($rowI = 0; $rowI < $grid->getHeight(); $rowI++) {
	for($colI = 0; $colI < $grid->getWidth(); $colI++) {
		$cellPoint = new Point($colI, $rowI);
		if($grid->get($cellPoint) !== 'X') {
			continue;
		}

		foreach($directions as $direction) {
			$next = $cellPoint->add($direction);
			if($grid->isInBound($next) && $grid->get($next) !== 'M') {
				continue;
			}

			if(checkXmas($grid, $cellPoint, $direction)) {
				$count++;
			}
		}
	}
}

echo $count;

/**
 * @param Grid<string> $grid
 * @param Point $point
 * @param Point $direction
 * @return bool
 */
function checkXmas(Grid $grid, Point $point, Point $direction): bool {
	try {
		return //grid->get($point) === 'X' &&
		       //$grid->get($point->add($direction)) === 'M' &&
		       $grid->get($point->add($direction->multiply(2))) === 'A' &&
		       $grid->get($point->add($direction->multiply(3))) === 'S';
	} catch(\OutOfBoundsException $e) {
		return false;
	}
}