<?php

use AoC\Common\Grid;
use AoC\Common\Point;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

/** @var Grid<int> $grid */
$grid = new Grid(array_map(
	fn($line) => array_map(intval(...), str_split($line)), iterator_to_array($file)
));
/** @var Grid<?Point[]> $trailheadTracker */
$trailheadTracker = Grid::fromDimensions($grid->getWidth(), $grid->getHeight(), null);

$trailheadSum = 0;
for($y = 0; $y < $grid->getHeight(); $y++) {
	for($x = 0; $x < $grid->getWidth(); $x++) {
		$p = new Point($x, $y);
		if($grid->get($p) === 0) {
			$trailheadSum += calculateTrailheadScore($grid, $trailheadTracker, $p);
		}
	}
}

echo $trailheadSum;

/**
 * @param Grid<int> $grid
 * @param Grid<?int> $trailheadCounterGrid
 * @param Point $point
 * @return int
 */
function calculateTrailheadScore(Grid $grid, Grid $trailheadCounterGrid, Point $point) : int {
	if($trailheadCounterGrid->get($point) !== null) {
		return $trailheadCounterGrid->get($point);
	}

	if($grid->get($point) === 9) {
		$trailheadCounterGrid->set($point, 1);
		return 1;
	}

	$higherNeighbours =
		array_filter($grid->getAdjacents($point), fn($neighbour) => $grid->get($neighbour) === $grid->get($point) + 1);
	$trailhead = array_reduce(
		$higherNeighbours,
		fn($acc, $neighbour) => $acc + calculateTrailheadScore($grid, $trailheadCounterGrid, $neighbour),
		0
	);

	$trailheadCounterGrid->set($point, $trailhead);
	return $trailhead;
}
