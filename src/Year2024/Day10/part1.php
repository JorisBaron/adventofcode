<?php

use AoC\Common\Grid;
use AoC\Common\Point;
use AoC\Common\StringableStorage;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

/** @var Grid<int> $grid */
$grid = new Grid(
	array_map(
		fn($line) => array_map(intval(...), str_split($line)),
		iterator_to_array($file)
	)
);
/** @var Grid<?Point[]> $trailheadTracker */
$trailheadTracker = Grid::fromDimensions($grid->getWidth(), $grid->getHeight(), null);

$trailheadSum = 0;
for($y = 0; $y < $grid->getHeight(); $y++) {
	for($x = 0; $x < $grid->getWidth(); $x++) {
		$p = new Point($x, $y);
		if($grid->get($p) === 0) {
			$trailheadSum += calculateTrailheadTrack($grid, $trailheadTracker, $p)->count();
		}
	}
}

echo $trailheadSum;

/**
 * @param Grid<int> $grid
 * @param Grid<?Point[]> $trailheadCounterGrid
 * @param Point $point
 * @return StringableStorage
 */
function calculateTrailheadTrack(Grid $grid, Grid $trailheadCounterGrid, Point $point) : StringableStorage {
	if($trailheadCounterGrid->get($point) !== null) {
		return $trailheadCounterGrid->get($point);
	}

	if($grid->get($point) === 9) {
		$store = new StringableStorage();
		$store->attach($point);
		$trailheadCounterGrid->set($point, $store);
		return $store;
	}

	$higherNeighbours =
		array_filter($grid->getAdjacents($point), fn($neighbour) => $grid->get($neighbour) === $grid->get($point) + 1);
	$trailheadTrack = array_reduce(
		$higherNeighbours,
		function(StringableStorage $storage, Point $neighbour) use ($grid, $trailheadCounterGrid) {
			$neighbourStorage = calculateTrailheadTrack($grid, $trailheadCounterGrid, $neighbour);
			$storage->addAll($neighbourStorage);
			return $storage;
		},
		new StringableStorage()
	);

	$trailheadCounterGrid->set($point, $trailheadTrack);
	return $trailheadTrack;
}