<?php

namespace AoC\Common;

readonly class ParametricEquation
{
	public function __construct(
		public Vector $p,
		public Vector $v
	) {}
}