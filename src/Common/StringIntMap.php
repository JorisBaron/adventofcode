<?php

namespace AoC\Common;

use ArrayAccess;
use Countable;
use Traversable;
use IteratorAggregate;
use ArrayIterator;
use InvalidArgumentException;

/**
 * @template TKey of string|int
 * @template TItem
 */
class StringIntMap implements ArrayAccess, Countable, IteratorAggregate
{
	/**
	 * @var array<TKey, TItem>
	 */
	protected array $data = [];

	/**
	 * @param array<TKey, TItem> $data
	 * @return static<TKey, TItem>
	 */
	public static function fromArray(array $data) : static {
		$map = new self();
		$map->data = $data;
		return $map;
	}


	/**
	 * @param TKey  $key
	 * @param TItem $value
	 */
	public function add(mixed $key, mixed $value) : void {
		$this->data[$key] = $value;
	}

	public function addAll(iterable $data) : void {
		foreach($data as $key => $value) {
			$this->add($key, $value);
		}
	}

	/**
	 * @param TKey $key
	 * @return TItem|null
	 */
	public function remove(mixed $key) {
		$value = $this->data[$key] ?? null;
		unset($this->data[$key]);
		return $value;
	}

	public function removeAll(iterable $data) : void {
		foreach($data as $key => $value) {
			$this->remove($key);
		}
	}

	/**
	 * @param TKey $key
	 */
	public function has(mixed $key) : bool {
		return isset($this->data[$key]);
	}

	/**
	 * @return TItem|null
	 */
	public function getFirst() {
		return $this->data[array_key_first($this->data)];
	}

	/**
	 * @return TItem|null
	 */
	public function shift() {
		$firstKey = array_key_first($this->data);
		return $this->remove($firstKey);
	}

	/**
	 * @return array<TKey, TItem>
	 */
	public function toArray() : array {
		return $this->data;
	}

	/**
	 * @param TKey $offset
	 * @return bool
	 */
	public function offsetExists(mixed $offset) : bool {
		return isset($this->data[$offset]);
	}

	/**
	 * @param TKey $offset
	 * @return TItem
	 */
	public function offsetGet(mixed $offset) : mixed {
		return $this->data[$offset];
	}

	/**
	 * @param TKey $offset
	 * @param TItem $value
	 * @throws InvalidArgumentException
	 * @return void
	 */
	public function offsetSet($offset, $value) : void {
		if (is_null($offset)) {
			throw new InvalidArgumentException('Offset cannot be null');
		}
		$this->data[$offset] = $value;
	}

	/**
	 * @param TKey $offset
	 * @return void
	 */
	public function offsetUnset(mixed $offset) : void {
		unset($this->data[$offset]);
	}

	public function count() : int {
		return count($this->data);
	}

	/**
	 * @return Traversable<TKey,TItem>
	 */
	public function getIterator() : Traversable {
		return new ArrayIterator($this->toArray());
	}
}