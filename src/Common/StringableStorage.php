<?php

namespace AoC\Common;

use SplObjectStorage;
use Stringable;

/**
 * @template TObject of Stringable
 * @template TValue
 * @extends SplObjectStorage<TObject, TValue>
 */
class StringableStorage extends SplObjectStorage
{
	/**
	 * @param object $object
	 * @return string
	 */
	public function getHash(object $object) : string {
		return (string)$object;
	}

	public function __toString() : string {
		$str = 'StringableStorage:(';
		foreach($this as $key){
			$str.=sprintf("{%s:%s}", $key, $this[$key]);
		}
		return $str.")";
	}
}