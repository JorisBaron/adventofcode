<?php

namespace AoC\Common;

use Traversable;
use Override;
use ArrayIterator;
use IteratorAggregate;
use ArrayAccess;

class Vector implements IteratorAggregate, ArrayAccess
{
	/** @var float[] */
	protected array $values;

	public function __construct(...$values) {
		if(is_array($values[0])){
			$this->values = $values[0];
		} else {
			$this->values = $values;
		}
	}

	#[Override]
	public function offsetExists(mixed $offset) : bool {
		return isset($this->values[$offset]);
	}

	#[Override]
	public function offsetGet(mixed $offset) : mixed {
		return $this->values[$offset];
	}

	#[Override]
	public function offsetSet(mixed $offset, mixed $value) : void {}

	#[Override]
	public function offsetUnset(mixed $offset) : void {}

	#[Override]
	public function getIterator() : Traversable {
		return new ArrayIterator($this->values);
	}
}