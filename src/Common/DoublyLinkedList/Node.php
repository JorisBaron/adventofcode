<?php

namespace AoC\Common\DoublyLinkedList;

/**
 * @template T
 */
class Node
{
	/**
	 * @var T
	 */
	public mixed $value;

	/**
	 * @var Node<T>|null
	 */
	public ?Node $prev;

	/**
	 * @var Node<T>|null
	 */
	public ?Node $next;

	/**
	 * @param T $value
	 * @param Node<T>|null $prev
	 * @param Node<T>|null $next
	 */
	public function __construct($value, ?Node $prev = null, ?Node $next = null)
	{
		$this->value = $value;
		$this->prev = $prev;
		$this->next = $next;
	}

	public function remove() : void {
		if($this->prev !== null) {
			$this->prev->next = $this->next;
		}
		if($this->next !== null) {
			$this->next->prev = $this->prev;
		}
	}
}