<?php

namespace AoC\Common\DoublyLinkedList;

/**
 * @template T
 */
class DoublyLinkedList
{
	/**
	 * @var Node<T>|null
	 */
	public private(set) ?Node $head = null;

	/**
	 * @var Node<T>|null
	 */
	public private(set) ?Node $tail = null;


	/**
	 * @param array    $array
	 * @param int      $start
	 * @param int|null $count
	 * @return DoublyLinkedList
	 */
	public static function fromArray(array $array, int $start = 0, ?int $count = null) : static {
		if($count === null) {
			$count = count($array)-$start;
		}

		$list = new static();
		for($i = $start; $i < $start+$count; $i++) {
			$list->append($array[$i]);
		}
		return $list;
	}

	public static function fromNode(Node $node, ?int $count = null) : static {
		$list = new static();
		$current = $node;
		for($i = 0; $i < $count; $i++) {
			$list->append($current->value);
			$current = $current->next;
		}
		return $list;
	}


	/**
	 * @param T $value
	 * @return Node<T>
	 */
	public function append($value) : Node {
		$newNode = new Node($value, prev: $this->tail);
		if($this->head === null) {
			$this->head = $newNode;
			$this->tail = $newNode;
			return $newNode;
		}

		$this->tail->next = $newNode;
		$this->tail = $newNode;

		return $newNode;
	}

	/**
	 * @param T $value
	 * @return Node<T>
	 */
	public function prepend($value) : Node {
		$newNode = new Node($value, next: $this->head);
		if($this->head === null) {
			$this->head = $newNode;
			$this->tail = $newNode;
			return $newNode;
		}

		$this->head->prev = $newNode;
		$this->head = $newNode;
		return $newNode;
	}

	public function insertAfter(Node $node, $value) : Node {
		$newNode = new Node($value, $node, $node->next);
		if($node->next !== null) {
			$node->next->prev = $newNode;
		} else {
			$this->tail = $newNode;
		}
		$node->next = $newNode;
		return $newNode;
	}

	/**
	 * @param Node<T> $node
	 */
	public function remove(Node $node) : void {
		if($node === $this->head) {
			$this->head = $node->next;
		}
		if($node === $this->tail) {
			$this->tail = $node->prev;
		}
		$node->remove();
	}

	/**
	 * @return T[]
	 */
	public function toArray() : array {
		$array = [];
		$current = $this->head;
		while($current !== null) {
			$array[] = $current->value;
			$current = $current->next;
		}
		return $array;
	}
}