<?php

namespace AoC\Common;

readonly class Point implements \Stringable
{
	public function __construct(
		public int $x,
		public int $y,
	) {}

	public function add(self $point) : static {
		return new static(
			$this->x + $point->x,
			$this->y + $point->y,
		);
	}

	public function subtract(self $point) : static {
		return new static(
			$this->x - $point->x,
			$this->y - $point->y,
		);
	}

	public function multiply(int $value) : static {
		return new static(
			$this->x * $value,
			$this->y * $value,
		);
	}

	public function equals(self $point) : bool {
		return $this->x === $point->x && $this->y === $point->y;
	}

	public function manhattanDistance(self $p) : int {
		return abs($this->x - $p->x) + abs($this->y - $p->y);
	}

	public function toString() : string {
		return $this->__toString();
	}

	public function __toString() : string {
		return $this->x.','.$this->y;
	}
}