<?php

namespace AoC\Common;

use Override;
use Traversable;
use IteratorAggregate;
use ArrayIterator;

/**
 * @template T
 * @template U
 */
readonly class Pair implements IteratorAggregate
{
	/**
	 * @param T $key
	 * @param U $value
	 */
	public function __construct(
		public mixed $key,
		public mixed $value,
	) {}

	#[Override]
	public function getIterator() : Traversable {
		return new ArrayIterator($this);
	}
}