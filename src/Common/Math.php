<?php

namespace AoC\Common;

class Math
{
	public static function gcd(int $a, int $b) : int {
		if($b > $a){
			[$a,$b] = [$b,$a];
		}

		if($b === 0) {
			return $a;
		}

		return self::gcd($b, $a % $b);
	}

	public static function lcm(int ...$values) : int {
		$lcm = 1;
		foreach($values as $value) {
			$lcm = $lcm*$value / self::gcd($lcm, $value);
		}

		return $lcm;
	}
}