<?php

namespace AoC\Common;

use ArrayAccess;
use Countable;
use Traversable;
use IteratorAggregate;
use ArrayIterator;

/**
 * @template T of string|int
 */
class StringIntSet implements ArrayAccess, Countable, IteratorAggregate
{
	/**
	 * @var array<T, true>
	 */
	protected array $data = [];


	/**
	 * @param iterable $keys
	 * @return static
	 */
	public static function fromIterable(iterable $keys): static {
		$set = new static();
		$set->addAll($keys);
		return $set;
	}


	/**
	 * @param T $key
	 */
	public function add(mixed $key) : void {
		$this->data[$key] = $key;
	}

	public function addAll(iterable $keys) : void {
		foreach($keys as $key) {
			$this->add($key);
		}
	}

	/**
	 * @param T $key
	 * @return void
	 */
	public function remove(mixed $key) : void {
		unset($this->data[$key]);
	}

	public function removeAll(iterable $keys) : void {
		foreach($keys as $key) {
			$this->remove($key);
		}
	}

	/**
	 * @param T $key
	 */
	public function has(mixed $key) : bool {
		return isset($this->data[$key]);
	}

	/**
	 * @return T|null
	 */
	public function getFirst() {
		return array_key_first($this->data);
	}

	/**
	 * @return T|null
	 */
	public function shift() {
		$first = $this->getFirst();
		$this->remove($first);
		return $first;
	}

	/**
	 * @return T[]
	 */
	public function toArray() : array {
		return array_keys($this->data);
	}

	/**
	 * @param T $offset
	 * @return bool
	 */
	public function offsetExists(mixed $offset) : bool {
		return isset($this->data[$offset]);
	}

	/**
	 * @param T $offset
	 * @return T
	 */
	public function offsetGet(mixed $offset) : mixed {
		return $this->data[$offset];
	}

	/**
	 * @param T $offset
	 * @param T $value
	 * @return void
	 */
	public function offsetSet($offset, $value) : void {
		if (is_null($offset)) {
			$this->data[$value] = $value;
		} else {
			$this->data[$offset] = $offset;
		}
	}

	/**
	 * @param T $offset
	 * @return void
	 */
	public function offsetUnset(mixed $offset) : void {
		unset($this->data[$offset]);
	}

	public function count() : int {
		return count($this->data);
	}

	/**
	 * @return Traversable<T>
	 */
	public function getIterator() : Traversable {
		return new ArrayIterator($this->toArray());
	}
}