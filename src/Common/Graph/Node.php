<?php

namespace AoC\Common\Graph;

use Stringable;
use AoC\Common\StringableStorage;


class Node
{
	public function __construct(
		readonly public string $identifier
	) {}


	public function __toString() : string {
		return sprintf("%s:%s", static::class, $this->identifier);
	}
}