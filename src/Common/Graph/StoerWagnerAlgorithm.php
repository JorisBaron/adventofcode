<?php

namespace AoC\Common\Graph;

use AoC\Common\StringableStorage;

class StoerWagnerAlgorithm
{
	protected Graph $graph;
	protected ?Cut $minCut = null;
	/** @var array<string, string[]> > */
	protected array $nodeGroups = [];

	public function __construct(Graph $graph) {
		$this->graph = clone $graph;
		$this->minCut = new Cut([], [], PHP_INT_MAX);
		foreach($graph->getNodes() as $nodeIdent=>$node){
			$this->nodeGroups[$nodeIdent] = [$nodeIdent];
		}
	}

	public function run(?string $a = null) : ?Cut {
		$this->minCut($a??array_key_first($this->graph->getNodes()));
		return $this->minCut;
	}

	protected function minCut(string $a) : void {
		while(count($this->graph->getNodes()) > 1){
			$this->minCutPhase($a);
		}
	}

	protected function minCutPhase(string $a) : void {
		$coupling = [];
		foreach($this->graph->getNodes() as $nodeIdent=>$node){
			$coupling[$nodeIdent] = 0;
		}
		unset($coupling[$a]);

		foreach($this->graph->getNextNodes($a) as $nextIdent) {
			$coupling[$nextIdent] += $this->graph->getEdge($a, $nextIdent)->getWeight();
		}

		$s = $a;
		$sSide = [$s];

		while(count($coupling) > 1) {
			/** @var string $maxNodeIdent */
			$maxNodeIdent = array_keys($coupling, max($coupling))[0];
			unset($coupling[$maxNodeIdent]);

			$nextNodes = $this->graph->getNextNodes($maxNodeIdent);
			foreach($nextNodes as $nextIdent) {
				if(isset($coupling[$nextIdent])) {
					$coupling[$nextIdent] += $this->graph->getEdge($maxNodeIdent, $nextIdent)->getWeight();
				}
			}


			$s = $maxNodeIdent;
			$sSide[] = $s;
		}
		$t = array_keys($coupling)[0];


		$cutWeight = $coupling[$t];

		if($cutWeight < $this->minCut->weight){
			$tSide = $this->nodeGroups[$t];
			$sSideStorage = [];
			foreach($sSide as $sSideNodeIdent) {
				$sSideStorage = array_merge($sSideStorage, $this->nodeGroups[$sSideNodeIdent]);
			}
			$this->minCut = new Cut($sSideStorage, $tSide, $cutWeight);
		}

		//shrinking
		$nextT = $this->graph->getNextNodes($t);
		foreach($nextT as $nodeIdent) {
			$tEdgeWeight = $this->graph->getEdge($t, $nodeIdent)->getWeight();
			if($this->graph->hasNextNode($s, $nodeIdent)){
				$edge = $this->graph->getEdge($s, $nodeIdent);
				$edge->setWeight($edge->getWeight() + $tEdgeWeight);
			} else {
				$this->graph->createEdge($s, $nodeIdent, $tEdgeWeight, false);
			}
		}
		$this->graph->removeNode($t);

		$this->nodeGroups[$s] = array_merge($this->nodeGroups[$s], $this->nodeGroups[$t]);
		$this->nodeGroups[$t] = $this->nodeGroups[$s];
	}
}