<?php

namespace AoC\Common\Graph;

use AoC\Common\StringableStorage;
use SplStack;
use AoC\Common\ArrayFunc;

class Graph
{
	/** @var array<string,Node> */
	protected array $nodes = [];
	/** @var array<string, Edge> */
	protected array $edges = [];
	/** @var array<string, array<string, Edge> > */
	protected array $adjacencyMatrix = [];
	/** @var array<string, array<string, Edge> > */
	protected array $reverseAdjacencyMatrix = [];

	public function __construct() {}

	public function __clone() : void {
		$this->nodes = ArrayFunc::deepClone($this->nodes);
		$this->edges = ArrayFunc::deepClone($this->edges);
		foreach($this->edges as $edge) {
			$this->adjacencyMatrix[$edge->n1][$edge->n2] = $edge;
			$this->reverseAdjacencyMatrix[$edge->n2][$edge->n1] = $edge;
			if(!$edge->directed) {
				$this->adjacencyMatrix[$edge->n2][$edge->n1] = $edge;
				$this->reverseAdjacencyMatrix[$edge->n1][$edge->n2] = $edge;
			}
		}
	}

	public function hasNode(mixed $identifier) : bool {
		return isset($this->nodes[$identifier]);
	}

	public function getNode(mixed $identifier) : ?Node {
		return $this->nodes[$identifier] ?? null;
	}

	public function getNodes() : array {
		return $this->nodes;
	}

	public function createNode(string $identifier) : Node {
		return $this->nodes[$identifier] = new Node($identifier);
	}

	public function addNode(Node $node) : void {
		$this->nodes[$node->identifier] = $node;
	}

	public function removeNode(string $nodeIdent) : void {
		foreach($this->adjacencyMatrix[$nodeIdent] as $edge) {
			unset($this->edges[$edge->getIdentifier()]);
		}

		foreach($this->reverseAdjacencyMatrix[$nodeIdent] as $edge) {
			unset($this->edges[$edge->getIdentifier()]);
		}

		unset($this->adjacencyMatrix[$nodeIdent]);
		unset($this->reverseAdjacencyMatrix[$nodeIdent]);

		foreach($this->adjacencyMatrix as &$row) {
			unset($row[$nodeIdent]);
		}

		foreach($this->reverseAdjacencyMatrix as &$col) {
			unset($col[$nodeIdent]);
		}

		unset($this->nodes[$nodeIdent]);
	}

	/**
	 * @param string $identifier
	 * @return string[]
	 */
	public function getNextNodes(string $identifier) : array {
		return array_keys($this->adjacencyMatrix[$identifier] ?? []);
	}

	public function hasNextNode(string $identifier, string $identifierNext) : bool {
		return isset($this->adjacencyMatrix[$identifier][$identifierNext]);
	}

	/**
	 * @param string $identifier
	 * @return string[]
	 */
	public function getPreviousNodes(string $identifier) : array {
		return array_keys($this->reverseAdjacencyMatrix[$identifier] ?? []);
	}

	public function hasPreviousNode(string $identifier, string $identifierPrev) : bool {
		return isset($this->reverseAdjacencyMatrix[$identifier][$identifierPrev]);
	}

	public function createEdge(string $startIdentifier, string $endIdentifier, int $weight, bool $directed = true
	) : Edge {
		if(!isset($this->nodes[$startIdentifier])) {
			$this->createNode($startIdentifier);
		}
		if(!isset($this->nodes[$endIdentifier])) {
			$this->createNode($endIdentifier);
		}

		$edge = new Edge($startIdentifier, $endIdentifier, $weight, $directed);
		if(!$directed) {
			$this->adjacencyMatrix[$endIdentifier][$startIdentifier] = $edge;
			$this->reverseAdjacencyMatrix[$startIdentifier][$endIdentifier] = $edge;
		}
		$this->adjacencyMatrix[$startIdentifier][$endIdentifier] = $edge;
		$this->reverseAdjacencyMatrix[$endIdentifier][$startIdentifier] = $edge;
		return $this->edges[$edge->getIdentifier()] = $edge;
	}

	public function removeEdge(string $startIdentifier, string $endIdentifier) : void {
		if(!isset($this->adjacencyMatrix[$startIdentifier][$endIdentifier])) {
			return;
		}

		$edge = $this->adjacencyMatrix[$startIdentifier][$endIdentifier];

		unset($this->edges[$edge->getIdentifier()]);
		unset($this->adjacencyMatrix[$startIdentifier][$endIdentifier]);
		unset($this->reverseAdjacencyMatrix[$endIdentifier][$startIdentifier]);

		if(!$edge->directed) {
			unset($this->adjacencyMatrix[$endIdentifier][$startIdentifier]);
			unset($this->reverseAdjacencyMatrix[$startIdentifier][$endIdentifier]);
		}
	}

	public function getEdge(string $n1Ident, string $n2Ident) : ?Edge {
		return $this->adjacencyMatrix[$n1Ident][$n2Ident] ?? null;
	}

	/**
	 * @return array
	 */
	public function getEdges() : array {
		return $this->edges;
	}
}