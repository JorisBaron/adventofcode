<?php

namespace AoC\Common\Graph;


class Edge
{
	public function __construct(
		public readonly string $n1,
		public readonly string $n2,
		public int $weight,
		readonly public bool $directed
	) {}


	public function getConnected(string $node) : string {
		return match ($node) {
			$this->n1 => $this->n2,
			$this->n2 => $this->n1,
		};
	}

	public function getWeight() : int {
		return $this->weight;
	}

	/**
	 * @param int $weight
	 */
	public function setWeight(int $weight) : void {
		$this->weight = $weight;
	}

	public function getIdentifier() : string {
		return sprintf("%s%s%s", $this->n1, $this->directed?'->':'<>' , $this->n2);
	}
}