<?php

namespace AoC\Common\Graph\Algorithms\Components;

use AoC\Common\StringIntSet;

readonly class ComponentsResult
{
	/**
	 * @param StringIntSet[] $components
	 * @param array<string, StringIntSet> $nodesToComponents
	 */
	public function __construct(
		public array $components,
		public array $nodesToComponents
	) {}
}