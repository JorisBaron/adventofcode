<?php

namespace AoC\Common\Graph\Algorithms\Components;

use AoC\Common\StringIntSet;
use AoC\Common\Graph\Graph;
use AoC\Common\Graph\Node;

class Components
{
	/**
	 * @param Graph $graph
	 * @return ComponentsResult
	 */
	public function getComponents(Graph $graph) : ComponentsResult {
		/** @var StringIntSet[] $components */
		$components = [];
		/** @var array<string, StringIntSet> $nodesToComponents */
		$nodesToComponents = [];
		$remainingNodes = StringIntSet::fromIterable(array_map(fn(Node $node)=>$node->identifier, $graph->getNodes()));

		while(count($remainingNodes) > 0) {
			$nodeIdentifier = $remainingNodes->getFirst();

			$component = $this->getComponent($graph, $nodeIdentifier);
			$remainingNodes->removeAll($component);

			$components[] = $component;
			foreach($component as $nodeIdentifier) {
				$nodesToComponents[$nodeIdentifier] = $component;
			}
		}

		return new ComponentsResult($components, $nodesToComponents);
	}

	public function getComponent(Graph $graph, string $startingNodeIdentifier) : StringIntSet {
		$component = new StringIntSet();
		$remainingNodes = new StringIntSet();
		$remainingNodes->add($startingNodeIdentifier);

		while($remainingNodes->count() > 0) {
			$nodeIdentifier = $remainingNodes->shift();
			$component->add($nodeIdentifier);

			foreach($graph->getNextNodes($nodeIdentifier) as $nextNodeIdentifier) {
				if(!$component->has($nextNodeIdentifier)) {
					$remainingNodes->add($nextNodeIdentifier);
				}
			}
		}

		return $component;
	}
}