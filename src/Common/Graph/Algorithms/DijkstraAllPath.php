<?php

namespace AoC\Common\Graph\Algorithms;

use AoC\Common\StringIntSet;
use AoC\Common\Graph\Edge as Edge;
use AoC\Common\Graph\Graph;
use AoC\Common\Graph\Node;

class DijkstraAllPath
{
	public function __construct(
		protected Graph $graph
	) {}

	/**
	 * @param Node $start
	 * @param Node $end
	 * @return array{0:int, 1:Graph}
	 */
	public function execute(Node $start, Node $end) : array {
		/** @var array<string,int> $dist */
		$dist = [];

		/** @var array<string, Edge[]> $prevEdges */
		$prevEdges = [];
		$prevEdges[$start->identifier] = null;

		/** @var array<string,Node> $nodesToProcess */
		$nodesToProcess = [];

		foreach($this->graph->getNodes() as $node){
			$nodesToProcess[$node->identifier] = $node;
			$dist[$node->identifier] = PHP_INT_MAX;
		}
		$dist[$start->identifier] = 0;


		while(count($nodesToProcess) > 0) {
			$minNodeIdentifier = $this->chooseNode($nodesToProcess, $dist);
			unset($nodesToProcess[$minNodeIdentifier]);

			if($minNodeIdentifier === $end->identifier) {
				break;
			}

			$this->processNode($minNodeIdentifier, $dist, $prevEdges);
		}


		$path = new Graph();
		$path->createNode($end->identifier);

		/** @var StringIntSet<string> $nodesToAddToPath */
		$nodesToAddToPath = new StringIntSet();
		$nodesToAddToPath->add($end->identifier);
		/** @var StringIntSet<string> $nodesAddedToPath */
		$nodesAddedToPath = new StringIntSet();


		while($nodesToAddToPath->count() > 0) {
			$nodeIdent = $nodesToAddToPath->shift();
			$nodesAddedToPath->add($nodeIdent);

			$prevs = $prevEdges[$nodeIdent] ?? [];

			foreach($prevs as $prev){
				$prevNodeIdent = $prev->getConnected($nodeIdent);
				$path->createEdge($prevNodeIdent, $nodeIdent, $prev->weight, true);
				if(!$nodesAddedToPath->has($prevNodeIdent)){
					$nodesToAddToPath->add($prevNodeIdent);
				}
			}
		}

		return [$dist[$end->identifier], $path];
	}

	/**
	 * @param array<string,Node> $nodes
	 * @param array<string,int> $dist
	 * @return ?string
	 */
	private function chooseNode(array $nodes, array $dist) : ?string {
		$minLength = PHP_INT_MAX;
		$minNode = null;
		foreach($nodes as $nodeIdentifier => $nodeToProcess) {
			if($dist[$nodeIdentifier] <= $minLength) {
				$minLength = $dist[$nodeIdentifier];
				$minNode = $nodeIdentifier;
			}
		}
		return $minNode;
	}

	/**
	 * @param string $nodeIdentifier
	 * @param array<string,int>  $dist
	 * @param array<string,Edge[]>  $prevEdges
	 * @return void
	 */
	private function processNode(string $nodeIdentifier, array &$dist, array &$prevEdges) : void {
		foreach($this->graph->getNextNodes($nodeIdentifier) as $neighborIdent) {
			$edge = $this->graph->getEdge($nodeIdentifier, $neighborIdent);
			$pathLength = $dist[$nodeIdentifier] + $edge->getWeight();
			if($pathLength === $dist[$neighborIdent]){
				$prevEdges[$neighborIdent][] = $edge;
			}
			if($pathLength < $dist[$neighborIdent]) {
				$dist[$neighborIdent] = $pathLength;
				$prevEdges[$neighborIdent] = [$edge];
			}
		}
	}
}