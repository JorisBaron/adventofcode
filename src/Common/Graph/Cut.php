<?php

namespace AoC\Common\Graph;

readonly class Cut
{
	/**
	 * @param string[] $nodeSet1
	 * @param string[] $nodeSet2
	 * @param int    $weight
	 */
	public function __construct(
		public array $nodeSet1,
		public array $nodeSet2,
		public int $weight
	) {}
}