<?php

namespace AoC\Common;

use Exception;

class ArrayFunc
{
	/**
	 * @template T
	 * @param array<T>         $array
	 * @param callable(T):bool $fn
	 * @return bool
	 */
	static function array_some(array $array, callable $fn) : bool {
		foreach($array as $value) {
			if($fn($value)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @template T
	 * @param array<T>         $array
	 * @param callable(T):bool $fn
	 * @return bool
	 */
	static function array_every(array $array, callable $fn) : bool {
		foreach($array as $value) {
			if(!$fn($value)) {
				return false;
			}
		}
		return true;
	}


	/**
	 * @throws Exception
	 */
	static function hammingDistance(array $a1, array $a2, array &$diffArray = []) : int {
		$max = count($a1);
		if($max !== count($a2)) {
			throw new Exception("Arrays are not the same length");
		}

		$dist = 0;
		for($i = 0; $i < $max; $i++) {
			if($a1[$i] !== $a2[$i]) {
				$dist++;
				$diffArray[] = $i;
			}
		}

		return $dist;
	}

	static function deepClone($arr) : array {
		$newArray = [];
		foreach($arr as $key => $value) {
			if(is_array($value)) {
				$newArray[$key] = static::deepClone($value);
			} elseif(is_object($value)) {
				$newArray[$key] = clone $value;
			} else {
				$newArray[$key] = $value;
			}
		}
		return $newArray;
	}
}
