<?php

namespace AoC\Common;

readonly class Point3D
{
	public function __construct(
		public int $x,
		public int $y,
		public int $z
	) {}

	public function get2d() : Point {
		return new Point($this->x, $this->y);
	}

	public function add(Point3D $point) : static {
		return new static(
			$this->x + $point->x,
			$this->y + $point->y,
			$this->z + $point->z,
		);
	}

	public function subtract(self $point) : static {
		return new static(
			$this->x - $point->x,
			$this->y - $point->y,
			$this->z - $point->z,
		);
	}

	public function multiply(int $value) : static {
		return new static(
			$this->x * $value,
			$this->y * $value,
			$this->z * $value,
		);
	}

	public function equals(self $point) : bool {
		return $this->x === $point->x
		       && $this->y === $point->y
		       && $this->z === $point->z;
	}

	public function manhattanDistance(self $p) : int {
		return abs($this->x - $p->x) + abs($this->y - $p->y) + abs($this->z - $p->z);
	}

	public function toString() : string {
		return $this->__toString();
	}

	public function __toString() : string {
		return $this->x.','.$this->y.','.$this->z;
	}
}