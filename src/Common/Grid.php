<?php

namespace AoC\Common;
use OutOfBoundsException;

/**
 * @template T
 */
class Grid
{
	protected int $width;
	protected int $height;

	/**
	 * @param T[][] $grid
	 */
	public function __construct(
		protected array $grid
	) {
		$this->height = count($grid);
		$this->width = count($grid[0]);
	}


	/**
	 * @param string[] $lineArray
	 * @return static
	 */
	public static function createFromInput(array $lineArray) : static {
		return new static(array_map(fn($line) => str_split($line), $lineArray));
	}

	public static function fromDimensions(int $width, int $height, mixed $defaultValue) : static {
		return new static(
			array_fill(0, $height,
				array_fill(0, $width, $defaultValue)
			)
		);
	}


	public function getWidth() : int {
		return $this->width;
	}

	public function getHeight() : int {
		return $this->height;
	}


	function isInBound(Point $point) : bool {
		return $point->x >= 0 && $point->y >= 0 && $point->x < $this->width && $point->y < $this->height;
	}

	/**
	 * @param Point $point
	 * @return T
	 * @throws OutOfBoundsException
	 */
	public function get(Point $point) : mixed {
		if(!$this->isInBound($point)) {
			throw new OutOfBoundsException();
		}

		return $this->grid[$point->y][$point->x];
	}

	/**
	 * @param int $y
	 * @return T[]
	 * @throws OutOfBoundsException
	 */
	public function getRow(int $y) : array {
		if($y < 0 || $y >= $this->height) {
			throw new OutOfBoundsException();
		}
		return $this->grid[$y];
	}

	/**
	 * @param int $x
	 * @return T[]
	 * @throws OutOfBoundsException
	 */
	public function getColumn(int $x) : array {
		if($x < 0 || $x >= $this->width) {
			throw new OutOfBoundsException();
		}

		return array_map(fn($row) => $row[$x], $this->grid);
	}

	/**
	 * @param Point $point
	 * @return Point[]
	 */
	public function getAdjacents(Point $point) : array {
		$adjacents = [];
		foreach([
			new Point( 1, 0),
			new Point(-1, 0),
			new Point( 0, 1),
			new Point( 0,-1)
		] as $direction){
			$adjacent = $point->add($direction);
			if($this->isInBound($adjacent)){
				$adjacents[] = $adjacent;
			}
		}
		return $adjacents;
	}


	/**
	 * @param Point $point
	 * @param T $value
	 * @return void
	 * @throws OutOfBoundsException
	 */
	public function set(Point $point, mixed $value) : void {
		if(!$this->isInBound($point)) {
			throw new OutOfBoundsException();
		}

		$this->grid[$point->y][$point->x] = $value;
	}


	public function display() : void {
		foreach($this->grid as $row) {
			echo implode('', $row).PHP_EOL;
		}
	}

	public function rotateClockwise() : void {
		$newGrid = [];
		for($i = 0; $i < $this->width; $i++) {
			$newRow = [];
			for($j = $this->height - 1; $j >= 0; $j--) {
				$newRow[] = $this->grid[$j][$i];
			}
			$newGrid[] = $newRow;
		}
		$this->grid = $newGrid;
		[$this->width, $this->height] = [$this->height, $this->width];
	}

	public function rotateCounterClockwise() : void {
		$newGrid = [];
		for($i = $this->width - 1; $i >= 0; $i--) {
			$newRow = [];
			for($j = 0; $j < $this->height; $j++) {
				$newRow[] = $this->grid[$j][$i];
			}
			$newGrid[] = $newRow;
		}
		$this->grid = $newGrid;
		[$this->width, $this->height] = [$this->height, $this->width];
	}

}