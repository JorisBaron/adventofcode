<?php

use AoC\Common\ArrayFunc;

require_once __DIR__.'/../../../vendor/autoload.php';

$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$sum = 0;
foreach ($file as $line) {
	$firstSeq = array_map(intval(...), explode(' ', $line));
	$seqs = [$firstSeq];
	$lastSeq = $firstSeq;

	while (!ArrayFunc::array_every($lastSeq, fn($value) => $value===0)) {
		$newSeq = [];
		for($i = 0; $i < count($lastSeq)-1; $i++){
			$newSeq[] = $lastSeq[$i+1] - $lastSeq[$i];
		}
		$seqs[] = $newSeq;
		$lastSeq = $newSeq;
	}

	for($i = count($seqs) -2; $i>=0; $i--){
		$seqs[$i][] = end($seqs[$i]) + end($seqs[$i+1]);
	}

	$sum += end($seqs[0]);
}

echo $sum;
