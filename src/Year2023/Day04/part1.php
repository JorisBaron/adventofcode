<?php

$file = new SplFileObject("input/input.txt");

$sum = 0;
foreach ($file as $line) {
	preg_match("/Card\s+\d+: ([\s\d]+) \| ([\s\d]+)/", trim($line), $matches);

	$winners = array_filter(explode(" ", $matches[1]), fn($numberStr) => $numberStr!=="");
	$played = array_filter(explode(" ", $matches[2]), fn($numberStr) => $numberStr!=="");

	$inter = array_intersect($winners, $played);
	$count = count($inter);

	if($count > 0) {
		$sum += 2**($count-1);
	}
}

echo $sum;