<?php

$file = new SplFileObject("input/input.txt");

$lines = iterator_to_array($file);

$numberOfCopies = array_fill(1, count($lines), 1);
foreach ($lines as $line) {

	preg_match("/Card\s+(\d+): ([\s\d]+) \| ([\s\d]+)/", trim($line), $matches);

	$cardNumber = (int)$matches[1];
	$winners = array_filter(explode(" ", $matches[2]), fn($numberStr) => $numberStr!=="");
	$played = array_filter(explode(" ", $matches[3]), fn($numberStr) => $numberStr!=="");

	$inter = array_intersect($winners, $played);
	$count = count($inter);

	$numberOfCopy = ($numberOfCopies[$cardNumber]??1);
	for($i=1; $i<=$count; $i++) {
		$numberOfCopies[$cardNumber+$i] += $numberOfCopy;
	}
}

echo array_sum($numberOfCopies);