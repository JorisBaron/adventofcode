<?php

namespace AoC\Year2023\Day21;

use AoC\Common\Grid;
use AoC\Common\Point;
use AoC\Common\StringableStorage;

/**
 * @extends Grid<string>
 */
class GardenGrid extends Grid
{
	protected const string ROCK_SYMBOL = '#';

	protected Point $start;

	public function __construct(array $grid) {
		parent::__construct($grid);

		foreach($this->grid as $y=>$row) {
			foreach($row as $x=>$cell) {
				if($cell === 'S') {
					$this->start = new Point($x,$y);
					break 2;
				}
			}
		}
	}

	/**
	 * @return Point
	 */
	public function getStart() : Point {
		return $this->start;
	}

	/**
	 * @param Point $point
	 * @param bool  $wrap
	 * @return Point[]
	 */
	public function getAdjacents(Point $point, bool $wrap = false) : array {
		$adjacents = [];
		foreach([
			new Point( 1, 0),
			new Point(-1, 0),
			new Point( 0, 1),
			new Point( 0,-1)
		] as $direction){
			$adjacent = $point->add($direction);
			if($wrap) {
				$adjacent = new Point(
					($adjacent->x % $this->width + $this->width) % $this->width,
					($adjacent->y % $this->height + $this->height) % $this->height
				);
				if($this->get($adjacent) !== self::ROCK_SYMBOL){
					$adjacents[] = $adjacent;
				}
			} else {
				if($this->isInBound($adjacent) && $this->get($adjacent) !== self::ROCK_SYMBOL){
					$adjacents[] = $adjacent;
				}
			}

		}

		return $adjacents;
	}

	public function calculateOddGrid() : int {
		$sum = 0;
		for($y = 0; $y < $this->height; $y++){
			for($x = ($y+1)%2; $x < $this->width; $x+=2){
				if($this->grid[$y][$x] !== self::ROCK_SYMBOL){
					$sum++;
				}
			}
		}
		return $sum;
	}

	public function toOddGrid() : static {
		for($y = 0; $y < $this->height; $y++){
			for($x = ($y+1)%2; $x < $this->width; $x+=2){
				if($this->grid[$y][$x] !== self::ROCK_SYMBOL){
					$this->grid[$y][$x] = "O";
				}
			}
		}
		return $this;
	}

	public function calculateEvenGrid() : int {
		$sum = 0;
		for($y = 0; $y < $this->height; $y++){
			for($x = $y%2; $x < $this->width; $x+=2){
				if($this->grid[$y][$x] !== self::ROCK_SYMBOL){
					$sum++;
				}
			}
		}
		return $sum;
	}

	public function toEvenGrid() : static {
		for($y = 0; $y < $this->height; $y++){
			for($x = $y%2; $x < $this->width; $x+=2){
				if($this->grid[$y][$x] !== self::ROCK_SYMBOL){
					$this->grid[$y][$x] = "O";
				}
			}
		}
		return $this;
	}

	/**
	 * @param Point $start
	 * @param int   $steps
	 * @return StringableStorage<Point>
	 */
	public function calculateSteps(Point $start, int $steps) : StringableStorage {
		/** @var StringableStorage<Point>[] $oddEvenPlots */
		$oddEvenPlots = [
			0 => new StringableStorage(),
			1 => new StringableStorage()
		];
		$oddEvenPlots[0]->attach($start);
		/** @var StringableStorage<Point> $currentOuterPlots */
		$currentOuterPlots = new StringableStorage();
		$currentOuterPlots->attach($start);

		for($i = 1; $i<=$steps; $i++) {
			$next = new StringableStorage();
			$destPlot = $oddEvenPlots[$i%2];
			foreach($currentOuterPlots as $toCompute) {
				$adjacents = $this->getAdjacents($toCompute);
				foreach($adjacents as $adjacent){
					$next->attach($adjacent);
				}
			}
			$next->removeAll($destPlot);
			$destPlot->addAll($next);
			$currentOuterPlots = $next;
		}


		return $oddEvenPlots[$steps%2];
	}

	public function toSteps(Point $start, int $steps) : static {
		$points = $this->calculateSteps($start, $steps);
		foreach($points as $point){
			$this->set($point, 'O');
		}
		return $this;
	}
}