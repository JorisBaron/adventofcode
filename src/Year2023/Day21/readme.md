# Part 1
Easy.

# Part 2
*(NB: thanks to [u/RewrittenCodeA for his post on day 21 solutions megathread](https://www.reddit.com/r/adventofcode/comments/18nevo3/comment/ket7c1r/?utm_source=reddit&utm_medium=web2x&context=3), 
helped me in the beginning)*

Oh gosh. 

Of course brute force does not work. First wanted to try things with modulo coordinates, toroidal grid etc, but was way too complicated.
Went to Reddit to get some inspiration, and it made me analyze my input.

Realized that borders and start lines and columns were clear, and the grid was 131\*131.
And 26501365 is 202300\*131 + 65.  
**HOW CONVENIENT**

From that, the problem reduces itself to "counting the repeating garden" and 
"how much final plots for each kind of repeated garden". 

First, we can notice that, except border gardens, they are all "complete", meaning that each plot is traversed.  
We can also notice that for all those complete gardens, there are 2 types: odd and even. I define an "even garden" as
a complete garden where 0,0 (and every *accessible* plot with an even sum of coordinates (I'll comme back to "*accessible*" later))
is a final plot. "Odd garden" is the same but sum of coordinates is odd ofc.  
Start garden is odd, because number of steps is odd, so final plots must be at an odd number of steps from start,
and start is an even plot.  
From there, we can notice that each further gardens alternate between odd and even: start garden is odd,
its adjacent ones are even, then odd, etc.

So, let's forget borders for now to count complete gardens:
* Borders are 202300 gardens away from the start, so there are 202299 (manhattan distance) complete gardens between
  start and borders
* Borders are odd gardens, since there is an odd number of complete gardens between border and start
  (like **S**EOEOE**B**, S is odd and so is B)

Let's plot an example to count odd and even gardens:
```
            B
          B E B
        B E O E B
      B E O E O E B
    B E O E O E O E B
  B E O E O E O E O E B
B E O E O E S E O E O E B
  B E O E O E O E O E B
    B E O E O E O E B
      B E O E O E B
        B E O E B
          B E B
            B
```
We can in fact consider only a quarter of this and multiply it by 4:
```
          B 
        B E 
      B E O 
    B E O E 
  B E O E O 
B E O E O E S
```
We can see that:
* the number of even garden is the sum of odd number from 1 to 5, or 1 to 202299 in our case 
  it can be calculated using the formula of the sum of the *n* first odd numbers which is *n²*, with *n*=3 in the exemple,
  and *n*=202300/2=101500 for the real input
* the number of even garden is the sum of even number from 1 to 4, or 1 to 202298 in our case
  again, it can be calculated with the sum of the *n* first even numbers: *n(n+1)*, with *n*=2 in the exemple,
  and *n*=(202300-2)/2=101499 for the real input

So to sum up, we have 101500² even and 101499*101500 odd gardens, +1 odd garden which is the starting one.

**Now the borders...**

We know that each straight lines from the start garden is not complete: only 131 steps were made 
starting from the middle of the side it is coming from (eg: for the top border, the first step in the last garden is 
made at (65,130), the middle of the bottom side), where it would need 196 to complete it (131+65).  
There, I assumed that, seeing the input (empty "diamond shape"), the last gardens will always form diagonals plots pattern.
For example, the last west garden would look similar to this:
```
. . . O . O .
. . O . O . O
. O . O . O .
O . O . O . O
. O . O . O .
. . O . O . O
. . . O . O .
```
From that, I deducted the shape of the edges (NW edge in this exemple):
```
                . . . . . . . | . . . O . . . |
                . . . . . . . | . . O . O . . |
                . . . . . . . | . O . O . O . |
                . . . . . . . | O . O . O . O |
                . . . . . . O | . O . O . O . |
                . . . . . O . | O . O . O . O |
                . . . . O . O | . O . O . O . |
                --------------+---------------+-
. . . . . . . | . . . O . O . |               |
. . . . . . . | . . O . O . O |               |
. . . . . . . | . O . O . O . |    complete   |
. . . . . . . | O . O . O . O |      even     |
. . . . . . O | . O . O . O . |     garden    |
. . . . . O . | O . O . O . O |               |
. . . . O . O | . O . O . O . |               |
--------------+---------------+---------------+-
. . . O . O . |               |               |
. . O . O . O |               |               |
. O . O . O . |    complete   |    complete   |
O . O . O . O |      even     |      odd      |
. O . O . O . |     garden    |     garden    |
. . O . O . O |               |    (start)    |
. . . O . O . |               |               |
--------------+---------------+---------------+-
              |               |               |
```
We can see different things here:
* there are 2 layer of edges
* the "thick" layer is always odd
* there are as many "thick" layers as the number of gardens between the start and the border (excluding those).
  In this exemple, that means 1 thick layer, in the real input it corresponds to 202299.
* the "thin" layer is always even
* there are as many "thin" layers as the number of gardens between the start (excl) and the border (incl).
  In this exemple, that means 2 thin layers, in the real input it corresponds to 202300.

Of course, each edges has it own "thick" and "thin" layers, meaning 8 final plot layouts to compute.
Notice that, each thin layer corresponds in fact to 65 step starting from one of the 4 corner, and the same goes
for thick layer but with 196 steps (131+65)

**We now have everything we want**: number of even and odd complete gardens, "corner" layouts and edges layout.
We just have to know of many final step each of those has.

First, I assumed that every plot on the garden was reachable, so I just counted every plots
that were on odd/even coordinates and that was not a rock (`#`). **However**, not every spot is reachable...
In fact 3 spot are not on the complete even garden, and the same goes for the odd one.  
So I had to compute each of those like I computed part 1: choose a starting point and walk down every path for *X* steps.
It led me to compute 14 layouts :
* Odd and even complete gardens (2)
* Every "corners", i.e. end of each straight line from start (4)
* Every "thick" and "thin" edges (4+4)

Finally, our number of final plots is this sum:
```
  4 * 101500^2 * <final plots in even garden>
+ (4 * 101500 * 101499 + 1) * <final plots in odd garden> // <- +1 is for the starting garden
+ 202299 * sum(<final plots in each thick edge>)
+ 202300 * sum(<final plots in each thin edge>)
+ sum(<final plots in "corner">)
```

And there we have it!!!