<?php

use AoC\Year2023\Day21\GardenGrid;
use AoC\Common\StringableStorage;

require_once __DIR__.'/../../../vendor/autoload.php';

//$file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$g = GardenGrid::createFromInput(iterator_to_array($file));

$plots = new StringableStorage();
$plots->attach($g->getStart());

for($i = 0; $i<64; $i++){
	$nextPlots = new StringableStorage();
	foreach($plots as $plot) {
		$adjacents = $g->getAdjacents($plot);
		foreach($adjacents as $adjacent){
			$nextPlots->attach($adjacent);
		}
	}
	$plots = $nextPlots;
}

echo $plots->count().PHP_EOL;