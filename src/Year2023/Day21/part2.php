<?php

use AoC\Year2023\Day21\GardenGrid;
use AoC\Common\Point;

require_once __DIR__.'/../../../vendor/autoload.php';

//$file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);
$lineArray = iterator_to_array($file);

$g = GardenGrid::createFromInput($lineArray);
GardenGrid::createFromInput($lineArray)->toSteps(new Point(0,0), 260)->display();
echo PHP_EOL;
GardenGrid::createFromInput($lineArray)->toSteps(new Point(0,0), 261)->display();

echo PHP_EOL;
GardenGrid::createFromInput($lineArray)->toSteps(new Point(65,  0), 130)->display();
GardenGrid::createFromInput($lineArray)->toSteps(new Point(0,  0), 195)->display();
GardenGrid::createFromInput($lineArray)->toSteps(new Point(0,  0), 64)->display();
echo PHP_EOL;

$totalSideGrids = 202_300;
$nOdd = $totalSideGrids/2;
$nEven = $nOdd-1;
$floorCompleteSideGridHalf = $totalSideGrids/2 - 1;

$completeOddGridCount = $nEven*($nEven+1); // sum of even number, bc the number of odd gardens is sum of even
$completeEvenGridCount = $nOdd*$nOdd; // sum of even number


$completeEvenGridStepCount = $g->calculateSteps(new Point(0,0), 260)->count();
$completeOddGridStepCount = $g->calculateSteps(new Point(0,0), 261)->count();
$sideGridCounts = [
	'N' => $g->calculateSteps(new Point( 65, 130), 130)->count(),
	'S' => $g->calculateSteps(new Point( 65,   0), 130)->count(),
	'W' => $g->calculateSteps(new Point(130,  65), 130)->count(),
	'E' => $g->calculateSteps(new Point(  0,  65), 130)->count(),
];
$diagonalGridCounts65 = [
	'NW' => $g->calculateSteps(new Point(130, 130), 64)->count(),
	'NE' => $g->calculateSteps(new Point(  0, 130), 64)->count(),
	'SW' => $g->calculateSteps(new Point(130,   0), 64)->count(),
	'SE' => $g->calculateSteps(new Point(  0,   0), 64)->count(),
];
$diagonalGridCounts196 = [
	'NW' => $g->calculateSteps(new Point(130, 130), 195)->count(),
	'NE' => $g->calculateSteps(new Point(  0, 130), 195)->count(),
	'SW' => $g->calculateSteps(new Point(130,   0), 195)->count(),
	'SE' => $g->calculateSteps(new Point(  0,   0), 195)->count(),
];

$s = (int)(
	$completeEvenGridStepCount * 4* $completeEvenGridCount +
	$completeOddGridStepCount * (4* $completeOddGridCount+1) +
	array_sum($sideGridCounts) +
	array_sum($diagonalGridCounts65) * $totalSideGrids +
	array_sum($diagonalGridCounts196) * ($totalSideGrids-1)
);

var_dump($s);