<?php

require_once __DIR__.'/../../../vendor/autoload.php';

use AoC\Year2023\Day10\PipeGrid;

$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$grid = PipeGrid::createFromInput(iterator_to_array($file));
$path = $grid->pathFromStart();

echo count($path)/2;