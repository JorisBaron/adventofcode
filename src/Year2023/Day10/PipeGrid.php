<?php

namespace AoC\Year2023\Day10;
use Exception;
use OutOfBoundsException;
use AoC\Common\Grid;
use AoC\Common\Point;

class PipeGrid extends Grid
{
	protected Point $start;

	/**
	 * @param string[][] $grid
	 * @throws Exception
	 */
	public function __construct(
		array $grid
	) {
		parent::__construct($grid);

		foreach($this->grid as $j => $row) {
			foreach($row as $i => $cell) {
				if($cell === "S") {
					$connected = '';
					$this->start = new Point($i, $j);
					$directions = [
						[
							'dir' => new Point(0,-1),
							'matching' => '|7F',
							'char' => "N"
						],
						[
							'dir' => new Point(0,1),
							'matching' => '|JL',
							'char' => "S"
						],
						[
							'dir' => new Point(-1,0),
							'matching' => '-LF',
							'char' => "W"
						],
						[
							'dir' => new Point(1,0),
							'matching' => '-J7',
							'char' => "E"
						]
					];
					foreach($directions as $direction){
						$offsetPoint = $this->start->add($direction['dir']);
						if(!$this->isInBound($offsetPoint)){
							continue;
						}

						if(str_contains($direction['matching'], $this->get($offsetPoint))) {
							$connected .= $direction['char'];
						}
					}

					$this->grid[$j][$i] = match ($connected) {
						"NS","SN" => "|",
						"NW","WN" => "J",
						"NE","EN" => "L",
						"SW","WS" => "7",
						"SE","ES" => "F",
						"WE","EW" => "-",
						default => throw new Exception("Invalid grid")
					};

					break 2;
				}
			}
		}
	}


	public function getWidth() : int {
		return $this->width;
	}

	public function getHeight() : int {
		return $this->height;
	}

	public function getStart() : Point {
		return $this->start;
	}


	/**
	 * @param Point $point
	 * @return Point[]
	 */
	public function getConnected(Point $point) : array {
		$directions = match ($this->get($point)) {
			"|" => [new Point( 0, 1), new Point( 0, -1)],
			"-" => [new Point( 1, 0), new Point(-1,  0)],
			"L" => [new Point( 1, 0), new Point( 0, -1)],
			"J" => [new Point(-1, 0), new Point( 0, -1)],
			"7" => [new Point(-1, 0), new Point( 0,  1)],
			"F" => [new Point( 1, 0), new Point( 0,  1)],
			"." => [],
		};
		return array_map(fn($dir) => $point->add($dir), $directions);
	}

	public function getNext(Point $point, Point $prevPoint) : Point {
		$connected = $this->getConnected($point);
		return $connected[0]->equals($prevPoint) ? $connected[1] : $connected[0];
	}

	/**
	 * @return Point[]
	 */
	public function pathFromStart() : array {
		$path = [
			$this->start,
			$this->getConnected($this->start)[0],
		];

		while(true) {
			$pathLen = count($path);
			$dest = $this->getNext($path[$pathLen-1], $path[$pathLen-2]);
			if($dest->equals($this->start)) {
				break;
			}
			$path[] = $dest;
		}

		return $path;
	}
}