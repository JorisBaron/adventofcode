<?php

require_once __DIR__.'/../../../vendor/autoload.php';

use AoC\Year2023\Day10\PipeGrid;
use AoC\Common\Point;

$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$grid = PipeGrid::createFromInput(iterator_to_array($file));
$path = $grid->pathFromStart();

$pathMap = array_fill_keys(
	array_map(fn($point) => $point->toString(), $path),
	true
);

$nbIn = 0;
for ($y=0; $y<$grid->getHeight(); $y++){
	$in = false;
	$currentEdge = '';
	for ($x=0; $x<$grid->getWidth(); $x++){
		$point = new Point($x, $y);
		if(isset($pathMap[$point->toString()])){
			$char = $grid->get($point);
			switch ($char) {
				//straight edge
				case '|' :
					$in = !$in;
					break;
				// edge start
				case 'F':
				case 'L':
					$currentEdge = $char;
					break;
				// edge end
				case 'J':
				case '7':
					$currentEdge .= $char;
					// if edge "switches side"
					if ($currentEdge==="FJ" || $currentEdge==="L7") {
						$in = !$in;
					}
					break;
			}
		}
		elseif($in) {
			$nbIn++;
		}
	}
}

echo $nbIn;