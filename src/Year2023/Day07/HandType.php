<?php

namespace AoC\Year2023\Day07;
enum HandType : int
{
	case HIGH_CARD       = 0;
	case ONE_PAIR        = 1;
	case TWO_PAIR        = 2;
	case THREE_OF_A_KIND = 3;
	case FULL_HOUSE      = 4;
	case FOUR_OF_A_KIND  = 5;
	case FIVE_OF_A_KIND  = 6;

	public static function compare(self $h1, self $h2) : int {
		return $h1->value <=> $h2->value;
	}
}
