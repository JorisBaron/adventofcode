<?php

namespace AoC\Year2023\Day07;


require_once "HandType.php";

readonly class Hand
{
	/**
	 * @param HandType $handType
	 * @param Card2|Card[]   $cards
	 * @param int      $bid
	 */
	public function __construct(
		public HandType $handType,
		protected array $cards,
		public int $bid
	) {}

	public static function parseHand(string $handString) : static {
		[$cardsStr, $bidStr] = explode(' ', trim($handString));
		$cardStrArray = str_split($cardsStr);

		$counts = array_count_values($cardStrArray);
		rsort($counts);

		return self::commonParse($counts, $cardStrArray, $bidStr, Card::class);
	}

	public static function parseHandPart2(string $handString) : static {
		[$cardsStr, $bidStr] = explode(' ', trim($handString));
		$cardStrArray = str_split($cardsStr);

		$countsByCards = array_count_values($cardStrArray);
		arsort($countsByCards);
		if(isset($countsByCards["J"])) {
			if(count($countsByCards) === 1) {
				$countsByCards["J"] = 5;
			} else {
				$j = $countsByCards["J"];
				unset($countsByCards["J"]);
				$countsByCards[array_key_first($countsByCards)] += $j;
			}
		}

		return self::commonParse($countsByCards, $cardStrArray, $bidStr, Card2::class);
	}

	/**
	 * @param array  $counts
	 * @param array  $cardStrArray
	 * @param string $bidStr
	 * @param string $cardClass
	 * @return static
	 */
	protected static function commonParse(array $counts, array $cardStrArray, string $bidStr, string $cardClass) : Hand {
		$handType = match (array_values($counts)) {
			[5]             => HandType::FIVE_OF_A_KIND,
			[4, 1]          => HandType::FOUR_OF_A_KIND,
			[3, 2]          => HandType::FULL_HOUSE,
			[3, 1, 1]       => HandType::THREE_OF_A_KIND,
			[2, 2, 1]       => HandType::TWO_PAIR,
			[2, 1, 1, 1]    => HandType::ONE_PAIR,
			[1, 1, 1, 1, 1] => HandType::HIGH_CARD
		};

		$cards = array_map([$cardClass, 'from'], $cardStrArray);

		return new static($handType, $cards, (int)$bidStr);
	}


	public static function compare(self $h1, self $h2) : int {
		$cmp = HandType::compare($h1->handType, $h2->handType);
		if($cmp !== 0) {
			return $cmp;
		}

		for($i = 0; $i < 5; $i++) {
			$cmp = $h1->cards[$i]::compare($h1->cards[$i], $h2->cards[$i]);
			if($cmp !== 0) {
				return $cmp;
			}
		}

		return 0;
	}
}