<?php

require_once __DIR__.'/../../../vendor/autoload.php';

use AoC\Year2023\Day07\Hand;

$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$hands = [];
foreach ($file as $line) {
	$hands[] = Hand::parseHandPart2($line);
}

usort($hands, Hand::compare(...));

$sum = 0;
foreach ($hands as $i=>$hand){
	$sum+= ($i+1)*$hand->bid;
}
echo $sum;