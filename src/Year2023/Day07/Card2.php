<?php

namespace AoC\Year2023\Day07;
enum Card2 : string
{
	case _A = "A";
	case _K = "K";
	case _Q = "Q";
	case _J = "J";
	case _T = "T";
	case _9 = "9";
	case _8 = "8";
	case _7 = "7";
	case _6 = "6";
	case _5 = "5";
	case _4 = "4";
	case _3 = "3";
	case _2 = "2";

	public const VALUES = [
		self::_A->value => 14,
		self::_K->value => 13,
		self::_Q->value => 12,
		//		self::_J->value => 11,
		self::_T->value => 10,
		self::_9->value => 9,
		self::_8->value => 8,
		self::_7->value => 7,
		self::_6->value => 6,
		self::_5->value => 5,
		self::_4->value => 4,
		self::_3->value => 3,
		self::_2->value => 2,
		self::_J->value => 1,
	];

	public function numeric() : int {
		return self::VALUES[$this->value];
	}

	public static function compare(self $c1, self $c2) : int {
		return $c1->numeric() <=> $c2->numeric();
	}
}
