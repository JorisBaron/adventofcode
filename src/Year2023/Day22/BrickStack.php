<?php

namespace AoC\Year2023\Day22;

use AoC\Common\StringableStorage;
use AoC\Common\Point3D;
use AoC\Common\Point;
use AoC\Common\ArrayFunc;
use SplQueue;

class BrickStack
{
	/** @var StringableStorage<Brick> */
	protected StringableStorage $bricks;
	/** @var StringableStorage<Point3D, Brick> */
	protected StringableStorage $brickMap;
	/** @var StringableStorage<Point, Brick> */
	protected StringableStorage $heightMap;

	public function __construct() {
		$this->bricks = new StringableStorage();
		$this->brickMap = new StringableStorage();
		$this->heightMap = new StringableStorage();
	}

	public function __clone() : void {
		$this->bricks = clone $this->bricks;
		$this->brickMap = clone $this->brickMap;
		$this->heightMap = clone $this->heightMap;
	}

	/**
	 * @return StringableStorage
	 */
	public function getBricks() : StringableStorage {
		return $this->bricks;
	}

	public function removeBrick(Brick $brick) : void {
		unset($this->bricks[$brick]);
		foreach($brick->getAllPoints() as $point3D) {
			unset($this->brickMap[$point3D]);
			$point2d = $point3D->get2d();
			if($this->heightMap[$point2d]->equals($brick)) {
				unset($this->heightMap[$point2d]);
				for($z = $point3D->z - 1; $z > 0; $z--) {
					$potentialHighestPoint = new Point3D($point2d->x, $point2d->y, $z);
					if($this->brickMap->contains($potentialHighestPoint)) {
						$this->heightMap[$point2d] = $this->brickMap[$potentialHighestPoint];
					}
				}
			}
		}
	}

	/**
	 * @param Point $point
	 * @return Brick|null
	 */
	protected function getHighest(Point $point) : ?Brick {
		if(!$this->heightMap->contains($point)) {
			return null;
		}
		return $this->heightMap[$point];
	}

	public function canDisintegrate(Brick $brick) : bool {
		$supportedBricks = $this->getSupported($brick);
		return ArrayFunc::array_every(
			iterator_to_array($supportedBricks),
			fn(Brick $brick) => $this->getSupporting($brick)->count() > 1
		);
	}

	/**
	 * @param Brick $brick
	 * @return StringableStorage
	 */
	public function getSupported(Brick $brick) : StringableStorage {
		return $this->getAdjacentBrick($brick, new Point3D(0, 0, 1));
	}

	/**
	 * @param Brick $brick
	 * @return StringableStorage
	 */
	public function getSupporting(Brick $brick) : StringableStorage {
		return $this->getAdjacentBrick($brick, new Point3D(0, 0, -1));
	}

	/**
	 * @param Brick   $brick
	 * @param Point3D $direction
	 * @return StringableStorage<Brick>
	 */
	protected function getAdjacentBrick(Brick $brick, Point3D $direction) : StringableStorage {
		$adjacentBricks = new StringableStorage();
		foreach($brick->getAllPoints() as $brickPoint) {
			$adjacentPoint = $brickPoint->add($direction);
			if($this->brickMap->contains($adjacentPoint) && !$this->brickMap[$adjacentPoint]->equals($brick)) {
				$adjacentBricks->attach($this->brickMap[$adjacentPoint]);
			}
		}
		return $adjacentBricks;
	}

	public function addBrick(Brick $brick) : void {
		$brick = $this->settleBrick($brick);
		$this->bricks->attach($brick);
		foreach($brick->getAllPoints() as $brickPoint) {
			$this->brickMap[$brickPoint] = $brick;
			$this->heightMap[$brickPoint->get2d()] = $brick;
		}
	}

	protected function settleBrick(Brick $brick) : Brick {
		$brickPoints = $brick->getAll2DPoints();
		$highestZ = 0;
		foreach($brickPoints as $brickPoint) {
			$highestThere = $this->getHighest($brickPoint);
			if($highestThere !== null && $highestThere->end->z > $highestZ) {
				$highestZ = $highestThere->end->z;
			}
		}

		return $brick->translateZ($highestZ + 1);
	}


	public function calculateDisintegration(Brick $brick) : int {
		$cloned = clone $this;
		$removed = 0;
		$queue = new SplQueue();
		$queue->enqueue($brick);

		while(!$queue->isEmpty()) {
			$brick = $queue->dequeue();
			$supportedBricks = $cloned->getSupported($brick);

			$cloned->removeBrick($brick);
			$removed++;

			foreach($supportedBricks as $supportedBrick) {
				$supporting = $cloned->getSupporting($supportedBrick);
				if($supporting->count() === 0) {
					$queue->enqueue($supportedBrick);
				}
			}
		}

		return $removed-1;
	}
}