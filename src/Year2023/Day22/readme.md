# Part 1

This was interesting. Not too easy, not too hard. Very cool. The "falling" mechanic was interesting to make.
I first used a heap while parsing the input to sort the bricks by Z coordinate,
then add each brick 1 by 1 from the lowest, making them "fall" at the same time.

Then for disintegration, just have to check if th bricks supported by the brick currently tested are also supported 
by at least another brick.

# Part 2

It took me some time.

At first, I wanted to do the following recursion :
1. Input is a set of brick to disintegrate/make fall.
   At the first recursion, this set in only composed of the brick to disintegrate
2. Create a set of bricks that will fall next
3. Get the bricks which were on top of them
4. For each of those bricks :
   1. Get a set of its supporting bricks
   2. Remove the inputted set of bricks from this set
   3. If the set is empty, then the brick will fall. We add it to the set from step **2.**
5. If the set is not empty, make the recursive call with this new set.

This kinda work, except in this case, but the problem is that it does not remember which brick has fallen or not 
during previous calls, which make the following setup returns a wrong answer:
```
EEEEEE
  D F
 CC F
 B  F
AAAAAA
```
1. First call with the set {A} : B and F are now unsupported and will fall.
2. Call with set {B,F} : C is unsupported, and despite F has "fallen", E is still supported by D.
3. Call with set {C} : D falls
4. Call with set {D} : recursive calls do not remember that F previously fell, 
so despite E is not supported anymore by D, it thinks it is still supported by F. So E does not fall

It could have adapted to include all the fallen brick in the set, but I chose to do it another way: 
I delete each fallen brick from the stack.  
This way, when getting the list of bricks supporting a brick, it only returns the one that didn't already fall,
which is more optimized in a sense: we just have to check if the list is empty to know if the brick will fall or not.  
I also dumped the recursiveness in favor of a queue to loop on. It avoids recursive calls overhead 
which reduce execution time.

Example : 
```
EEEEEE    EEEEEE    EEEEEE    EEEEEE    EEEEEE    EEEEEE    ......
..D.F.    ..D.F.    ..D.F.    ..D...    ..D...    ......    ......
.CC.F. -> .CC.F. -> .CC.F. -> .CC... -> ...... -> ...... -> ......
.B..F.    .B..F.    ....F.    ......    ......    ......    ......
AAAAAA    ......    ......    ......    ......    ......    ......
```