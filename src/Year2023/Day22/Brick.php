<?php

namespace AoC\Year2023\Day22;

use AoC\Common\Point3D;
use AoC\Common\Point;

readonly class Brick
{
	public function __construct(
		public Point3D $start,
		public Point3D $end
	) {}


	public static function fromInput(string $input) : static {
		[$startStr, $endStr] = explode('~', $input);
		return new static(
			new Point3D(...explode(',', $startStr)),
			new Point3D(...explode(',', $endStr))
		);
	}

	public function equals(self $brick) : bool {
		return $this->start->equals($brick->start) && $this->end->equals($brick->end);
	}


	public function translateZ(int $newZ) : static {
		$diff = $newZ - $this->start->z;
		if($diff===0){
			return $this;
		}

		$diffPoint = new Point3D(0, 0, $diff);
		return new static(
			$this->start->add($diffPoint),
			$this->end->add($diffPoint),
		);
	}

	/**
	 * @return Point3D[]
	 */
	public function getAllPoints() : array {
		$dir = new Point3D(
			$this->end->x <=> $this->start->x,
			$this->end->y <=> $this->start->y,
			$this->end->z <=> $this->start->z
		);

		$points = [$this->start];
		$point = $this->start;
		while(!$point->equals($this->end)){
			$point = $point->add($dir);
			$points[] = $point;
		}

		return $points;
	}

	/**
	 * @return Point[]
	 */
	public function getAll2DPoints() : array {
		if($this->end->z !== $this->start->z) {
			return [$this->start->get2d()];
		}

		$dir = new Point3D(
			$this->end->x <=> $this->start->x,
			$this->end->y <=> $this->start->y,
			$this->end->z <=> $this->start->z
		);

		$points = [$this->start->get2d()];
		$point = $this->start;
		while(!$point->equals($this->end)){
			$point = $point->add($dir);
			$points[] = $point->get2d();
		}

		return $points;
	}

	public function __toString() : string {
		return sprintf("%s:[%s][%s]", self::class, $this->start, $this->end);
	}
}