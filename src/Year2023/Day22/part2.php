<?php

use AoC\Year2023\Day22\BricksSnapshot;
use AoC\Year2023\Day22\Brick;
use AoC\Year2023\Day22\BrickStack;
use AoC\Common\StringableStorage;

require_once __DIR__.'/../../../vendor/autoload.php';

//$file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$bricksSnapshot = new BricksSnapshot();
foreach($file as $line) {
	$bricksSnapshot->insert(Brick::fromInput($line));
}

$brickStack = new BrickStack();
while(!$bricksSnapshot->isEmpty()){
	$b = $bricksSnapshot->extract();
	$brickStack->addBrick($b);
}

$sum = 0;
foreach($brickStack->getBricks() as $i=>$brick){
	$calc2 = $brickStack->calculateDisintegration($brick);
	$sum += $calc2;
	echo $i.' '.$calc2.PHP_EOL;
}
echo $sum.PHP_EOL;

//101541 OK