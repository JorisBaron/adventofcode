<?php

namespace AoC\Year2023\Day22;

use Override;
use SplHeap;

/**
 * @extends SplHeap<Brick>
 */
class BricksSnapshot extends SplHeap
{
	/**
	 * @param Brick $value1
	 * @param Brick $value2
	 * @return int
	 */
	#[Override]
	protected function compare($value1, $value2) : int {
		return $value2->start->z <=> $value1->start->z ?:
				$value2->start->x <=> $value1->start->x ?:
				$value2->start->y <=> $value1->start->y;
	}
}