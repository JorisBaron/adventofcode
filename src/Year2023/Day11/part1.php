<?php

require_once __DIR__.'/../../../vendor/autoload.php';

use AoC\Year2023\Day11\SpaceGrid;

$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$s = SpaceGrid::createFromInput(iterator_to_array($file));
$galaxies = $s->getGalaxies();
$n = count($galaxies);

$distSum=0;

for ($i=0; $i<$n; $i++){
	for ($j=$i+1; $j<$n; $j++){
		$g1 = $galaxies[$i];
		$g2 = $galaxies[$j];
		$distSum += $galaxies[$i]->manhattanDistance($galaxies[$j]) + $s->getExpandedCountOnPath($galaxies[$i], $galaxies[$j]);
	}
}

echo $distSum;
