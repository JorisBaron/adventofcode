<?php

namespace AoC\Year2023\Day11;
use Exception;
use OutOfBoundsException;
use AoC\Common\Grid;
use AoC\Common\ArrayFunc;
use AoC\Common\Point;

class SpaceGrid extends Grid
{
	protected array $expandedRows = [];
	protected array $expandedCols = [];

	/**
	 * @param string[][] $grid
	 * @throws Exception
	 */
	public function __construct(
		array $grid
	) {
		parent::__construct($grid);

		$this->expand();
	}

	protected function expand() : void {
		for($i = 0; $i < $this->height; $i++) {
			if(ArrayFunc::array_every($this->grid[$i], fn($cell) => $cell === '.')) {
				$this->expandedRows[] = $i;
			}
		}

		for($j = 0; $j < $this->width; $j++) {
			if(ArrayFunc::array_every(
				array_map(fn($row) => $row[$j], $this->grid),
				fn($cell) => $cell === '.'
			)
			) {
				$this->expandedCols[] = $j;
			}
		}
	}

	public function getExpandedCols(int $x1, int $x2) : array {
		if($x1 > $x2) {
			[$x2, $x1] = [$x1, $x2];
		}
		return array_filter($this->expandedCols, fn($colIndex) => $x1 <= $colIndex && $colIndex <= $x2);
	}

	public function getExpandedRows(int $y1, int $y2) : array {
		if($y1 > $y2) {
			[$y2, $y1] = [$y1, $y2];
		}
		return array_filter($this->expandedRows, fn($rowIndex) => $y1 <= $rowIndex && $rowIndex <= $y2);
	}

	public function getExpandedCountOnPath(Point $start, Point $end, int $multiplier = 2) : int {
		return (count($this->getExpandedCols($start->x, $end->x)) + count($this->getExpandedRows($start->y, $end->y))) * ($multiplier - 1);
	}

	/**
	 * @return Point[]
	 */
	public function getGalaxies() : array {
		$galaxies = [];
		foreach($this->grid as $y => $row) {
			foreach($row as $x => $cell) {
				if($cell === "#") {
					$galaxies[] = new Point($x, $y);
				}
			}
		}
		return $galaxies;
	}

}