<?php

require_once __DIR__.'/../../../vendor/autoload.php';

use AoC\Year2023\Day02\Game;

$file = new SplFileObject("input/input.txt");

$gamePowerSum = 0;
foreach($file as $gameString) {
	$game = Game::parseGame($gameString);
	$gamePowerSum += $game->power();
}

echo $gamePowerSum;