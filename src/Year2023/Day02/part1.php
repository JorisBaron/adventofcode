<?php

require_once __DIR__.'/../../../vendor/autoload.php';

use AoC\Year2023\Day02\Game;

$file = new SplFileObject("input/input.txt");
$initialConfig = [
	"red" => 12,
	"green" => 13,
	"blue" => 14
];

$gameIdSum = 0;
foreach ($file as $gameString) {
	$game = Game::parseGame($gameString);
	if($game->isPossible(...$initialConfig)) {
		$gameIdSum += $game->id;
	}
}

echo $gameIdSum;