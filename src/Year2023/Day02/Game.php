<?php

namespace AoC\Year2023\Day02;

readonly class Game
{
	public function __construct(
		public int $id,
		public int $red,
		public int $green,
		public int $blue,
	) {}

	public static function parseGame(string $gameString) : static {
		$inputSplitColon = explode(": ", trim($gameString));

		$id = (int)substr($inputSplitColon[0], 5);

		$sets = explode("; ", $inputSplitColon[1]);
		$rgb = [
			'red'   => 0,
			'green' => 0,
			'blue'  => 0
		];
		foreach($sets as $set) {
			$cubesList = explode(", ", $set);
			foreach($cubesList as $cubeCount) {
				[$number, $color] = explode(" ", $cubeCount);
				$rgb[$color] = max((int)$number, $rgb[$color]);
			}
		}

		return new static($id, $rgb['red'], $rgb['green'], $rgb['blue']);
	}

	public function isPossible(int $red, int $green, int $blue) : bool {
		return $this->red <= $red && $this->green <= $green && $this->blue <= $blue;
	}

	public function power() : int {
		return $this->red * $this->green * $this->blue;
	}
}