<?php

use AoC\Year2023\Day19\Common;
use AoC\Year2023\Day19\WorkflowCollection;
use AoC\Year2023\Day19\RangedPart;

require_once __DIR__.'/../../../vendor/autoload.php';

//$file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

[$rulesStr, $partsStr] = Common::parseInput($file);

$wkfCollection = WorkflowCollection::fromInput($rulesStr);

$toProcess = new SplQueue();
$toProcess->enqueue([RangedPart::createFullRange(), 'in']);
/** @var RangedPart[] $accepted */
$accepted = [];
while(!$toProcess->isEmpty()){
	$current = $toProcess->dequeue();
	$wkf = $wkfCollection->get($current[1]);
	$outs = $wkf->computeOutcomeRanges($current[0]);

	foreach($outs as $out) {
		if($out[1] === 'A') {
			$accepted[] = $out[0];
		} elseif($out[1] !== 'R') {
			$toProcess->enqueue($out);
		}
	}
}

$s = 0;
foreach($accepted as $value) {
	$s += $value->total();
}


echo $s;