<?php

namespace AoC\Year2023\Day19;

class WorkflowCollection
{
	/** @var array<string,Workflow> */
	protected array $workflows = [];

	public static function fromInput(array $workflowsStrArray) : static {
		$coll = new static();
		foreach($workflowsStrArray as $workflowStr) {
			$workflow = Workflow::fromInput($workflowStr);
			$coll->workflows[$workflow->getName()] = $workflow;
		}
		return $coll;
	}

	public function get(string $workflowName) : Workflow {
		return $this->workflows[$workflowName];
	}
}