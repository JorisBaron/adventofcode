<?php

namespace AoC\Year2023\Day19;

class RangedPart
{
	protected function __construct(
		public Range $x,
		public Range $m,
		public Range $a,
		public Range $s,
	) {}

	public static function createFullRange() : static {
		return new static(
			new Range(Rule::MIN_VALUE, Rule::MAX_VALUE),
			new Range(Rule::MIN_VALUE, Rule::MAX_VALUE),
			new Range(Rule::MIN_VALUE, Rule::MAX_VALUE),
			new Range(Rule::MIN_VALUE, Rule::MAX_VALUE),
		);
	}

	public function __clone() {
		$this->x = clone $this->x;
		$this->m = clone $this->m;
		$this->a = clone $this->a;
		$this->s = clone $this->s;
	}

	public function getRange(string $category) : Range {
		return $this->{$category};
	}

	public function setRange(string $category, Range $range) : void {
		$this->{$category} = $range;
	}

	/**
	 * @param string $category
	 * @param Range  $intersecting
	 * @return void
	 * @throws NoRangeIntersection
	 */
	public function intersectRange(string $category, Range $intersecting) : void {
		$this->setRange($category, $this->getRange($category)->intersection($intersecting));
	}


	public function total() : int {
		return $this->x->length() * $this->m->length() * $this->a->length() * $this->s->length();
	}
}