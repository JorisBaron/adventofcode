<?php

namespace AoC\Year2023\Day19;

readonly class Range
{
	public function __construct(
		public int $min,
		public int $max,
	) {}

	public function length() : int {
		return $this->max-$this->min+1;
	}

	/**
	 * @throws NoRangeIntersection
	 */
	public function intersection(self $range) : ?static {
		$inter = new static(
			max($this->min, $range->min),
			min($this->max, $range->max)
		);

		if($inter->min > $inter->max) {
			throw new NoRangeIntersection();
		}

		return $inter;
	}

	public function includes(int $x) : bool {
		return $this->min <= $x && $x <= $this->max;
	}
}