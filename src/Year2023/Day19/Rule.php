<?php

namespace AoC\Year2023\Day19;

use Closure;

readonly class Rule
{
	const int MIN_VALUE = 1;
	const int MAX_VALUE = 4000;

	protected function __construct(
		public ?string $category,
		public Range $matchingRange,
		public string $destination,
		public Workflow $parentWorkflow,
	) {}

	public static function fromInput(string $input, Workflow $workflow) : static {
		if(preg_match('/^([xmas])([<>])(\d+):([a-zAR]+)$/', $input, $matches)){
			$value = (int)$matches[3];
			$category = $matches[1];
			$destination = $matches[4];
			if($matches[2]==='>') {
				$range = new Range($value + 1, self::MAX_VALUE);
			} else {
				$range = new Range(self::MIN_VALUE, $value - 1);
			}
		} else {
			$category = null;
			$range = new Range(self::MIN_VALUE, self::MAX_VALUE);
			$destination = $input;
		}

		return new static(
			$category,
			$range,
			$destination,
			$workflow
		);
	}

	public function check(Part $part) : bool {
		if($this->category === null) {
			return true;
		}

		return $this->matchingRange->includes($part->get($this->category));
	}

	/**
	 * @return Range
	 */
	public function getInverseRange() : Range {
		if($this->matchingRange->min === self::MIN_VALUE) {
			return new Range($this->matchingRange->max+1, self::MAX_VALUE);
		} else {
			return new Range(self::MIN_VALUE, $this->matchingRange->min-1);
		}
	}
}