<?php

namespace AoC\Year2023\Day19;

use Exception;

class Workflow
{
	/** @var Rule[] */
	protected array $rules;

	protected function __construct(
		protected string $name
	) {}

	public static function fromInput(string $input) {
		preg_match('/^([a-z]+)\{([^}]+)}$/', $input, $matches);
		$wkf = new static($matches[1]);
		foreach(explode(',', $matches[2]) as $ruleStr) {
			$wkf->rules[] = Rule::fromInput($ruleStr, $wkf);
		}
		return $wkf;
	}

	public function getName() : string {
		return $this->name;
	}

	/**
	 * @throws Exception
	 */
	public function processPart(Part $part) : string {
		foreach($this->rules as $rule){
			if($rule->check($part)){
				return $rule->destination;
			}
		}
		throw new Exception("No default rule");
	}

	/**
	 * @param RangedPart $start
	 * @return RangedPart[]
	 */
	public function computeOutcomeRanges(RangedPart $start) : array {
		$current = $start;
		$outcomes = [];
		foreach($this->rules as $rule) {
			if($rule->category === null) {
				$outcomes[] = [$current, $rule->destination];
				break;
			}

			try {
				$outcome = clone $current;
				$outcome->intersectRange($rule->category, $rule->matchingRange);
				$outcomes[] = [$outcome, $rule->destination];
			} catch(NoRangeIntersection) {}

			try {
				$current->intersectRange($rule->category, $rule->getInverseRange());
			} catch(NoRangeIntersection) {
				break;
			}
		}
		return $outcomes;
	}
}