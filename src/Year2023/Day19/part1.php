<?php

use AoC\Year2023\Day19\Common;
use AoC\Year2023\Day19\WorkflowCollection;
use AoC\Year2023\Day19\Part;

require_once __DIR__.'/../../../vendor/autoload.php';

//$file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

[$rulesStr, $partsStr] = Common::parseInput($file);

$wkfCollection = WorkflowCollection::fromInput($rulesStr);

$sum = 0;
foreach($partsStr as $partStr) {
	$part = Part::fromInput($partStr);
	$next = "in";

	while($next !== "A" && $next !== 'R') {
		$next = $wkfCollection->get($next)->processPart($part);
	}

	if($next === 'A') {
		$sum += $part->total();
	}
}

echo $sum;