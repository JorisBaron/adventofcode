<?php

namespace AoC\Year2023\Day19;

readonly class Part
{
	protected function __construct(
		public int $x,
		public int $m,
		public int $a,
		public int $s,
	) {}

	public static function fromInput(string $input) : static {
		preg_match('/^\{x=(\d+),m=(\d+),a=(\d+),s=(\d+)}$/', $input, $matches);
		unset($matches[0]);
		return new static(...array_map(intval(...), $matches));
	}

	public function get(string $category) {
		return $this->{$category};
	}

	public function total() : int {
		return $this->x + $this->m + $this->a + $this->s;
	}
}