<?php

namespace AoC\Year2023\Day19;

use SplFileObject;

class Common
{
	public static function parseInput(SplFileObject $file) : array {
		$rulesStrArray = [];
		while(($line = $file->fgets()) !== "") {
			$rulesStrArray[] = $line;
		}

		$partsStrArray = [];
		while(!$file->eof()) {
			$partsStrArray[] = $file->fgets();
		}

		return [$rulesStrArray, $partsStrArray];
	}
}