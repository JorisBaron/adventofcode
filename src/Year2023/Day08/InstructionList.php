<?php

namespace AoC\Year2023\Day08;

class InstructionList
{
	protected string $instructions;
	protected int    $length;
	protected int    $current = 0;

	public function __construct(string $instructions) {
		$this->instructions = $instructions;
		$this->length = strlen($instructions);
	}

	public function next() : string {
		return $this->instructions[($this->current++) % $this->length];
	}

	public function getStepCount() : int {
		return $this->current;
	}

	public function reset() : void {
		$this->current = 0;
	}
}