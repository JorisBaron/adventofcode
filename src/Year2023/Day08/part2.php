<?php

require_once __DIR__.'/../../../vendor/autoload.php';

use AoC\Year2023\Day08\InstructionList;
use AoC\Year2023\Day08\Network;
use AoC\Year2023\Day08\Node;
use AoC\Common\Math;


$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$instructionList = new InstructionList($file->fgets());
$file->fgets(); // Skip empty line #2

$network = new Network();
while (!$file->eof()) {
	$network->addNode(Node::createFromInputLine($file->fgets()));
}

/** @var Node[] $startingNodes */
$startingNodes = array_values($network->getNodesEndingWith("A"));
$cyclesLength = [];
foreach ($startingNodes as $node) {
	$instructionList->reset();
	$starting = $node;
	$current = $node;
	$i = 0;
	$traversed = [$node->name => 0];

	do  {
		$nextInstruction = $instructionList->next();
		$current = $network->getNode($current->getNext($nextInstruction));
	} while (!str_ends_with($current->name, "Z"));

	$cyclesLength[] = $instructionList->getStepCount();
}

$lcm = 1;
foreach ($cyclesLength as $value) {
	$lcm = Math::lcm($lcm, $value);
}

echo $lcm;
