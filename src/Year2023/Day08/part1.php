<?php

require_once __DIR__.'/../../../vendor/autoload.php';

use AoC\Year2023\Day08\InstructionList;
use AoC\Year2023\Day08\Network;
use AoC\Year2023\Day08\Node;


$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$instructionList = new InstructionList($file->fgets());
$file->fgets(); // Skip empty line #2

$network = new Network();
while (!$file->eof()) {
	$network->addNode(Node::createFromInputLine($file->fgets()));
}

$currentNode = $network->getNode("AAA");
while ($currentNode->name !== "ZZZ"){
	$instruction = $instructionList->next();
	$nextNodeName = match ($instruction) {
		"R" => $currentNode->right,
		"L" => $currentNode->left,
	};
	$currentNode = $network->getNode($nextNodeName);
}

echo $instructionList->getStepCount();