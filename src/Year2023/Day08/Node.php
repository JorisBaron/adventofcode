<?php

namespace AoC\Year2023\Day08;

readonly class Node
{
	public function __construct(
		public string $name,
		public string $left,
		public string $right,
	) {}

	public static function createFromInputLine(string $input) : static {
		preg_match('/^([0-9A-Z]+)\s*=\s*\(([0-9A-Z]+),\s*([0-9A-Z]+)\)$/', $input, $matches);
		return new static(
			name : $matches[1],
			left : $matches[2],
			right: $matches[3]
		);
	}

	public function getNext(string $side) : string {
		return match (strtolower($side)) {
			"r", "right" => $this->right,
			"l", "left"  => $this->left,
		};
	}
}