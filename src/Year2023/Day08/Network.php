<?php

namespace AoC\Year2023\Day08;

class Network
{
	/** @var Node[] */
	protected array $nodes = [];

	public function addNode(Node $node) : void {
		$this->nodes[$node->name] = $node;
	}

	public function getNode(string $nodeName) : Node {
		return $this->nodes[$nodeName];
	}

	public function getNodesEndingWith(string $lastChar) : array {
		return array_filter($this->nodes, fn(Node $node) => str_ends_with($node->name, $lastChar));
	}
}