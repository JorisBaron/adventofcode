<?php

function doHash(string $input) : int {
	$val = 0;
	for ($i=0; $i<strlen($input); $i++){
		$val = (($val + ord($input[$i])) * 17) & 255;
	}
	return $val;
}