<?php

require_once __DIR__ . '/common.php';

$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$line = $file->fgets();
$lineArr = explode(',', $line);

$boxes = array_fill(0, 256, []);

foreach ($lineArr as $str) {
	if (str_ends_with($str, "-")) {
		$label = substr($str, 0, -1);
		$boxNum = doHash($label);
		unset($boxes[$boxNum][$label]);
	} else {
		[$label, $value] = explode('=',$str);
		$boxNum = doHash($label);
		$boxes[$boxNum][$label] = (int)$value;
	}
}

$sum = 0;
foreach ($boxes as $bI => $box) {
	foreach (array_keys($box) as $slot => $label) {
		$sum += ($bI + 1) * ($slot + 1) * $box[$label];
	}
}
echo $sum;