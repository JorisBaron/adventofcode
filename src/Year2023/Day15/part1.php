<?php

require_once __DIR__.'/common.php';

$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$line = $file->fgets();
$lineArr = explode(',', $line);

$sum = 0;
foreach ($lineArr as $str) {
	$sum += doHash($str);
}
echo $sum;