<?php

namespace AoC\Year2023\Day20\Circuit;

use Override;

class FlipFlop extends AbstractGate
{
	protected const string GATE_PREFIX = '%';

	protected PulseType $value = PulseType::LOW;

	#[Override]
	public function addInput(Gate $gate) : void {}

	#[Override]
	public function inputValue(Gate $gate, PulseType $pulseType) : ?PulseUpdate {
		if($pulseType === PulseType::HIGH) {
			return null;
		}

		$this->value = $this->value->getInvert();
		$update = new PulseUpdate($this, $this->value, $this->getOutputs());
		$this->addPulseHistory($update->pulse);

		return $update;
	}


	#[Override]
	public static function acceptConfig(string $configString) : bool {
		return str_starts_with($configString, self::GATE_PREFIX);
	}

	#[Override]
	public static function fromConfig(string $configString) : static {
		return new static(substr($configString, 1));
	}
}