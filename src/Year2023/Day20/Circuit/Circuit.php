<?php

namespace AoC\Year2023\Day20\Circuit;

use SplQueue;

class Circuit
{
	/** @var array<string, Gate> */
	protected array $gates = [];
	/** @var SplQueue<PulseUpdate> */
	protected SplQueue $ongoingPulses;

	protected array $pulseCount = [];
	protected int $buttonPressCount = 0;

	public function __construct() {
		$this->ongoingPulses = new SplQueue();
		foreach(PulseType::cases() as $pulseType){
			$this->pulseCount[$pulseType->name] = 0;
		}
	}

	public function addGate(Gate $gate) : void {
		$this->gates[$gate->getName()] = $gate;
	}

	public function hasGate(string $name) : bool {
		return isset($this->gates[$name]);
	}

	public function getGate(string $name) : Gate {
		return $this->gates[$name];
	}

	/**
	 * @return int
	 */
	public function getPulseCountProduct() : int {
		return array_product($this->pulseCount);
	}


	public function start() : void {
		$this->buttonPressCount++;
		$this->pulseCount[PulseType::LOW->name] += 1;

		$pulseUpdate = $this->gates[Broadcaster::BROADCASTER_NAME]->inputValue(null, PulseType::LOW);

		$this->ongoingPulses->enqueue($pulseUpdate);

		while(!$this->ongoingPulses->isEmpty()) {
			$pulseUpdate = $this->ongoingPulses->dequeue();
			$nextPulseUpdates = $pulseUpdate->apply();
			$this->pulseCount[$pulseUpdate->pulse->name] += count($nextPulseUpdates);

			foreach($nextPulseUpdates as $nextPulseUpdate) {
				$this->ongoingPulses->enqueue($nextPulseUpdate);
			}
		}
	}
}