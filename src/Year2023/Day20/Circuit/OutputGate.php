<?php

namespace AoC\Year2023\Day20\Circuit;

use AoC\Year2023\Day20\Circuit\AbstractGate;
use Override;

class OutputGate extends AbstractGate
{
	/** @var PulseType[] */
	protected array $pulsesHistory = [];

	#[Override] public static function acceptConfig(string $configString) : bool {
		return true;
	}

	#[Override] public static function fromConfig(string $configString) : static {
		return new static($configString);
	}

	/**
	 * @inheritDoc
	 */
	#[Override] public function addInput(Gate $gate) : void {}

	/**
	 * @inheritDoc
	 */
	#[Override]
	public function inputValue(Gate $gate, PulseType $pulseType) : ?PulseUpdate {
		$this->pulsesHistory[] = $pulseType;
		return null;
	}

	/**
	 * @return array
	 */
	public function getPulsesHistory() : array {
		return $this->pulsesHistory;
	}

	public function resetPulsesHistory() : void {
		$this->pulsesHistory = [];
	}
}