<?php

namespace AoC\Year2023\Day20\Circuit;

class CircuitFactory
{
	protected array $gateClasses = [];
	protected string $untypedGateClass;

	/**
	 * @template T of Gate
	 * @param class-string<T> $gateClass
	 * @return void
	 */
	public function registerGateClass(string $gateClass) : void {
		$this->gateClasses[] = $gateClass;
	}

	/**
	 * @template T of Gate
	 * @param class-string<T> $gateClass
	 * @return void
	 */
	public function registerDefaultGate(string $gateClass) : void {
		$this->untypedGateClass = $gateClass;
	}

	public function parseConfiguration(array $instructions) : Circuit {
		$gatesOutputs = [];
		$circuit = new Circuit();

		foreach($instructions as $instruction){
			[$moduleRawName, $rawOutputs] = explode(' -> ', $instruction);
			foreach($this->gateClasses as $gateClass){
				if([$gateClass, 'acceptConfig']($moduleRawName)){
					/** @var Gate $gate */
					$gate = [$gateClass, 'fromConfig']($moduleRawName);
					$circuit->addGate($gate);
					$gatesOutputs[$gate->getName()] = explode(', ', $rawOutputs);
				}
			}
		}

		foreach($gatesOutputs as $gateName => $gateOutputs) {
			$sender = $circuit->getGate($gateName);
			foreach($gateOutputs as $outputGateName) {
				if(!$circuit->hasGate($outputGateName)) {
					$receiver = new ($this->untypedGateClass)($outputGateName);
					$circuit->addGate($receiver);
				} else {
					$receiver = $circuit->getGate($outputGateName);
				}
				$sender->addOutput($receiver);
				$receiver->addInput($sender);
			}
		}

		return $circuit;
	}
}