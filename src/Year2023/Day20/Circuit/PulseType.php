<?php

namespace AoC\Year2023\Day20\Circuit;

enum PulseType
{
	case LOW;
	case HIGH;

	public function getInvert() : self {
		return match ($this) {
			self::LOW => self::HIGH,
			self::HIGH => self::LOW,
		};
	}
}
