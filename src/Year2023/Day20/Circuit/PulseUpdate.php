<?php

namespace AoC\Year2023\Day20\Circuit;

readonly class PulseUpdate
{
	/**
	 * @param Gate      $sender
	 * @param PulseType $pulse
	 * @param Gate[]    $receivers
	 */
	public function __construct(
		public Gate $sender,
		public PulseType $pulse,
		public array $receivers
	) {}

	/**
	 * @return PulseUpdate[]
	 */
	public function apply() : array {
		$nextUpdates = [];
		foreach($this->receivers as $gate) {
			$update = $gate->inputValue($this->sender, $this->pulse);
			if($update !== null) {
				$nextUpdates[] = $update;
			}
		}

		return $nextUpdates;
	}
}