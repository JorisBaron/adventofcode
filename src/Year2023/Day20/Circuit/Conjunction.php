<?php

namespace AoC\Year2023\Day20\Circuit;


use Override;
use AoC\Common\ArrayFunc;

class Conjunction extends AbstractGate
{
	protected const string GATE_PREFIX = '&';

	/** @var array<string, PulseType> */
	protected array $values = [];

	/**
	 * @inheritDoc
	 */
	#[Override]
	public function addInput(Gate $gate) : void {
		$this->values[$gate->getName()] = PulseType::LOW;
	}

	/**
	 * @inheritDoc
	 */
	#[Override]
	public function inputValue(Gate $gate, PulseType $pulseType) : PulseUpdate {
		$this->values[$gate->getName()] = $pulseType;

		$update = new PulseUpdate(
			$this,
			ArrayFunc::array_every($this->values, fn($pulse) => $pulse === PulseType::HIGH)
				? PulseType::LOW
				: PulseType::HIGH,
			$this->getOutputs()
		);

		$this->addPulseHistory($update->pulse);

		return $update;
	}

	#[Override]
	public static function acceptConfig(string $configString) : bool {
		return str_starts_with($configString, self::GATE_PREFIX);
	}

	#[Override]
	public static function fromConfig(string $configString) : static {
		return new static(substr($configString, 1));
	}
}