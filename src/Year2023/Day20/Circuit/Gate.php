<?php

namespace AoC\Year2023\Day20\Circuit;

interface Gate
{
	public static function acceptConfig(string $configString) : bool;

	public static function fromConfig(string $configString) : static;

	/**
	 * @return string
	 */
	public function getName() : string;

	/**
	 * @param Gate $gate
	 * @return void
	 */
	public function addInput(Gate $gate) : void;

	/**
	 * @param Gate      $gate
	 * @param PulseType $pulseType
	 * @return PulseUpdate|null
	 */
	public function inputValue(Gate $gate, PulseType $pulseType) : ?PulseUpdate;

	/**
	 * @param Gate $gate
	 * @return void
	 */
	public function addOutput(Gate $gate) : void;

	/**
	 * @return Gate[]
	 */
	public function getOutputs() : array;
}