<?php

namespace AoC\Year2023\Day20\Circuit;


use Override;

class Broadcaster extends AbstractGate
{
	const string BROADCASTER_NAME = 'broadcaster';

	protected PulseType $value;

	public function __construct() {
		parent::__construct(self::BROADCASTER_NAME);
	}

	/**
	 * @inheritDoc
	 */
	#[Override]
	public function addInput(Gate $gate) : void {}

	/**
	 * @inheritDoc
	 */
	#[Override]
	public function inputValue(?Gate $gate, PulseType $pulseType) : PulseUpdate {
		$this->value = $pulseType;
		return new PulseUpdate($this, $this->value, $this->outputs);
	}


	#[\Override] public static function acceptConfig(string $configString) : bool {
		return $configString === self::BROADCASTER_NAME;
	}

	#[\Override] public static function fromConfig(string $configString) : static {
		return new static();
	}
}