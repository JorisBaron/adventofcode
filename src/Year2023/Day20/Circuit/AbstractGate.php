<?php

namespace AoC\Year2023\Day20\Circuit;

use Override;

abstract class AbstractGate implements Gate
{
	protected string $name;
	/** @var Gate[] */
	protected array $outputs = [];
	/** @var PulseType[] */
	protected array $pulsesHistory = [];

	public function __construct(string $name) {
		$this->name = $name;
	}

	/**
	 * @inheritDoc
	 */
	#[Override]
	public function getName() : string {
		return $this->name;
	}

	/**
	 * @inheritDoc
	 */
	#[Override]
	public function addOutput(Gate $gate) : void {
		$this->outputs[] = $gate;
	}

	/**
	 * @inheritDoc
	 */
	#[Override]
	public function getOutputs() : array {
		return $this->outputs;
	}

	public function addPulseHistory(PulseType $pulseType) : void {
		$this->pulsesHistory[] = $pulseType;
	}

	/**
	 * @return array
	 */
	public function getPulsesHistory() : array {
		return $this->pulsesHistory;
	}

	public function resetPulsesHistory() : void {
		$this->pulsesHistory = [];
	}
}