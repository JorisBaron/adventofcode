<?php

use AoC\Year2023\Day20\Circuit\CircuitFactory;
use AoC\Year2023\Day20\Circuit\Broadcaster;
use AoC\Year2023\Day20\Circuit\FlipFlop;
use AoC\Year2023\Day20\Circuit\Conjunction;
use AoC\Year2023\Day20\Circuit\OutputGate;
use AoC\Year2023\Day20\Circuit\PulseType;
use AoC\Year2023\Day20\Circuit\Circuit;
use AoC\Year2023\Day20\Circuit\AbstractGate;
use AoC\Common\ArrayFunc;
use AoC\Common\Math;

require_once __DIR__.'/../../../vendor/autoload.php';

//$file = new SplFileObject(__DIR__."/input/test.txt");
//$file = new SplFileObject(__DIR__."/input/test2.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$factory = new CircuitFactory();
$factory->registerGateClass(Broadcaster::class);
$factory->registerGateClass(FlipFlop::class);
$factory->registerGateClass(Conjunction::class);
$factory->registerDefaultGate(OutputGate::class);

$circuit = $factory->parseConfiguration(iterator_to_array($file));


$rxInputInputs = ['vz' => null, 'bq' => null, 'qh' => null, 'lt' => null];

for($i=1; true; $i++) {
	$circuit->start();

	foreach($rxInputInputs as $rxInputInput => &$buttonPressCount) {
		/** @var AbstractGate $gate */
		$gate = $circuit->getGate($rxInputInput);
		if(in_array(PulseType::HIGH, $gate->getPulsesHistory()) && $buttonPressCount === null) {
			$buttonPressCount = $i;
		}
	}

	if(ArrayFunc::array_every($rxInputInputs, fn($value) => $value!==null)) {
		break;
	}
}

var_dump($rxInputInputs);
var_dump(Math::lcm(...$rxInputInputs));