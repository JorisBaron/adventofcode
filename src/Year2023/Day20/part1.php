<?php

use AoC\Year2023\Day20\Circuit\CircuitFactory;
use AoC\Year2023\Day20\Circuit\Broadcaster;
use AoC\Year2023\Day20\Circuit\FlipFlop;
use AoC\Year2023\Day20\Circuit\Conjunction;
use AoC\Year2023\Day20\Circuit\OutputGate;

require_once __DIR__.'/../../../vendor/autoload.php';

//$file = new SplFileObject(__DIR__."/input/test.txt");
//$file = new SplFileObject(__DIR__."/input/test2.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$factory = new CircuitFactory();
$factory->registerGateClass(Broadcaster::class);
$factory->registerGateClass(FlipFlop::class);
$factory->registerGateClass(Conjunction::class);
$factory->registerDefaultGate(OutputGate::class);

$circuit = $factory->parseConfiguration(iterator_to_array($file));

for($i=0; $i<1000; $i++) {
	$circuit->start();
}
echo $circuit->getPulseCountProduct();