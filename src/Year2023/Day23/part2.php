<?php

use AoC\Year2023\Day23\HikingGrid;
use AoC\Common\Graph\Graph;
use AoC\Common\Graph\Node;
use AoC\Common\StringableStorage;
use AoC\Year2023\Day23\Hike;

var_dump(ini_get('opcache.enable'));
var_dump(ini_get('opcache.enable_cli'));

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$grid = HikingGrid::createFromInput(iterator_to_array($file));

$graph = new Graph();
foreach($grid->getPaths() as $hikingPath){
	$startNodeIdent = $hikingPath->start->toString();
	$endNodIdent = $hikingPath->end->toString();

	$graph->createEdge($startNodeIdent, $endNodIdent, $hikingPath->length, false);
}
$startNode = $graph->getNode($grid->start->toString());

$startTime = microtime(true);

$initHike = new Hike($startNode);
$cache = new StringableStorage();
$hikeLength = maxPath(
	$graph,
	$initHike,
	$graph->getNode($grid->end->toString()),
	0,
	$cache
);

var_dump($hikeLength);

// echo $startNode->identifier;
// $current = $startNode;
// foreach($hike->getPath() as $edge) {
// 	$next = $edge->getConnected($current);
// 	echo sprintf(' --(%d)-> %s', $edge->getWeight(), $next->identifier);
// 	$current = $next;
// }



function maxPath(Graph $graph, Hike $currentHike, Node $end, int $longest, StringableStorage &$cache) : int {

	// echo $hikeToProcess->count().PHP_EOL;
	// /** @var Hike $hike */
	// $hike = $hikeToProcess->pop();
	global $startTime;
	$hikeEnd = $currentHike->getEnd();

	if($hikeEnd->identifier === $end->identifier) {
		$currentLongest = max($currentHike->getLength(), $longest);
		if($currentLongest != $longest) {
			echo sprintf("%d (%f)", $currentLongest, microtime(true)-$startTime).PHP_EOL;
		}
		return $currentLongest;
	}

	foreach($graph->getNode($hikeEnd->identifier) as $neighborIdent) {
		$neighbor = $graph->getNode($neighborIdent);
		$edge = $graph->getEdge($hikeEnd->identifier, $neighborIdent);
		if(!$currentHike->hasNode($neighbor)) {
			$currentHike->addEdge($edge);
			$longest = maxPath($graph, $currentHike, $end, $longest, $cache);
			$currentHike->removeLastEdge();
		}
	}

	return $longest;
}

//6386 (0.91)
//6654 (65.43)
//6658 (126.71)
//6734 (209.66)