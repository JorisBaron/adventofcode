<?php

namespace AoC\Year2023\Day23;

use AoC\Common\Grid;
use AoC\Common\Point;

/**
 * @extends Grid<string>
 */
class HikingGrid extends Grid
{
	protected const string FOREST_SYMBOL = '#';

	public readonly Point $start;
	public readonly Point $end;

	/** @var Point[] */
	protected array $intersections = [];
	/** @var HikingPath[] */
	protected array $paths = [];

	public function __construct(array $grid) {
		parent::__construct($grid);
		$this->start = new Point(1, 0);
		$this->end = new Point($this->width-2, $this->height-1);

		$this->findIntersections();
		$this->calculatePaths();
	}

	/**
	 * @return HikingPath[]
	 */
	public function getPaths() : array {
		return $this->paths;
	}

	public function getAdjacents(Point $point) : array {
		$adjacents = [];
		foreach([
			new Point( 1, 0),
			new Point(-1, 0),
			new Point( 0, 1),
			new Point( 0,-1)
		] as $direction){
			$adjacent = $point->add($direction);
			if($this->isInBound($adjacent) && $this->get($adjacent)!==self::FOREST_SYMBOL){
				$adjacents[] = $adjacent;
			}
		}
		return $adjacents;
	}

	public function getNexts(Point $point, Point $previous) : array {
		$adjacents = [];
		foreach([
			new Point( 1, 0),
			new Point(-1, 0),
			new Point( 0, 1),
			new Point( 0,-1)
		] as $direction){
			$adjacent = $point->add($direction);
			if($this->isInBound($adjacent) && $this->get($adjacent)!==self::FOREST_SYMBOL && !$adjacent->equals($previous)){
				$adjacents[] = $adjacent;
			}
		}
		return $adjacents;
	}

	protected function findIntersections() : void {
		foreach($this->grid as $y=>$row) {
			foreach($row as $x=>$cell) {
				if($this->grid[$y][$x] === self::FOREST_SYMBOL){
					continue;
				}

				$point = new Point($x, $y);
				$adjacents = $this->getAdjacents($point);
				if(count($adjacents)>2){
					$this->intersections[] = $point;
				}
			}
		}
	}

	protected function calculatePaths() : void {
		// start
		$this->paths[] = $this->calculatePath($this->start, $this->getAdjacents($this->start)[0]);

		// others
		foreach($this->intersections as $intersection){
			foreach([
				'^' => new Point( 0,-1),
				'<' => new Point(-1, 0),
				'>' => new Point( 1, 0),
				'v' => new Point( 0, 1),
			] as $symbol=>$direction){
				$adjacent = $intersection->add($direction);
				if(!$this->isInBound($adjacent) || $this->get($adjacent) !== $symbol){
					continue;
				}

				$this->paths[] = $this->calculatePath($intersection, $adjacent);
			}
		}
	}

	protected function calculatePath(Point $start, Point $first) : HikingPath {
		$lastsVisited = [$first, $this->getNexts($first, $start)[0]];
		$length = 2;
		do {
			$next = $this->getNexts($lastsVisited[1], $lastsVisited[0])[0] ?? null;
			if($next === null){
				break;
			}
			array_shift($lastsVisited);
			$lastsVisited[] = $next;
			$length++;
		} while(!in_array($this->get($lastsVisited[0]), ['^','v','<','>']));
		return new HikingPath($start, $lastsVisited[1]??$lastsVisited[0], $length);
	}

}