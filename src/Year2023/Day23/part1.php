<?php

use AoC\Year2023\Day23\HikingGrid;
use AoC\Common\Graph\Graph;
use AoC\Common\Graph\Node;
use AoC\Common\Graph\Edge;
use AoC\Common\StringableStorage;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$grid = HikingGrid::createFromInput(iterator_to_array($file));



$graph = new Graph();
foreach($grid->getPaths() as $hikingPath){
	$startNodeIdent = $hikingPath->start->toString();
	$endNodIdent = $hikingPath->end->toString();

	$graph->createEdge($startNodeIdent, $endNodIdent, $hikingPath->length);
}
$startNode = $graph->getNode($grid->start->toString());

[$length, $path] = maxDijkstra(
	$graph,
	$startNode,
	$graph->getNode($grid->end->toString()),
);

echo $length.PHP_EOL;

echo $startNode->identifier;
$current = $startNode;
/** @var Edge $edge */
foreach($path as $edge) {
	$next = $edge->getConnected($current);
	echo sprintf(' --(%d)-> %s', $edge->getWeight(), $next->identifier);
	$current = $next;
}


function maxDijkstra(Graph $graph, Node $start, Node $end) : array {
	/** @var StringableStorage<Node,int> $dist */
	$dist = new StringableStorage();

	/** @var StringableStorage<Node, Edge> $prevEdges */
	$prevEdges = new StringableStorage();
	$prevEdges[$start] = null;

	/** @var StringableStorage<Node> $nodesToProcess */
	$nodesToProcess = new StringableStorage();

	/** @var StringableStorage<Node,int> $processedIncomingEdges */
	$processedIncomingEdges = new StringableStorage();

	foreach($graph->getNodes() as $node){
		$nodesToProcess->attach($node);
		$processedIncomingEdges[$node] = count($graph->getPreviousNodes($node->identifier));
		$dist[$node] = null;
	}
	$dist[$start] = 0;


	while($nodesToProcess->count() > 0) {
		foreach($nodesToProcess as $nodeToProcess) {
			if($processedIncomingEdges[$nodeToProcess] === 0) {
				$node = $nodeToProcess;
				break;
			}
		}
		unset($nodesToProcess[$node]);


		foreach($graph->getNextNodes($node->identifier) as $neighborIdent) {
			$neighbor = $graph->getNode($neighborIdent);
			$edge = $graph->getEdge($node->identifier, $neighborIdent);
			$processedIncomingEdges[$neighbor] -= 1;
			$pathLength = $dist[$node] + $edge->getWeight();
			if($pathLength > $dist[$neighbor]) {
				$dist[$neighbor] = $pathLength;
				$prevEdges[$neighbor] = $edge;
			}
		}
	}

	/** @var SplStack<Edge> $path */
	$path = new SplStack();
	$currentNode = $end;
	while($currentNode->identifier !== $start->identifier){
		$path->push($prevEdges[$currentNode]);
		$prevNode = $path->top()->getConnected($currentNode);
		$currentNode = $prevNode;
	}

	return [$dist[$end], $path];
}