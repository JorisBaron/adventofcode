<?php

namespace AoC\Year2023\Day23;

use AoC\Common\Graph\Edge;
use AoC\Common\StringableStorage;
use AoC\Common\Graph\Node;
use SplDoublyLinkedList;

class Hike
{
	/** @var array<string,Edge> */
	protected array $path;
	/** @var array<string,Node> */
	protected array $nodes = [];

	protected Node $start;
	protected Node $end;

	protected int $length = 0;

	public function __construct(Node $start) {
		$this->path = [];
		$this->nodes = [];
		$this->nodes[$start->identifier] = $start;
		$this->start = $start;
		$this->end = $start;
	}

	public function __clone() : void {
		// $this->path = clone $this->path;
		// $this->nodes = clone $this->nodes;
		$this->start = clone $this->start;
		$this->end = clone $this->end;
	}

	/**
	 * @return array
	 */
	public function getNodes() : array {
		return $this->nodes;
	}

	public function hasNode(Node $node) : bool {
		return isset($this->nodes[$node->identifier]);
	}

	public function getStart() : Node {
		return $this->start;
	}

	public function getEnd() : Node {
		return $this->end;
	}

	public function getLength() : int {
		return $this->length;
	}

	public function addEdge(Edge $edge) : void {
		$this->path[] = $edge;
		$this->end = $edge->getConnected($this->end);
		$this->nodes[$this->end->identifier] = true;
		$this->length += $edge->getWeight();
	}

	public function removeLastEdge() : void {
		$lastEdge = array_pop($this->path);
		$prevLastNode = $lastEdge->getConnected($this->end);

		unset($this->nodes[$this->end->identifier]);
		$this->end = $prevLastNode;
		$this->length -= $lastEdge->getWeight();
	}

	public function getPath() : array {
		return $this->path;
	}
}