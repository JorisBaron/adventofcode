<?php

namespace AoC\Year2023\Day23;

use AoC\Common\Point;

readonly class HikingPath
{
	public function __construct(
		public Point $start,
		public Point $end,
		public int $length
	) {}
}