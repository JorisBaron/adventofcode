<?php

use AoC\Common\Graph\Graph;
use AoC\Common\Graph\StoerWagnerAlgorithm;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$graph = new Graph();
foreach($file as $line) {
	[$module1Ident, $others] = explode(': ', $line);
	foreach(explode(' ', $others) as $otherIdent) {
		$graph->createEdge($module1Ident, $otherIdent, 1, false);
	}
}

$algo = new StoerWagnerAlgorithm($graph);
$minCut = $algo->run();
echo $minCut->weight.PHP_EOL;
echo count($minCut->nodeSet1)*count($minCut->nodeSet2).PHP_EOL;