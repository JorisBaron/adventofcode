# Part 1

First tried to brute force it with a 3-level nested loop,
looping through each edge and trying to remove the 3 selected edges and see if the graph now has two parts.
However, this is `O(|V|*|E|^3)`, and the edge set is quite big, so it is very slow (at least in PHP...).

I then looked online to get an algorithm. I first found thing about "graph cut", and "max-flow/min-cut" algorithms,
which gave me flashbacks from engineer degree...  
Anyway, the most known max-flow algorithms (Ford-Fulkerson and Edmonds-Karp) require a source and a sink vertex,
which we don't have.

I then stumbled upon a [StackExchange topic about the min-cut in an undirected graph](https://cs.stackexchange.com/q/3399)
with [this answer](https://cs.stackexchange.com/a/126465) about the 
[Stoer–Wagner algorithm](https://en.wikipedia.org/wiki/Stoer%E2%80%93Wagner_algorithm) which fits perfectly to our situation.
It is `O(|V|*|E|+|V|²*log|V|)` as of the wikipedia page, but mine is probably more like `O(|V|^3+|E|)`,
which is better than the brute force method.  
My implementation might be a little *funky*, but works well!