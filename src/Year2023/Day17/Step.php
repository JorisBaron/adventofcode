<?php

namespace AoC\Year2023\Day17;

use AoC\Common\Point;

readonly class Step
{
	public function __construct(
		public Point $point,
		public Point $direction,
		public int $length,
		public int $straightLength,
	) {}

	public function toKey() : string {
		return sprintf("%s|%s|%d", $this->point->toString(), $this->direction->toString(), $this->straightLength);
	}
}