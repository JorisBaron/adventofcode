<?php

namespace AoC\Year2023\Day17;

use AoC\Common\Grid;
use AoC\Common\Point;

/**
 * @extends Grid<int>
 */
class HeatLossGrid extends Grid
{
	public static function createFromInput(array $lineArray) : static {
		return new static(
			array_map(
				fn($line) => array_map(intval(...), str_split($line)),
				$lineArray
			)
		);
	}

	/**
	 * @param Point $point
	 * @return array<string, int>
	 */
	public function getAdjacents(Point $point) : array {
		$adjacents = [];
		foreach([
			new Point(1,0),
			new Point(-1,0),
			new Point(0,1),
			new Point(0,-1)
		] as $direction){
			$adjacent = $point->add($direction);
			if($this->isInBound($adjacent)){
				$adjacents[$adjacent->toString()] = $this->get($adjacent) ;
			}
		}
		return $adjacents;
	}

	public function dijkstra(Point $start, Point $end, int $maxStraight, int $minStraight = 0) {
		/** @var Step[] $marked */
		$marked = [];
		$prevs = [];

		/** @var \SplHeap<Step> $heap */
		$heap = new class extends \SplHeap {
			/**
			 * @param Step $value1
			 * @param Step $value2
			 * @return int
			 */
			protected function compare(mixed $value1, mixed $value2) : int {
				return $value2->length <=> $value1->length;
			}
		};
		foreach([new Point(1,0), new Point(0,1)] as $point){
			$heap->insert(new Step($start, $point, 0, 0));
		}

		while(true){
			$minStep = $heap->extract();

			if(isset($marked[$minStep->toKey()])) {
				continue;
			}

			$marked[$minStep->toKey()] = $minStep;

			if($minStep->point->equals($end)) {
				if($minStep->straightLength < $minStraight) {
					continue;
				} else {
					break;
				}
			}

			$adjacentsPoints = $this->getAdjacents($minStep->point);
			foreach($adjacentsPoints as $pointCoords=>$loss){
				[$x,$y] = array_map(intval(...), explode(',', $pointCoords));
				$p = new Point($x, $y);

				if($p->equals($minStep->point->subtract($minStep->direction))){
					continue;
				}

				$sameDirection = $minStep->point->add($minStep->direction)->equals($p);
				$newStep = new Step(
					$p,
					$p->subtract($minStep->point),
					$minStep->length + $loss,
					$sameDirection ? $minStep->straightLength+1 : 1
				);

				if(isset($marked[$newStep->toKey()])) {
					continue;
				}

				if($sameDirection && $newStep->straightLength > $maxStraight) {
					continue;
				}

				if(!$sameDirection && $minStep->straightLength < $minStraight) {
					continue;
				}

				$heap->insert($newStep);
				$prevs[$newStep->toKey()] = $minStep;
			}
		}

		$path = [$minStep];
		while(!$path[0]->point->equals($start)){
			array_unshift($path, $prevs[$path[0]->toKey()]);
		}

		$marked = null;
		return [$minStep->length, $path];
	}
}