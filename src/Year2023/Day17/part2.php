<?php

ini_set('memory_limit', '2G');

use AoC\Year2023\Day17\HeatLossGrid;
use AoC\Common\Point;
use AoC\Common\Grid;
use AoC\Year2023\Day17\Step;

require_once __DIR__.'/../../../vendor/autoload.php';

//$file = new SplFileObject(__DIR__."/input/test3.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$g = HeatLossGrid::createFromInput(iterator_to_array($file));
$start = new Point(0,0);
$end = new Point($g->getWidth()-1, $g->getHeight()-1);

$dij = $g->dijkstra($start, $end, 10, 4);

$loss = $dij[0];
///** @var Step[] $path */
//$path = $dij[1];
//
///** @var Grid<string> $gPath */
//$gPath = new Grid(
//	array_fill(0, $g->getHeight(), array_fill(0, $g->getWidth(), '.'))
//);
//
//foreach($path as $step) {
//	$gPath->set($step->point, '#');
//}
//$gPath->display();

echo $loss;