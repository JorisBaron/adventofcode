<?php

use AoC\Common\Point3D;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$linesEquations = [];
foreach($file as $line) {
	[$pointStr, $slopeStr] = explode(" @ ", $line);
	$point = new Point3D(...explode(', ', $pointStr));
	$slopePoint = new Point3D(...explode(', ', $slopeStr));

	$point2d = $point->get2d();
	$slopePoint2d = $slopePoint->get2d();

	$a = $slopePoint2d->y / $slopePoint2d->x;
	$b = $point2d->y - $a*$point2d->x;

	$linesEquations[] = [
		'a' => $a, 'b' => $b,
		'point' => $point2d,
		'slopePoint' => $slopePoint2d
	];
}

$minCoord = 200000000000000;
$maxCoord = 400000000000000;

$sum = 0;
for($i=0; $i<count($linesEquations)-1; $i++){
	['a'=>$a1, 'b'=>$b1, 'point'=>$point1, 'slopePoint' => $slope1] = $linesEquations[$i];
	for($j=$i+1; $j<count($linesEquations); $j++) {
		['a'=>$a2, 'b'=>$b2, 'point'=>$point2, 'slopePoint' => $slope2] = $linesEquations[$j];

		if($a1 === $a2) {
			echo "None".PHP_EOL;
			continue;
		}

		$x = ($b2-$b1)/($a1-$a2);
		$y = $a1*$x+$b1;

		if($x < $minCoord || $x > $maxCoord || $y < $minCoord || $y > $maxCoord) {
			echo "outside".PHP_EOL;
			continue;
		}

		if(
			(($x - $point1->x <=> 0) !== ($slope1->x<=>0)) ||
			(($x - $point2->x <=> 0) !== ($slope2->x<=>0))
		) {
			echo "Past".PHP_EOL;
			continue;
		}

		echo sprintf("%f,%f", $x, $y).PHP_EOL;
		$sum++;
	}
}

echo PHP_EOL.$sum.PHP_EOL;