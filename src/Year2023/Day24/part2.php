<?php

use AoC\Common\Point3D;
use AoC\Common\Vector;
use AoC\Common\ParametricEquation;
use MathPHP\LinearAlgebra\MatrixFactory;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

/** @var ParametricEquation[] $linesEquations */
$linesEquations = [];
foreach($file as $line) {
	[$pointStr, $dirStr] = explode(" @ ", $line);
	$point = new Vector(...explode(', ', $pointStr));
	$vector = new Vector(...explode(', ', $dirStr));
	$line = new ParametricEquation($point, $vector);
	$linesEquations[] = $line;
}


/*
 * Thanks u/Solidifor
 * @see https://www.reddit.com/r/adventofcode/comments/18pnycy/comment/kfkge4t/
 * @see https://github.com/dirk527/aoc2021/blob/main/src/aoc2023/Day24.jpg
 * @see https://github.com/dirk527/aoc2021/blob/main/src/aoc2023/Day24.java
 */


$matrix = array_fill(0, 4, array_fill(0, 4, 0));
$equalVector = [];

$line1 = $linesEquations[0];
for($i = 0; $i < 4; $i++) {
	$line2 = $linesEquations[$i + 1];
	$matrix[$i][0] = $line2->v[1] - $line1->v[1]; // rsx (+xpr)
	$matrix[$i][1] = $line1->v[0] - $line2->v[0]; // rsy (ypr)
	$matrix[$i][2] = $line1->p[1] - $line2->p[1]; // rvx (xvr)
	$matrix[$i][3] = $line2->p[0] - $line1->p[0]; // rvy (yvr)

	// sy1*vx1 - sx1*vy1 - sy2*vx2 + sx2*vy2
	$equalVector[$i] = $line1->p[1] * $line1->v[0]
	                   - $line1->p[0] * $line1->v[1]
	                   - $line2->p[1] * $line2->v[0]
	                   + $line2->p[0] * $line2->v[1];
}

$M = MatrixFactory::create($matrix);
$V = new \MathPHP\LinearAlgebra\Vector($equalVector);

$res = $M->solve($V);

[$xpr, $ypr, $xvr, $yvr] = $res;

$t1 = ($line1->p[0] - $xpr)/($xvr - $line1->v[0]);
$t2 = ($line2->p[0] - $xpr)/($xvr - $line2->v[0]);

$z1 = $line1->p[2] + $line1->v[2]*$t1;
$z2 = $line2->p[2] + $line2->v[2]*$t2;

$zvr = ($z1-$z2)/($t1-$t2);
$zpr = $z1-$zvr*$t1;

var_dump(sprintf("%f", $xpr + $ypr + $zpr));

