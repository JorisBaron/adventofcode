# Part 1

Oh boy this smells math. I do not hate math, but if I can stand away from it I feel better.

*Anyway...*

In fact part 1 is not too complicated. I didn't know the existence of "parametric line equation" before that,
so I just transformed each line equation into it's `y = ax + b` form
* `a` is deduce from the velocity vector (let's call it `v`): `a = vv.y / v.x`
* `b` is then calculated by plugging the starting point (`p`) in the equation : `b = p.y - a*p.x`

Fine, then lets compute the intersection point of each line.  
I found this [wikipedia page on line-line intersection](https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection)
and took the formulas from the "[Given two line equations](https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection#Given_two_line_equations)"
paragraph.  
We first need to check if the slope of line 1 is different from slope of line 2, otherwise they are parallel,
and our formula would divide by 0.  
When the intersection point is calculated, we then need to check:
1. if it is in the boundaries
2. if one of the hail stone (or both) crossed the intersection in the past

`1.` is trivial to check. For `2.`, we need to check if the current direction of the hailstone is the same as 
the direction from itself to the intersection point. In fact, we even just need to check for one of the two coordinates.
In practice, we "just" have to check if the sign of `i.x - p.x` (`i` being the intersection point) is the same as `v.x`.
I took advantage of the spaceship operator (`<=>`) of PHP to do that.

Then just count the number of hailstone passing those conditions, and voilà!

# Part 2

Ok now I hate math.

And now I know what a parametric line equation is.

Anyway, I couldn't figure out how to do. The only thing I managed to do is spit out to non-linear equation
that I have no idea how to solve. Even online solver didn't come to rescue...

Went to Reddit to take some inspiration, and then saw [**u/Solidifor comment**](https://www.reddit.com/r/adventofcode/comments/18pnycy/comment/kfkge4t/)
who just explained how to make linear equation out of this (with [this image](https://github.com/dirk527/aoc2021/blob/main/src/aoc2023/Day24.jpg)).
Big thanks for that! It took me some time to understand it, but then I just took that and put it in a
[solving library](https://packagist.org/packages/markrogoyski/math-php) (nope, I'm not coding that): 
4 equations to find the `p.x`, `p.y`, `v.x` and `v.y` of the rock (`r`).
Then solved some more equations to find `p.z` and `v.z`, and there we are!

**Please no more linear algebra... please...**