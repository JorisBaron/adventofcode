<?php

use AoC\Year2023\Day03\EnginePuzzle;
use AoC\Common\Point;

require_once __DIR__.'/../../../vendor/autoload.php';
require_once __DIR__.'/functions.php';


$p = EnginePuzzle::createFromFile("input/input.txt");

$partNumbersSum = 0;

for($y = 0; $y < $p->getHeight(); $y++) {
	for($x = 0; $x < $p->getWidth(); $x++) {
		$point = new Point($x, $y);
		if($p->is($point, $p::DIGIT_REGEX)) {
			$number = $p->getNumberString($point);
			$numberLength = strlen($number);

			$borderingOffsets = getBorderingCellOffsets($numberLength);
			foreach($borderingOffsets as $offset) {
				$offsetPoint = $point->add($offset);
				if($p->is($offsetPoint, $p::SYMBOL_REGEX)) {
					$partNumbersSum += (int)$number;
					break;
				}
			}

			$x += $numberLength; // skip the cells of the number
		}
	}
}

echo $partNumbersSum;