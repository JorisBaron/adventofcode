<?php

use AoC\Common\Point;

/**
 * @param int $length
 * @return Point[]
 */
function getBorderingCellOffsets(int $length) : array {
	$cells = [
		new Point(-1, -1), new Point($length, -1),
		new Point(-1, 0), new Point($length, 0),
		new Point(-1, 1), new Point($length, 1),
	];
	for ($i=0; $i<$length; $i++){
		$cells[] = new Point($i, -1);
		$cells[] = new Point($i, 1);
	}
	return $cells;
}