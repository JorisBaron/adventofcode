<?php

namespace AoC\Year2023\Day03;

use SplFileObject;
use AoC\Common\Grid;
use AoC\Common\Point;
use OutOfBoundsException;

class EnginePuzzle extends Grid
{
	public const string SYMBOL_REGEX = "/[^.\d]/";
	public const string DIGIT_REGEX  = "/\d/";
	public const string GEAR_REGEX   = "/\*/";


	public static function createFromFile(string $filePath) : static {
		$file = new SplFileObject($filePath);
		$file->setFlags(SplFileObject::DROP_NEW_LINE);
		return parent::createFromInput(iterator_to_array($file));
	}


	public function is(Point $point, string $regex) : bool {
		if(!$this->isInBound($point)){
			return false;
		}
		return preg_match($regex, $this->get($point));
	}


	public function getNumberString(Point $point) : string {
		if(!$this->isInBound($point)){
			return '';
		}
		$char = $this->get($point);

		if(!preg_match(static::DIGIT_REGEX, $char)) {
			return '';
		}

		return $char.$this->getNumberString($point->add(new Point(1, 0)));

	}
}