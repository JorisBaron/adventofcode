<?php

require_once __DIR__."/functions.php";

$file = new SplFileObject("input/input.txt");

/*
 * R : distance record
 * T : total time available
 * h : hold time
 * d : travel distance
 *
 * We want to find h in :
 * R < d = (T-h)*h
 * -h² + Th > R
 * -h² + Th - R > 0
 *  h² - Th + R < 0
 *
 * In fact, the 2 roots are the 2 bounding values between which the record will be beaten
 * so we juste need to count how many values are in between those
 * which is : floor(high root) - ceil(low root) + 1
 */


//parse input
$time = parseLine($file->fgets());
$record = parseLine($file->fgets());


$roots = findRoots(-1, $time, -$record);
sort($roots);
$lowRoot = ceil($roots[0]);
$highRoot = floor($roots[1]);

echo $highRoot - $lowRoot + 1;



/**
 * @param string $line
 * @return int
 */
function parseLine(string $line) : int {
	$line = preg_replace("/[A-Za-z:\s]/", "", $line);
	return (int)trim($line);
}