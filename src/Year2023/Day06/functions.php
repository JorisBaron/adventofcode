<?php

function findRoots(int $a, int $b, int $c) {
	$sDelta = sqrt($b**2 - 4*$a*$c);
	return [
		(-$b - $sDelta) / (2*$a),
		(-$b + $sDelta) / (2*$a),
	];
}