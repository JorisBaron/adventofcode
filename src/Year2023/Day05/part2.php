<?php

require_once __DIR__.'/../../../vendor/autoload.php';

use AoC\Year2023\Day05\RangeMap;
use AoC\Year2023\Day05\Range;
use AoC\Year2023\Day05\Rule;


$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::READ_AHEAD | SplFileObject::SKIP_EMPTY | SplFileObject::DROP_NEW_LINE);

/** @var RangeMap[] $maps */
$maps = [];
$mapsIndex = -1;

/** @var Range[] $seedRanges */
$seedRanges = [];
$inputArray = explode(" ", str_replace("seeds: ", "", $file->current()));

for($i=0; $i<count($inputArray); $i+=2) {
	$start  = (int)$inputArray[$i];
	$length = (int)$inputArray[$i+1];
	$seedRanges[] = new Range($start,$start + $length - 1);
}
$file->next();


//parse the input
while ($file->valid()) {
	$line = $file->current();

	if(str_contains($line, "map")){
		$maps[] = new RangeMap();
		$mapsIndex++;

		$file->next();
		continue;
	}

	[$destStart, $srcStart, $length] = explode(" ", $line);
	$maps[$mapsIndex]->addRule(
		Rule::createFromSourceLengthDest((int)$srcStart, (int)$length, (int)$destStart)
	);

	$file->next();
}

foreach ($maps as $map){
	$map->finalize();
}

// calculate seeds
$minLoc = PHP_INT_MAX;
foreach ($seedRanges as $seedRange) {
	$ranges = [$seedRange];
	foreach ($maps as $map) {
		$nextRanges = [];
		foreach ($ranges as $range) {
			$nextRanges = array_merge($nextRanges, $map->getDestRanges($range));
		}
		$ranges = $nextRanges;
	}


	foreach ($ranges as $range) {
		if($range->start < $minLoc) {
			$minLoc = $range->start;
		}
	}
}

echo $minLoc;
