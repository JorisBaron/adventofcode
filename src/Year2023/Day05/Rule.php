<?php

namespace AoC\Year2023\Day05;

readonly class Rule
{
	public function __construct(
		public Range $range,
		public int $offset
	) {}

	public function getMin() : int {
		return $this->range->start;
	}

	public function getMax() : int {
		return $this->range->end;
	}

	public function map(int $value) : int {
		return $value + $this->offset;
	}

	public function getDestRange() : Range {
		return new Range(
			$this->range->start + $this->offset,
			$this->range->end + $this->offset
		);
	}

	/**
	 * @throws InvalidRangeException
	 */
	public function restrictRange(Range $range) : static {
		return new static(
			Range::intersection($range, $this->range),
			$this->offset
		);
	}

	/**
	 * @throws InvalidRangeException
	 */
	public static function createFromSourceLengthDest(int $source, int $length, int $dest) : static {
		return new static(new Range($source, $source + $length - 1), $dest - $source);
	}

	public static function compare(self $rule1, self $rule2) : int {
		return $rule1->getMin() <=> $rule2->getMin();
	}
}