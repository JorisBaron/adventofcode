<?php

require_once __DIR__.'/../../../vendor/autoload.php';

use AoC\Year2023\Day05\RangeMap;
use AoC\Year2023\Day05\Rule;
use AoC\Year2023\Day05\Range;


$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::READ_AHEAD | SplFileObject::SKIP_EMPTY | SplFileObject::DROP_NEW_LINE);

/** @var RangeMap[] $maps */
$maps = [];
$mapsIndex = -1;

$seeds = array_map(
	fn($seedStr) => (int)$seedStr,
	explode(
		" ",
		str_replace(
			"seeds: ",
			"",
			$file->current()
		)
	)
);
$file->next();


//parse the input
while ($file->valid()) {
	$line = $file->current();

	if(str_contains($line, "map")){
		$maps[] = new RangeMap();
		$mapsIndex++;

		$file->next();
		continue;
	}

	[$destStart, $srcStart, $length] = explode(" ", $line);
	$maps[$mapsIndex]->addRule(new Rule(new Range((int)$srcStart, (int)$destStart), (int)$length));

	$file->next();
}

// calculate seeds
$minLocation = PHP_INT_MAX;
foreach ($seeds as $seed) {
	foreach ($maps as $map){
		$seed = $map->map($seed);
	}

	if($seed<$minLocation) {
		$minLocation = $seed;
	}
}

echo $minLocation;