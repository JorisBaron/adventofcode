<?php

namespace AoC\Year2023\Day05;

readonly class Range
{
	/**
	 * @throws InvalidRangeException
	 */
	public function __construct(
		public int $start,
		public int $end
	) {
		if($this->start > $this->end) {
			throw new InvalidRangeException();
		}
	}

	public function contains(int $value) : bool {
		return $this->start <= $value && $value <= $this->end;
	}

	public function intersect(self $range) : bool {
		return $this->start <= $range->end && $range->start <= $this->end;
	}

	/**
	 * @throws InvalidRangeException
	 */
	public static function intersection(Range $r1, Range $r2) : Range {
		return new Range(
			max($r1->start, $r2->start),
			min($r1->end, $r2->end),
		);
	}
}