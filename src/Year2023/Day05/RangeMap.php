<?php

namespace AoC\Year2023\Day05;

class RangeMap
{
	/** @var Rule[] */
	protected array $rules     = [];
	protected bool  $finalized = false;


	public function addRule(Rule $rule) : void {
		$this->rules[] = $rule;
	}

	public function finalize() : void {
		usort($this->rules, Rule::compare(...));
		$fillers = [];
		$ruleNumbers = count($this->rules);

		if($this->rules[0]->getMin() > 0) {
			$fillers[] = new Rule(new Range(0, $this->rules[0]->getMin() - 1), 0);
		}

		for($i = 1; $i < $ruleNumbers; $i++) {
			$prevMax = $this->rules[$i - 1]->getMax() + 1;
			$curMin = $this->rules[$i]->getMin();
			if($prevMax !== $curMin) {
				$fillers[] = new Rule(new Range($prevMax, $curMin - 1), 0);
			}
		}
		$fillers[] = new Rule(new Range($this->rules[$ruleNumbers - 1]->getMax() + 1, PHP_INT_MAX), 0);

		$this->rules = array_merge($this->rules, $fillers);
		usort($this->rules, Rule::compare(...));

		$this->finalized = true;
	}


	/**
	 * @throws NoRuleFoundException
	 */
	public function map(int $value) : int {
		if(!$this->finalized) {
			$this->finalize();
		}

		foreach($this->rules as $rule) {
			if($rule->range->contains($value)) {
				return $rule->map($value);
			}
		}

		throw new NoRuleFoundException();
	}

	public function getDestRanges(Range $range) {
		if(!$this->finalized) {
			$this->finalize();
		}

		$resultingRules = [];

		foreach($this->rules as $rule) {
			if($rule->range->intersect($range)) {
				$resultingRules[] = $rule->restrictRange($range);
			}
		}

		return array_map(fn(Rule $rule) => $rule->getDestRange(), $resultingRules);
	}
}

