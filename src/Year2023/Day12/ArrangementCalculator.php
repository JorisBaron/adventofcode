<?php

namespace AoC\Year2023\Day12;

use Exception;

class ArrangementCalculator
{
	public function __construct(
		protected string $stateString,
		protected array $pattern
	) {}

	public function calculate() : int {
		$states = [
			new State($this->stateString)
		];

		for ($i = 0; $i < strlen($this->stateString); $i++) {
			$next = [];
			foreach ($states as $state) {
				$next = array_merge($next, $this->playLine($state));
			}

			/** @var State[] $regroupedNext */
			$regroupedNext = [];
			foreach ($next as $nextState){
				foreach ($regroupedNext as $added) {
					if (State::canRegroup($added, $nextState)){
						$added->addRegroup($nextState);
						continue 2;
					}
				}
				$regroupedNext[] = $nextState;
			}

			$states = $regroupedNext;
		}

		$states = array_filter($states, fn($line) => $line->getGroups() === $this->pattern);


		return array_sum(array_map(fn(State $state) => $state->getWeight(), $states));
	}

	public function playLine(State $state) {
		switch ($state->getCurrentChar()) {
			case '.' :
				if($state->isParsingGroup()) {
					$state->stopParsingGroup();
					if($state->getCurrentGroup() === $this->pattern[$state->getCurrentGroupIndex()]) {
						$state->incrementI();
						return [$state];
					} else {
						return [];
					}
				} else {
					$state->incrementI();
					return [$state];
				}
			case '#':
				if(!$state->isParsingGroup()) {
					$state->newGroup();
					if ($state->getGroupCount() > count($this->pattern)){
						return [];
					}
				}
				$state->incrementGroupSize();

				if($state->getCurrentGroup() > $this->pattern[$state->getCurrentGroupIndex()]){
					return [];
				} else {
					$state->incrementI();
					return [$state];
				}
			case '?':
				$cloneState = clone $state;
				return array_merge(
					$this->playLine($state->setCurrentChar('.')),
					$this->playLine($cloneState->setCurrentChar('#'))
				);
			default:
				throw new Exception();
		}
	}
}