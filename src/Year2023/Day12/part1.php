<?php

require_once __DIR__.'/../../../vendor/autoload.php';

use AoC\Year2023\Day12\ArrangementCalculator;

$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$possSum = 0;
foreach ($file as $line) {
	[$state, $pattern] = explode(' ', $line);
	$state = preg_replace(['/^\.+|\.+$/', '/\.+/'], ['', '.'], $state);
	$pattern = array_map(intval(...), explode(",", $pattern));
	$arrangementCalculator = new ArrangementCalculator($state, $pattern);
	$possSum += $arrangementCalculator->calculate();
}
echo $possSum;
