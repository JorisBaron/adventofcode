<?php

namespace AoC\Year2023\Day12;
class State
{
	protected string $state;
	protected int    $currentI = 0;
	/** @var int[] */
	protected array $groups            = [];
	protected int   $currentGroupIndex = -1;
	protected bool  $parsingGroup      = false;

	protected int $weight = 1;

	public function __construct($state) {
		$this->state = $state;
	}

	public function getState() : string {
		return $this->state;
	}

	public function getCurrentChar() : string {
		return $this->state[$this->currentI];
	}

	public function setCurrentChar(string $value) : static {
		if($this->state[$this->currentI] === "?" && ($value === '.' || $value === "#")) {
			$this->state[$this->currentI] = $value;
		}
		return $this;
	}

	public function incrementI() : void {
		$this->currentI++;
	}

	public function isParsingGroup() : bool {
		return $this->parsingGroup;
	}

	public function stopParsingGroup() : void {
		$this->parsingGroup = false;
	}

	public function getGroups() : array {
		return $this->groups;
	}

	public function getCurrentGroup() : int {
		return $this->groups[$this->currentGroupIndex];
	}

	public function incrementGroupSize() : void {
		$this->groups[$this->currentGroupIndex]++;
	}

	public function getCurrentGroupIndex() : int {
		return $this->currentGroupIndex;
	}


	public function newGroup() : void {
		$this->currentGroupIndex++;
		$this->groups[] = 0;
		$this->parsingGroup = true;
	}

	public function getGroupCount() : int {
		return count($this->groups);
	}


	public function getWeight() : int {
		return $this->weight;
	}

	public function addRegroup(State $state) : void {
		$this->weight += $state->weight;
	}

	public static function canRegroup(State $s1, State $s2) : bool {
		return $s1->currentI === $s2->currentI &&
		       !$s1->isParsingGroup() && !$s2->isParsingGroup() &&
		       $s1->groups === $s2->groups;
	}
}