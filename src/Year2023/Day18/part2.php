<?php

use AoC\Year2023\Day18\LavaPool;

require_once __DIR__.'/../../../vendor/autoload.php';

//$file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$lp = LavaPool::fromInputPart2($file);
echo $lp->calculateArea().PHP_EOL;