<?php

namespace AoC\Year2023\Day18;

use SplObjectStorage;
use AoC\Common\Point;
use AoC\Common\Grid;

class LavaPool
{
	/** @var PoolEdge[] $edges */
	protected array $edges = [];

	protected Point $dimension;

	protected function __construct() {}

	public static function fromInput(iterable $linesArray) : static {
		$minX = 0;
		$minY = 0;
		$maxX = 0;
		$maxY = 0;
		$nextEdgeStart = new Point(0,0);

		$lavaPool = new static();

		/** @var PoolEdge[] $rawEdges */
		$rawEdges = [];
		foreach($linesArray as $line) {
			$edge = PoolEdge::fromInput($line, $nextEdgeStart);
			$rawEdges[] = $edge;

			$nextEdgeStart = $edge->getNextEdgeStartPoint();
			$minX = min($minX, $nextEdgeStart->x);
			$minY = min($minY, $nextEdgeStart->y);
			$maxX = max($maxX, $nextEdgeStart->x);
			$maxY = max($maxY, $nextEdgeStart->y);
		}

		$lavaPool->setDimension(new Point($maxX - $minX +1, $maxY - $minY +1));

		$minPoint = new Point($minX, $minY);
		$offset = $minPoint->multiply(-1);

		$lavaPool->edges = array_map(fn ($edge) => $edge->offset($offset), $rawEdges);

		return $lavaPool;
	}

	public static function fromInputPart2(iterable $linesArray) : static {
		$minX = 0;
		$minY = 0;
		$maxX = 0;
		$maxY = 0;
		$nextEdgeStart = new Point(0,0);

		$lavaPool = new static();

		/** @var PoolEdge[] $rawEdges */
		$rawEdges = [];
		foreach($linesArray as $line) {
			$edge = PoolEdge::fromInputPart2($line, $nextEdgeStart);
			$rawEdges[] = $edge;

			$nextEdgeStart = $edge->getNextEdgeStartPoint();
			$minX = min($minX, $nextEdgeStart->x);
			$minY = min($minY, $nextEdgeStart->y);
			$maxX = max($maxX, $nextEdgeStart->x);
			$maxY = max($maxY, $nextEdgeStart->y);
		}

		$lavaPool->setDimension(new Point($maxX - $minX +1, $maxY - $minY +1));

		$minPoint = new Point($minX, $minY);
		$offset = $minPoint->multiply(-1);

		$lavaPool->edges = array_map(fn ($edge) => $edge->offset($offset), $rawEdges);

		return $lavaPool;
	}


	/**
	 * @param Point $dimension
	 */
	public function setDimension(Point $dimension) : void {
		$this->dimension = $dimension;
	}

	public function getWidth() : int {
		return $this->dimension->x;
	}

	public function getHeight() : int {
		return $this->dimension->y;
	}


	public function calculateArea() : int {
		$sum = 0;
		$sumSuppl = 0;
		$horizontalPoint = new Point(1,0);
		$verticalPoint = new Point(0,1);
		$nbEdges = count($this->edges);

		foreach($this->edges as $i=>$edge){
			$p1 = $edge->start;
			$p2 = $edge->getNextEdgeStartPoint();
			$sum += $p1->x*$p2->y - $p1->y*$p2->x;
			if($edge->direction->equals($horizontalPoint) || $edge->direction->equals($verticalPoint)) {
				$sumSuppl += $edge->length;
			}
		}
		return $sum/2 + $sumSuppl + 1;
	}

	public function toGrid() : Grid {
		$gridArray = [];
//		for($y = 0; $y<$this->getHeight(); $y++) {
//			$row = [];
//			for($x = 0; $x < $this->getWidth(); $x++) {
//				$row[] = $this->pointToEdges->contains(new Point($x, $y)) ? '#' : '.';
//			}
//			$gridArray[] = $row;
//		}
//		$startPoint = $this->edges[0]->start;
//		$gridArray[$startPoint->y][$startPoint->x] = '@';
		return new Grid($gridArray);
	}
}