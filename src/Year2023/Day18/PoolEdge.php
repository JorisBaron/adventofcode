<?php

namespace AoC\Year2023\Day18;

use AoC\Common\Point;

readonly class PoolEdge
{
	public function __construct(
		public Point $start,
		public Point $direction,
		public int $length
	) {}

	public static function fromInput(string $input, Point $start) : static {
		preg_match('/^([UDLR]) (\d+) \((#[0-9a-f]{6})\)$/', $input, $matches);
		$dirPoint = match ($matches[1]) {
			'U' => new Point( 0,-1),
			'D' => new Point( 0, 1),
			'L' => new Point(-1, 0),
			'R' => new Point( 1, 0),
		};

		return new static($start, $dirPoint, (int)$matches[2]);
	}

	public static function fromInputPart2(string $input, Point $start) : static {
		preg_match('/#([0-9a-f]{5})([0-9a-f])/', $input, $matches);
		$dirPoint = match ($matches[2]) {
			'0' => new Point( 1, 0),
			'1' => new Point( 0, 1),
			'2' => new Point(-1, 0),
			'3' => new Point( 0,-1),
		};

		return new static($start, $dirPoint, hexdec($matches[1]));
	}

	public function offset(Point $offset) : static {
		return new static($this->start->add($offset), $this->direction, $this->length);
	}


	public function getLastPoint() : Point {
		return $this->start->add($this->direction->multiply($this->length-1));
	}

	public function getNextEdgeStartPoint() : Point {
		return $this->start->add($this->direction->multiply($this->length));
	}

	public function getAllPoints() : array {
		$points = [];
		for($i = 0; $i < $this->length; $i++) {
			$points[] = $this->start->add($this->direction->multiply($i));
		}
		return $points;
	}
}