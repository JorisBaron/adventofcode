<?php

require_once __DIR__.'/../../../vendor/autoload.php';

use AoC\Year2023\Day14\RockGrid;

$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$g = RockGrid::createFromInput(iterator_to_array($file));
$g->tiltNorth();
//$g->display();
echo $g->calculateLoadNorth();