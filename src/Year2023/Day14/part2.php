<?php

require_once __DIR__.'/../../../vendor/autoload.php';

use AoC\Year2023\Day14\RockGrid;

$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$cycles = 1000000000;

$cache = [];
$loads = [];
$g = RockGrid::createFromInput(iterator_to_array($file));
for ($i=0; $i<$cycles; $i++) {
	for ($j=0; $j<4; $j++){
		$g->tiltNorth();
		$g->rotateClockwise();
	}

	$loads[] = $g->calculateLoadNorth();
	$rocks = json_encode($g->getRoundRocksCoordinates());
	if(isset($cache[$rocks])) {
		$cache[$rocks][] = $i;
		break;
	} else {
		$cache[$rocks] = [$i];
	}

	if($i% 100 === 0){
		echo $i.PHP_EOL;
	}
}



$cyclesCycleLength = $cache[$rocks][1] - $cache[$rocks][0];
$cyclesCycleStart = $cache[$rocks][0];
$key = ($cycles-1 - $cyclesCycleStart) % $cyclesCycleLength;

echo $loads[$key + $cyclesCycleStart];

//echo $g->calculateLoadNorth().PHP_EOL;
//$g->display();