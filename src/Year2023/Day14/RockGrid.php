<?php

namespace AoC\Year2023\Day14;

use AoC\Common\Grid;

class RockGrid extends Grid
{
	public function tiltNorth() : void {
		for($x = 0; $x < $this->width; $x++) {
			$roundRocks = 0;
			for($y = $this->height - 1; $y >= 0; $y--) {
				switch($this->grid[$y][$x]) {
					case 'O' :
						$roundRocks++;
						$this->grid[$y][$x] = ".";
						break;
					case '#' :
						for($i = 1; $i <= $roundRocks; $i++) {
							$this->grid[$y + $i][$x] = "O";
						}
						$roundRocks = 0;
						break;
				}
			}
			for($i = 0; $i < $roundRocks; $i++) {
				$this->grid[$i][$x] = "O";
			}

		}
	}

	public function calculateLoadNorth() : int {
		$sum = 0;
		foreach($this->grid as $i => $row) {
			$rowValue = $this->height - $i;
			foreach($row as $cell) {
				if($cell === 'O') {
					$sum += $rowValue;
				}
			}
		}
		return $sum;
	}

	public function getRoundRocksCoordinates() : array {
		$coords = [];
		foreach($this->grid as $y => $row) {
			foreach($row as $x => $cell) {
				if($cell === "O") {
					$coords[] = [$x, $y];
				}
			}

		}
		return $coords;
	}
}