<?php

require_once __DIR__.'/../../../vendor/autoload.php';

use AoC\Year2023\Day13\MirrorGrid;

$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$sum = 0;
$lineArray = [];
foreach ($file as $line) {
	if(empty($line)){
		$g = MirrorGrid::createFromInput($lineArray);
		$sum += $g->almostSummarize();

		$lineArray=[];
		continue;
	}

	$lineArray[] = $line;
}

echo $sum;