<?php

namespace AoC\Year2023\Day13;

use AoC\Common\Grid;
use OutOfBoundsException;
use AoC\Common\ArrayFunc;

class MirrorGrid extends Grid
{
	public function searchVerticalReflectionLine() : ?int {
		for($i = 0; $i < $this->width - 1; $i++) {
			if($this->getColumn($i) === $this->getColumn($i + 1)) {
				for($j = 1; true; $j++) {
					try {
						if($this->getColumn($i - $j) !== $this->getColumn($i + $j + 1)) {
							break;
						}
					} catch(OutOfBoundsException) {
						return $i;
					}
				}

			}
		}
		return null;
	}

	public function searchHorizontalReflectionLine() : ?int {
		for($i = 0; $i < $this->height - 1; $i++) {
			if($this->getRow($i) === $this->getRow($i + 1)) {
				for($j = 1; true; $j++) {
					try {
						if($this->getRow($i - $j) !== $this->getRow($i + $j + 1)) {
							break;
						}
					} catch(OutOfBoundsException) {
						return $i;
					}
				}

			}
		}
		return null;
	}

	public function summarize() : int {
		$reflectionLine = $this->searchHorizontalReflectionLine();
		if($reflectionLine !== null) {
			return ($reflectionLine + 1) * 100;
		}

		$reflectionLine = $this->searchVerticalReflectionLine();
		if($reflectionLine !== null) {
			return $reflectionLine + 1;
		}

		return 0;
	}

	protected function searchAlmostReflection(callable $getAxisFunction, int $axisLimit) : ?int {
		for($i = 0; $i < $axisLimit - 1; $i++) {
			$distance = ArrayFunc::hammingDistance($getAxisFunction($i), $getAxisFunction($i + 1));
			if($distance <= 1) {
				$totalDist = $distance;
				for($j = 1; true; $j++) {
					try {
						$totalDist += ArrayFunc::hammingDistance($getAxisFunction($i - $j), $getAxisFunction($i + $j + 1));
						if($totalDist > 1) {
							break;
						}
					} catch(OutOfBoundsException) {
						if($totalDist !== 1) {
							break;
						}
						return $i;
					}
				}

			}
		}
		return null;
	}

	public function searchAlmostVerticalReflection() : ?int {
		return $this->searchAlmostReflection($this->getColumn(...), $this->width);
	}

	public function searchAlmostHorizontalReflection() : ?int {
		return $this->searchAlmostReflection($this->getRow(...), $this->height);
	}

	public function almostSummarize() : int {
		$reflectionLine = $this->searchAlmostHorizontalReflection();
		if($reflectionLine !== null) {
			return ($reflectionLine + 1) * 100;
		}

		$reflectionLine = $this->searchAlmostVerticalReflection();
		if($reflectionLine !== null) {
			return $reflectionLine + 1;
		}

		return 0;
	}

}