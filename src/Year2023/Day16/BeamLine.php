<?php

namespace AoC\Year2023\Day16;

use AoC\Year2023\Day16\Beam;

require_once __DIR__.'/Beam.php';

readonly class BeamLine
{
	protected array $cachedCoords;

	public function __construct(
		public int $startX,
		public int $startY,
		public int $dX,
		public int $dY,
		public int $length
	) {}


	public function getLastBeam() : Beam {
		$deltaLength = $this->length - 1;
		return new Beam(
			$this->startX + $this->dX * $deltaLength,
			$this->startY + $this->dY * $deltaLength,
			$this->dX,
			$this->dY
		);
	}

	public function getAllCoords() : array {
		if(isset($this->cachedCoords)) {
			return $this->cachedCoords;
		}

		$coords = [];
		for($i = 0; $i < $this->length; $i++) {
			$coords[] = [
				$this->startX + $this->dX * $i,
				$this->startY + $this->dY * $i,
			];
		}

		$this->cachedCoords = $coords;
		return $coords;
	}

	public function toCacheKey() : string {
		return sprintf("%d,%d,%d,%d", $this->startX, $this->startY, $this->dX, $this->dY);
	}
}