<?php

require_once __DIR__.'/../../../vendor/autoload.php';

use AoC\Year2023\Day16\Beam;
use AoC\Year2023\Day16\BeamGrid;

$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$fileArray = iterator_to_array($file);
$size = count($fileArray);
$maxEnergized = 0;
$grid = BeamGrid::createFromInput(iterator_to_array($file));

for($i=0; $i<count($fileArray); $i++){
	echo $i.PHP_EOL;

	$startingBeams = [
		new Beam(0, $i, 1, 0),
		new Beam($size-1, $i, -1, 0),
		new Beam($i, 0, 0, 1),
		new Beam($i, $size-1, 0, -1),
	];
	foreach($startingBeams as $startingBeam) {
		$energized = playGrid($grid, $startingBeam);
		$maxEnergized = max($maxEnergized, $energized);
	}
}

echo $maxEnergized;


function playGrid(BeamGrid $grid, Beam $startingBeam) {
	$beams = [$startingBeam];
	$energized = [implode(',',$startingBeam->getCoords()) => true];
	$beamLinesCache = [];

	while(count($beams)>0){
		$beam = array_shift($beams);
		$beamLines = $grid->applyBeam($beam);
		foreach($beamLines as $beamLine) {
			if(isset($beamLinesCache[$beamLine->toCacheKey()])) {
				continue;
			}
			$beamLinesCache[$beamLine->toCacheKey()] = true;

			$coordsList = $beamLine->getAllCoords();
			foreach($coordsList as $coords) {
				$energized[implode(',',$coords)] = true;
			}

			$beams[] = $beamLine->getLastBeam();
		}
	}

	return count($energized);
}