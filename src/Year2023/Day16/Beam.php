<?php

namespace AoC\Year2023\Day16;
use JsonSerializable;

readonly class Beam implements JsonSerializable
{
	public function __construct(
		public int $x,
		public int $y,
		public int $dx,
		public int $dy,
	) {}

	public function isHorizontal() : bool {
		return $this->dx !== 0 && $this->dy === 0;
	}

	public function isVertical() : bool {
		return $this->dx === 0 && $this->dy !== 0;
	}

	public function getDirectionString() : string {
		return match ([$this->dx, $this->dy]) {
			[1, 0]  => '>',
			[-1, 0] => '<',
			[0, 1]  => 'v',
			[0, -1] => '^'
		};
	}


	/**
	 * @return int[]
	 */
	public function getCoords() : array {
		return [
			$this->x,
			$this->y
		];
	}

	public function nextStraight() : static {
		return new static($this->x + $this->dx, $this->y + $this->dy, $this->dx, $this->dy);
	}

	public function nextTurnRight() : static {
		$nextDx = -$this->dy;
		$nextDy = $this->dx;
		return new static($this->x + $nextDx, $this->y + $nextDy, $nextDx, $nextDy);
	}

	public function nextTurnLeft() : static {
		$nextDx = $this->dy;
		$nextDy = -$this->dx;
		return new static($this->x + $nextDx, $this->y + $nextDy, $nextDx, $nextDy);
	}

	public function jsonSerialize() : string|false {
		return json_encode([
			'x'  => $this->x,
			'y'  => $this->y,
			'dx' => $this->dx,
			'dy' => $this->dy
		]);
	}
}