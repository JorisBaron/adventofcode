<?php

namespace AoC\Year2023\Day16;

use AoC\Common\Grid;
use AoC\Year2023\Day16\Beam;
use AoC\Year2023\Day16\BeamLine;

require_once __DIR__."/../../Common/Grid.php";
require_once __DIR__."/Beam.php";
require_once __DIR__."/BeamLine.php";

/**
 * @extends Grid<string>
 */
class BeamGrid extends Grid
{
	protected array $cache = [];


	public static function createFromInput(array $lineArray) : static {
		return new static(
			array_map(
				fn($line) => array_map(
					fn($cell) => $cell,
					str_split($line)
				),
				$lineArray
			)
		);
	}


	/**
	 * @param Beam $beam
	 * @return BeamLine[]
	 */
	public function applyBeam(Beam $beam) : array {
		if(isset($this->cache[$beam->jsonSerialize()])) {
			return $this->cache[$beam->jsonSerialize()];
		}

		$coords = $beam->getCoords();
		if(!$this->isInBound(...$coords)) {
			return [];
		}

		$nextBeams = match ($this->get(...$coords)) {
			'/'  => [$beam->isHorizontal() ? $beam->nextTurnLeft() : $beam->nextTurnRight()],
			'\\' => [$beam->isVertical() ? $beam->nextTurnLeft() : $beam->nextTurnRight()],
			'|'  => $beam->isHorizontal() ? [$beam->nextTurnRight(), $beam->nextTurnLeft()] : [$beam->nextStraight()],
			'-'  => $beam->isVertical() ? [$beam->nextTurnRight(), $beam->nextTurnLeft()] : [$beam->nextStraight()],
			'.'  => [$beam->nextStraight()],
		};


		$beamLines = [];
		foreach($nextBeams as $nextBeam) {
			$beamLine = $this->generateBeamLine($nextBeam);
			if($beamLine !== null) {
				$beamLines[] = $beamLine;
			}
		}
		$this->cache[$beam->jsonSerialize()] = $beamLines;

		return $beamLines;
	}

	protected function generateBeamLine(Beam $beam) : ?BeamLine {
		if(!$this->isInBound(...$beam->getCoords())) {
			return null;
		}

		$allowedSplitter = $beam->isVertical() ? '|' : '-';
		$startCoords = $beam->getCoords();
		$length = 1;
		while(in_array($this->get(...$beam->getCoords()), ['.', $allowedSplitter])) {
			$nextBeam = $beam->nextStraight();
			if(!$this->isInBound(...$nextBeam->getCoords())) {
				break;
			}
			$beam = $nextBeam;
			$length++;
		}
		return new BeamLine($startCoords[0], $startCoords[1], $beam->dx, $beam->dy, $length);
	}
}