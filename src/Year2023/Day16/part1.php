<?php


require_once __DIR__.'/../../../vendor/autoload.php';

use AoC\Year2023\Day16\Beam;
use AoC\Year2023\Day16\BeamGrid;

$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$grid = BeamGrid::createFromInput(iterator_to_array($file));
$beam = new Beam(0,0, 1,0);

$beams = [$beam];
$energized = [implode(',',$beam->getCoords()) => true];
$beamLinesCache = [];

while(count($beams)>0){
	$beam = array_shift($beams);
	$beamLines = $grid->applyBeam($beam);
	foreach($beamLines as $beamLine) {
		if(isset($beamLinesCache[$beamLine->toCacheKey()])) {
			continue;
		}
		$beamLinesCache[$beamLine->toCacheKey()] = true;

		$coordsList = $beamLine->getAllCoords();
		foreach($coordsList as $coords) {
			$energized[implode(',',$coords)] = true;
		}

		$beams[] = $beamLine->getLastBeam();
	}
}


echo count($energized).PHP_EOL;