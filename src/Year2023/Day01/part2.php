<?php

use AoC\Common\Pair;

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$digits = [
	"one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
	"1", "2", "3", "4", "5", "6", "7", "8", "9"
];

$sum = 0;
foreach ($file as $inputLine) {
	/** @var Pair<int, string> $bestFirst */
	$bestFirst = new Pair(PHP_INT_MAX, "");
	/** @var Pair<int, string> $bestLast */
	$bestLast = new Pair(-1, "");

	foreach ($digits as $digit) {
		$pos = strpos($inputLine, $digit);
		if ($pos !== false && $pos < $bestFirst->key) {
			$bestFirst = new Pair($pos, matchNumber($digit));
		}

		$rpos = strrpos($inputLine, $digit);
		if ($rpos !== false && $rpos > $bestLast->key) {
			$bestLast = new Pair($rpos, matchNumber($digit));
		}
	}

	$sum += (int)($bestFirst->value.$bestLast->value);
}

echo $sum;

function matchNumber(string $number) : string {
	return match ($number) {
		"one"   => "1",
		"two"   => "2",
		"three" => "3",
		"four"  => "4",
		"five"  => "5",
		"six"   => "6",
		"seven" => "7",
		"eight" => "8",
		"nine"  => "9",
		default => $number
	};
}