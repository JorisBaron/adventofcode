<?php

require_once __DIR__.'/../../../vendor/autoload.php';

// $file = new SplFileObject(__DIR__."/input/test.txt");
$file = new SplFileObject(__DIR__."/input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$sum = 0;
foreach ($file as $inputLine) {
	preg_match("/^\D*(\d)/", $inputLine, $firstDigitMatches);
	preg_match("/(\d)\D*$/", $inputLine, $lastDigitMatches);
	$sum += (int)($firstDigitMatches[1].$lastDigitMatches[1]);
}

echo $sum;