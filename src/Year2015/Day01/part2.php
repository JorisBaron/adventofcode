<?php

$file = new SplFileObject("input/input.txt");

$floor = 0;
$i = 1;
while (false !== ($char = $file->fgetc())) {
	$floor += $char ==="(" ? 1 : -1;
	if($floor < 0){
		break;
	}
	$i++;
}
echo $i;