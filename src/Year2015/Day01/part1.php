<?php

$file = new SplFileObject("input/input.txt");

$floor = 0;
while (false !== ($char = $file->fgetc())) {
	$floor += $char ==="(" ? 1 : -1;
}
echo $floor;