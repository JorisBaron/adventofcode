<?php

$file = new SplFileObject("input/input.txt");

$nices = 0;
foreach ($file as $line) {
	$line = trim($line);

	if (preg_match('/(.)(.).*\1\2/', $line)
	    && preg_match('/(.).\1/', $line)
	){
		$nices++;
	}
}

echo $nices;