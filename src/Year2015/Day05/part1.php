<?php

$file = new SplFileObject("input/input.txt");

$nices = 0;
foreach ($file as $line) {
	$line = trim($line);

	if (preg_match('/[aeiou].*[aeiou].*[aeiou]/', $line)
	    && preg_match('/(.)\1/', $line)
		&& !preg_match('/ab|cd|pq|xy/', $line)){
		$nices++;
	}
}

echo $nices;