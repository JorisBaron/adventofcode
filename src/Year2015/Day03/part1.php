<?php

$file = new SplFileObject("input/input.txt");

$visited = ["0,0" => true];
$currentPos = [0,0];
while (false !== ($char = $file->fgetc())) {
	$translation = match ($char) {
		"^" => [0,1],
		"v" => [0,-1],
		">" => [1,0],
		"<" => [-1,0]
	};
	$currentPos[0] += $translation[0];
	$currentPos[1] += $translation[1];

	$mapKey = implode(",", $currentPos);
	if(!isset($visited[$mapKey])){
		$visited[$mapKey] = true;
	}
}
echo count($visited);