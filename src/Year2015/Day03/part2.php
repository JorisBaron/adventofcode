<?php

$file = new SplFileObject("input/input.txt");

$visited = ["0,0" => true];
$currentPos = [
	[0,0],
	[0,0]
];
$which = 0;
while (false !== ($char = $file->fgetc())) {
	$translation = match ($char) {
		"^" => [0,1],
		"v" => [0,-1],
		">" => [1,0],
		"<" => [-1,0]
	};
	$currentPos[$which][0] += $translation[0];
	$currentPos[$which][1] += $translation[1];

	$mapKey = implode(",", $currentPos[$which]);
	if(!isset($visited[$mapKey])){
		$visited[$mapKey] = true;
	}

	$which = $which === 0 ? 1 : 0;
}
echo count($visited);