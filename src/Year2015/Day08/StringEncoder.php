<?php

namespace AoC\Year2015\Day08;

class StringEncoder
{
	public string $raw;
	public string $encoded;

	public function __construct(string $input) {
		$this->raw = $input;
		$encoded = '"';
		$i = 0;
		while($i < strlen($input)) {
			$char = $input[$i];
			switch($char) {
				case '"' :
				case '\\' :
					$encoded .= '\\';
				default :
					$encoded .= $char;
					break;
			}

			$i++;
		}
		$this->encoded = $encoded.'"';
	}
}