<?php

use AoC\Year2015\Day08\StringParser;

require_once __DIR__.'/../../../vendor/autoload.php';

$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$sum = 0;
foreach ($file as $i => $line) {
	$str = new StringParser($line);
	echo sprintf("%d : %s (%d) => %s (%d)".PHP_EOL, $i, $line, strlen($str->raw), $str->parsed,  strlen($str->parsed));
	$sum += strlen($str->raw) - strlen($str->parsed);
}

echo $sum;