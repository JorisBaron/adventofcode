<?php

use AoC\Year2015\Day08\StringEncoder;

require_once __DIR__.'/../../../vendor/autoload.php';

$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$sum = 0;
foreach ($file as $i => $line) {
	$str = new StringEncoder($line);
	echo sprintf("%d : %s (%d) => %s (%d)".PHP_EOL, $i, $line, strlen($str->raw), $str->encoded,  strlen($str->encoded));
	$sum += strlen($str->encoded) - strlen($str->raw);
}

echo $sum;