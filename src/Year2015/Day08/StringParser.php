<?php

namespace AoC\Year2015\Day08;

readonly class StringParser
{
	public string $raw;
	public string $parsed;

	public function __construct(string $input) {
		$this->raw = $input;
		$parsed = "";
		$i = 1;
		while($i < strlen($input) - 1) {
			$char = $input[$i];
			if($char === "\\") {
				$i++;
				switch($input[$i]) {
					case "\\" :
					case '"':
						$parsed .= $input[$i];
						break;
					case 'x';
						$parsed .= "X";
						$i += 2;
						break;
				}
			} else {
				$parsed .= $char;
			}

			$i++;
		}
		$this->parsed = $parsed;
	}
}