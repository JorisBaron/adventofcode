<?php

use AoC\Year2015\Day09\Graph;

require_once __DIR__.'/../../../vendor/autoload.php';

$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$calculated = [];
$maxPath = 0;
$graph = new Graph();
foreach ($file as $line) {
	preg_match('/^([A-Za-z]+)\sto\s([A-Za-z]+)\s=\s(\d+)$/', $line, $matches);
	[$_, $from, $to, $weight] = $matches;
	$graph->addEdges($from, $to, (int)$weight);
}

foreach ($graph->getNodes() as $node){
	try {
		$nodeMax = visit($graph, [$node], [$node => true]);
		if ($nodeMax > $maxPath){
			$maxPath = $nodeMax;
		}
	} catch (PathNotCovering $e) {}
}

echo $maxPath;

/**
 * @throws PathNotCovering
 */
function visit(Graph $graph, array $currentPath, array $visited = []) {
	$pathLength = count($currentPath);
	if($pathLength === $graph->getNodeCount()){
		return 0;
	}

	$dests = $graph->getPossibleDestinations($currentPath[count($currentPath)-1]);
	$maxSubCost = 0;
	foreach ($dests as $dest=>$weight) {
		if(!isset($visited[$dest])) {
			try {
				$localMax = visit($graph, array_merge($currentPath, [$dest]), array_merge($visited, [$dest=>true])) + $weight;
				if($localMax > $maxSubCost) {
					$maxSubCost = $localMax;
				}
			} catch (PathNotCovering) {}
		}
	}

	if ($maxSubCost === 0) {
		throw new PathNotCovering();
	}

	return $maxSubCost;
}


class PathNotCovering extends Exception {}
