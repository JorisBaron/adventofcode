<?php

use AoC\Year2015\Day09\Graph;

require_once __DIR__.'/../../../vendor/autoload.php';

$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$calculated = [];
$minPath = PHP_INT_MAX;
$graph = new Graph();
foreach($file as $line) {
	preg_match('/^([A-Za-z]+)\sto\s([A-Za-z]+)\s=\s(\d+)$/', $line, $matches);
	[$_, $from, $to, $weight] = $matches;
	$graph->addEdges($from, $to, (int)$weight);
}

foreach($graph->getNodes() as $node) {
	try {
		$nodeMin = visit($graph, [$node], [$node => true]);
		if($nodeMin < $minPath) {
			$minPath = $nodeMin;
		}
	} catch(PathNotCovering $e) {
	}
}

echo $minPath;

/**
 * @throws PathNotCovering
 */
function visit(Graph $graph, array $currentPath, array $visited = []) {
	$pathLength = count($currentPath);
	if($pathLength === $graph->getNodeCount()) {
		return 0;
	}

	$dests = $graph->getPossibleDestinations($currentPath[count($currentPath) - 1]);
	$minSubCost = null;
	foreach($dests as $dest => $weight) {
		if(!isset($visited[$dest])) {
			try {
				$localMin =
					visit($graph, array_merge($currentPath, [$dest]), array_merge($visited, [$dest => true])) + $weight;
				if($minSubCost === null || $localMin < $minSubCost) {
					$minSubCost = $localMin;
				}
			} catch(PathNotCovering) {
			}
		}
	}

	if($minSubCost === null) {
		throw new PathNotCovering();
	}

	return $minSubCost;
}


class PathNotCovering extends Exception { }
