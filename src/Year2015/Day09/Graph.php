<?php

namespace AoC\Year2015\Day09;

class Graph
{
	/** @var array<string, array<string, int> > */
	protected array $edges = [];
	/** @var array<string,true> */
	protected array $nodes = [];

	public function addEdges(string $from, string $to, int $weight) : void {
		$this->edges[$from][$to] = $weight;
		$this->edges[$to][$from] = $weight;
		if(!isset($this->nodes[$to])) {
			$this->nodes[$to] = true;
		}
		if(!isset($this->nodes[$from])) {
			$this->nodes[$from] = true;
		}
	}

	/**
	 * @param string $from
	 * @return array<string,int>
	 */
	public function getPossibleDestinations(string $from) : array {
		return $this->edges[$from] ?? [];
	}

	public function getNodeCount() : int {
		return count($this->nodes);
	}

	/**
	 * @return string[]
	 */
	public function getNodes() : array {
		return array_keys($this->nodes);
	}
}