<?php

$file = new SplFileObject("input/input.txt");
$file->setFlags(SplFileObject::DROP_NEW_LINE);

$n = array_map(intval(...), str_split($file->fgets()));

$maxIter = 50;

for ($iterations=0; $iterations < $maxIter; $iterations++){
	$len = count($n);
	$tmp = [];
	$currentDigit = $n[0];
	$currentCount = 1;
	for ($i=1; $i<$len; $i++){
		$digit = $n[$i];
		if($digit !== $currentDigit) {
			$tmp[] = $currentCount;
			$tmp[] = $currentDigit;
			$currentDigit = $digit;
			$currentCount = 1;
		} else {
			$currentCount++;
		}
	}

	$tmp[] = $currentCount;
	$tmp[] = $currentDigit;

	$n = $tmp;
}

echo count($n);