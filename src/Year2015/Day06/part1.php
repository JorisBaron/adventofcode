<?php

require_once __DIR__.'/../../../vendor/autoload.php';

use AoC\Year2015\Day06\LightGrid;

$file = new SplFileObject("input/input.txt");

$g = new LightGrid();

foreach ($file as $line){
	preg_match('/^(turn on|turn off|toggle) (\d+),(\d+) through (\d+),(\d+)$/', trim($line),$matches);
	$g->change((int)$matches[2], (int)$matches[3], (int)$matches[4], (int)$matches[5], $matches[1]);
}

echo $g->countLit().PHP_EOL;

