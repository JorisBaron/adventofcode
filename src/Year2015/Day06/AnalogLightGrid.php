<?php

namespace AoC\Year2015\Day06;

class AnalogLightGrid
{
	/** @var bool[][] */
	protected array $grid;
	protected int   $lit = 0;

	public function __construct() {
		$this->grid = array_fill(
			0,
			1000,
			array_fill(0, 1000, 0)
		);
	}

	public function change(int $x1, int $y1, int $x2, int $y2, string $type) : void {
		if ($x1 > $x2) {
			[$x1, $x2] = [$x2, $x1];
		}
		if ($y1 > $y2) {
			[$x1, $x2] = [$x2, $x1];
		}

		for ($y = $y1 ; $y <= $y2; $y++) {
			for ($x = $x1 ; $x <= $x2; $x++) {
				switch ($type) {
					case "turn on":
						$this->grid[$y][$x] += 1;
						break;
					case "turn off":
						if($this->grid[$y][$x] > 0) {
							$this->grid[$y][$x] -= 1;
						}
						break;
					case "toggle":
						$this->grid[$y][$x] += 2;
						break;

				}
			}
		}
	}

	public function countLit() : int {
		$sum = 0;
		for ($y=0 ; $y < 1000; $y++) {
			for ($x = 0; $x < 1000; $x++) {
				$sum += $this->grid[$y][$x];
			}
		}
		return $sum;
	}
}