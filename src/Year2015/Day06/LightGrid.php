<?php

namespace AoC\Year2015\Day06;

class LightGrid
{
	/** @var bool[][] */
	protected array $grid;
	protected int   $lit = 0;

	public function __construct() {
		$this->grid = array_fill(
			0,
			1000,
			array_fill(0, 1000, false)
		);
	}

	public function change(int $x1, int $y1, int $x2, int $y2, string $type) : void {
		if ($x1 > $x2) {
			[$x1, $x2] = [$x2, $x1];
		}
		if ($y1 > $y2) {
			[$x1, $x2] = [$x2, $x1];
		}

		for ($y = $y1 ; $y <= $y2; $y++) {
			for ($x = $x1 ; $x <= $x2; $x++) {
				switch ($type) {
					case "turn on":
						$this->grid[$y][$x] = true;
						break;
					case "turn off":
						$this->grid[$y][$x] = false;
						break;
					case "toggle":
						$this->grid[$y][$x] = !$this->grid[$y][$x];
						break;

				}
			}
		}
	}

	public function countLit() : int {
		$sum = 0;
		for ($y=0 ; $y < 1000; $y++) {
			for ($x = 0; $x < 1000; $x++) {
				if($this->grid[$y][$x])
					$sum++;
			}
		}
		return $sum;
	}
}