<?php

function day04(string $inputPath, string $zeros) : int {
	$fileContent = trim(file_get_contents("input/input.txt"));

	$i=0;
	do {
		$i++;
	} while(substr(md5($fileContent . $i), 0, $zeros) !== str_repeat("0",$zeros));

	return $i;
}