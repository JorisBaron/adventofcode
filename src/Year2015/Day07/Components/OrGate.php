<?php

namespace AoC\Year2015\Day07\Components;

use AoC\Year2015\Day07\Components\Contracts\BinaryGate;

require_once __DIR__."/Contracts/BinaryGate.php";

class OrGate extends BinaryGate
{
	protected function calculateValue() : void {
		$this->value = $this->input1->value() | $this->input2->value();
	}
}