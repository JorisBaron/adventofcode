<?php

namespace AoC\Year2015\Day07\Components\Contracts;

use AoC\Year2015\Day07\Components\Wire;

abstract class UnaryGate extends AbstractGate
{
	protected Wire $input;

	/**
	 * @param Wire $input
	 */
	public function setInput(Wire $input) : void {
		$this->input = $input;
	}

	/**
	 * @return Component[]
	 */
	public function update() : array {
		if($this->value !== null){
			return [];
//			return;
		}

		if($this->input->value() === null){
			return [];
//			return;
		}

		$this->calculateValue();
		return [$this->output];
//		$this->output->update();
	}
}