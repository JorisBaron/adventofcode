<?php

namespace AoC\Year2015\Day07\Components\Contracts;

require_once __DIR__."/UnaryGate.php";

abstract class UnaryNumberGate extends UnaryGate
{
	protected int $number;

	public function setNumber(int $number) : void {
		$this->number = $number;
	}
}