<?php

namespace AoC\Year2015\Day07\Components\Contracts;

interface Component
{
	/**
	 * @return Component[]
	 */
	public function update() : array;
	public function reset() : void;
	public function value() : ?int;
}