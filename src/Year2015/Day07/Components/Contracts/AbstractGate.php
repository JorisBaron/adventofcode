<?php

namespace AoC\Year2015\Day07\Components\Contracts;

use AoC\Year2015\Day07\Components\Wire;

require_once __DIR__."/Component.php";
require_once __DIR__."/../Wire.php";

abstract class AbstractGate implements Component
{
	protected string $name;
	protected ?int $value = null;

	protected Wire $output;

	public function __construct(string $name) {
		$this->name = $name;
	}


	public function setOutput(Wire $output) : void {
		$this->output = $output;
	}


	public function reset() : void {
		$this->value = null;
	}

	public function value() : ?int {
		return $this->value;
	}

	abstract protected function calculateValue() : void;
}