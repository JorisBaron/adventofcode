<?php

namespace AoC\Year2015\Day07\Components\Contracts;

use AoC\Year2015\Day07\Components\Wire;

require_once __DIR__."/AbstractGate.php";

abstract class BinaryGate extends AbstractGate
{
	protected Wire $input1;
	protected Wire $input2;

	/**
	 * @param Wire $input1
	 */
	public function setInput1(Wire $input1) : void {
		$this->input1 = $input1;
	}

	/**
	 * @param Wire $input2
	 */
	public function setInput2(Wire $input2) : void {
		$this->input2 = $input2;
	}

	/**
	 * @return Component[]
	 */
	public function update() : array {
		if($this->value !== null){
			return [];
//			return;
		}

		if($this->input1->value() === null || $this->input2->value() === null){
			return [];
//			return;
		}

		$this->calculateValue();
		return [$this->output];
//		$this->output->update();
	}
}