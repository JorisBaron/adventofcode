<?php

namespace AoC\Year2015\Day07\Components;

use AoC\Year2015\Day07\Components\Contracts\Component;

require_once __DIR__."/Contracts/Component.php";

class Wire implements Component
{
	protected string $name;
	protected ?int $value = null;

	protected Component $input;

	/** @var Component[] */
	protected array $outputs = [];

	public function __construct(string $name) {
		$this->name = $name;
	}

	public function setInput(Component $input) : void {
		$this->input = $input;
	}

	public function addOutput(Component $output) : void {
		$this->outputs[] = $output;
	}

	public function value() : ?int {
		return $this->value;
	}

	/**
	 * @return Component[]
	 */
	public function update() : array {
		if($this->value !== null) {
			return [];
		}

		if($this->input->value() === null){
			return [];
		}

		$this->value = $this->input->value();
		return $this->outputs;
	}

	public function reset() : void {
		$this->value = null;
	}
}