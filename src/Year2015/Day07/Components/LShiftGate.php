<?php

namespace AoC\Year2015\Day07\Components;

use AoC\Year2015\Day07\Components\Contracts\UnaryNumberGate;

require_once __DIR__."/Contracts/UnaryNumberGate.php";

class LShiftGate extends UnaryNumberGate
{
	protected function calculateValue() : void {
		$this->value = ($this->input->value() << $this->number) & 0xffff;
	}
}