<?php

namespace AoC\Year2015\Day07\Components;

use AoC\Year2015\Day07\Components\Contracts\Component;

require_once __DIR__."/Contracts/Component.php";

class Number implements Component
{
	public function __construct(
		protected int $value,
		protected Wire $output
	) {
		$this->output->setInput($this);
	}

//	public function setOutput(Wire $output) : void {
//		$this->output = $output;
//	}

	public function value() : int {
		return $this->value;
	}


	public function reset() : void {}

	/**
	 * @return Component[]
	 */
	public function update() : array {
		return [$this->output];
//		$this->output->update();
	}
}