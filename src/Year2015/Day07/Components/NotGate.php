<?php

namespace AoC\Year2015\Day07\Components;

use AoC\Year2015\Day07\Components\Contracts\UnaryGate;

require_once __DIR__."/Contracts/UnaryGate.php";

class NotGate extends UnaryGate
{
	protected function calculateValue() : void {
		$this->value = ~$this->input->value() & 0xffff;
	}
}