<?php

use AoC\Year2015\Day07\Assembly;

require_once __DIR__.'/../../../vendor/autoload.php';

$file = new SplFileObject("input/input.txt");

$lines = iterator_to_array($file);

$a = new Assembly();
foreach ($lines as $i=>$line) {
	$a->parseInstruction(trim($line));
}


$wires = $a->getWireValues();
$newB = $wires["a"];

foreach ($lines as &$line){
	if (preg_match("/-> b$/", trim($line))){
		$line = sprintf('%d -> b', $newB);;
	}
}
unset($line);

$b = new Assembly();
foreach ($lines as $i=> $line) {
	$b->parseInstruction(trim($line));
}

$wires2 = $b->getWireValues();
ksort($wires2);
foreach ($wires2 as $key => $wire) {
	echo $key . ": " . $wire.PHP_EOL;
};