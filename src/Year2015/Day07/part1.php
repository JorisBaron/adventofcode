<?php

use AoC\Year2015\Day07\Assembly;

require_once __DIR__.'/../../../vendor/autoload.php';

$file = new SplFileObject("input/input.txt");

$a = new Assembly();

foreach ($file as $i=>$line) {
	$a->parseInstruction(trim($line));
}

$wires = $a->getWireValues();
ksort($wires);
foreach ($wires as $key => $wire) {
	echo $key . ": " . $wire.PHP_EOL;
};