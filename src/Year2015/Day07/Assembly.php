<?php

namespace AoC\Year2015\Day07;

use AoC\Year2015\Day07\Components\Contracts\AbstractGate;
use AoC\Year2015\Day07\Components\Wire;
use AoC\Year2015\Day07\Components\Number;
use AoC\Year2015\Day07\Components\AndGate;
use AoC\Year2015\Day07\Components\OrGate;
use AoC\Year2015\Day07\Components\LShiftGate;
use AoC\Year2015\Day07\Components\RShiftGate;
use AoC\Year2015\Day07\Components\NotGate;
use Exception;
use AoC\Year2015\Day07\Components\Contracts\Component;

require_once __DIR__."/Components/AndGate.php";
require_once __DIR__ . "/Components/OrGate.php";
require_once __DIR__ . "/Components/LShiftGate.php";
require_once __DIR__ . "/Components/RShiftGate.php";
require_once __DIR__ . "/Components/NotGate.php";
require_once __DIR__ . "/Components/Wire.php";
require_once __DIR__ . "/Components/Number.php";

class Assembly
{

	/** @var AbstractGate[] */
	protected array $gates = [];
	/** @var Wire[] */
	protected array $wires = [];
	/** @var Number[] */
	protected array $numbers = [];

	protected function getOrCreateWire(string $name) : Wire {
		if (!isset($this->wires[$name])) {
			$this->wires[$name] = new Wire($name);
		}

		if (is_numeric($name)) {
			$this->numbers[] = new Number((int)$name, $this->wires[$name]);
		}

		return $this->wires[$name];
	}


	public function parseInstruction(string $instruction) : void {
		$identifierRegexPart = "[a-z]+|\d+";
		$andRegex = sprintf('/^(%1$s) AND (%1$s)$/', $identifierRegexPart);
		$orRegex = sprintf('/^(%1$s) OR (%1$s)$/', $identifierRegexPart);
		$lShiftRegex = sprintf('/^(%1$s) LSHIFT (\d+)$/', $identifierRegexPart);
		$rShiftRegex = sprintf('/^(%1$s) RSHIFT (\d+)$/', $identifierRegexPart);
		$notRegex = sprintf('/^NOT (%1$s)$/', $identifierRegexPart);
		$directRegex = '/^([a-z]+)$/';
		$valueRegex = '/^(\d+)$/';


		$inOut = explode(" -> ", $instruction);
		[$inputString, $outIdent] = $inOut;

		$outWire = $this->getOrCreateWire($outIdent);

		if (preg_match($andRegex, $inputString, $matches)) {
			$input1 = $this->getOrCreateWire($matches[1]);
			$input2 = $this->getOrCreateWire($matches[2]);

			$this->gates[] = $gate = new AndGate($instruction);
			$gate->setInput1($input1);
			$gate->setInput2($input2);
			$gate->setOutput($outWire);

			$input1->addOutput($gate);
			$input2->addOutput($gate);
			$outWire->setInput($gate);
		} elseif (preg_match($orRegex, $inputString, $matches)) {
			$input1 = $this->getOrCreateWire($matches[1]);
			$input2 = $this->getOrCreateWire($matches[2]);

			$this->gates[] = $gate = new OrGate($instruction);
			$gate->setInput1($input1);
			$gate->setInput2($input2);
			$gate->setOutput($outWire);

			$input1->addOutput($gate);
			$input2->addOutput($gate);
			$outWire->setInput($gate);
		} elseif (preg_match($lShiftRegex, $inputString, $matches)) {
			$input = $this->getOrCreateWire($matches[1]);
			$shift = (int)$matches[2];

			$this->gates[] = $gate = new LShiftGate($instruction);
			$gate->setInput($input);
			$gate->setNumber($shift);
			$gate->setOutput($outWire);

			$input->addOutput($gate);
			$outWire->setInput($gate);
		} elseif (preg_match($rShiftRegex, $inputString, $matches)) {
			$input = $this->getOrCreateWire($matches[1]);
			$shift = (int)$matches[2];

			$this->gates[] = $gate = new RShiftGate($instruction);
			$gate->setInput($input);
			$gate->setNumber($shift);
			$gate->setOutput($outWire);

			$input->addOutput($gate);
			$outWire->setInput($gate);
		} elseif (preg_match($notRegex, $inputString, $matches)) {
			$input = $this->getOrCreateWire($matches[1]);

			$this->gates[] = $gate = new NotGate($instruction);
			$gate->setInput($input);
			$gate->setOutput($outWire);

			$input->addOutput($gate);
			$outWire->setInput($gate);
		} elseif (preg_match($directRegex, $inputString, $matches)) {
			$input = $this->getOrCreateWire($matches[1]);
			$outWire->setInput($input);
			$input->addOutput($outWire);
		} elseif (preg_match($valueRegex, $inputString, $matches)) {
			$this->numbers[] = new Number((int)$matches[1], $outWire);
		} else {
			throw new Exception("Instruction unclear");
		}
	}

	public function getWireValues() : array {
		foreach ($this->numbers as $number) {
			$toUpdate = $number->update();
			while (count($toUpdate) > 0) {
				/** @var Component $component */
				$component = array_shift($toUpdate);
				$toUpdate = array_merge($toUpdate, $component->update());
			}
		}

		$wiresValues = [];
		foreach ($this->wires as $ident => $wire) {
			$wiresValues[$ident] = $wire->value();
		}
		return $wiresValues;
	}

	public function resetAll() : void {
		foreach ($this->wires as $wire){
			$wire->reset();
		}

		foreach ($this->gates as $gate){
			$gate->reset();
		}
	}

	public function overrideWire(string $name, $value) {
		$wire = $this->wires[$name];
	}
}