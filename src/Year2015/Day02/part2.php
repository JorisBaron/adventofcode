<?php

$file = new SplFileObject("input/input.txt");

$ribonsSum = 0;
foreach ($file as $line) {
	$dims = array_map(fn($str) => (int)$str, explode("x", $line));
	sort($dims);
	$ribonsSum += 2*($dims[0]+$dims[1]) + $dims[0]*$dims[1]*$dims[2];
}
echo $ribonsSum;