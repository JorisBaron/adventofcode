<?php

$file = new SplFileObject("input/input.txt");

$areasSum = 0;
foreach ($file as $line) {
	$dims = array_map(fn($str) => (int)$str, explode("x", $line));
	sort($dims);
	$areas = [
		$dims[0] * $dims[1],
		$dims[1] * $dims[2],
		$dims[2] * $dims[0],
	];
	$areasSum += 3*$areas[0] + 2*$areas[1] + 2*$areas[2];
}
echo $areasSum;